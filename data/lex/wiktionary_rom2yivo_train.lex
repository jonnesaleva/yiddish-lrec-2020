ob	א ָ ב
ovinu	א ָ ב ֿ י נ ו
oblast	א ָ ב ל א ַ ס ט
agent	א ַ ג ע נ ט
oger	א ָ ג ע ר
advokat	א ַ ד װ א ָ ק א ַ ט
adverb	א ַ ד װ ע ר ב
adyektiv	א ַ ד י ע ק ט י װ
adl	א ַ ד ל
odler	א ָ ד ל ע ר
oder	א ָ ד ע ר
oder	א ָ ד ר
adres	א ַ ד ר ע ס
avtograf	א ַ װ ט א ָ ג ר א ַ ף
avtor	א ַ װ ט א ָ ר
ovnt	א ָ װ נ ט
ovntshtern	א ָ װ נ ט ש ט ע ר ן
azot	א ַ ז א ָ ט
azbuke	א ַ ז ב ו ק ע
ozere	א ָ ז ע ר ע
ozhene	א ָ ז ש ע נ ע
akhrayes	א ַ ח ר י ו ת
atom	א ַ ט א ָ ם
atak	א ַ ט א ַ ק
atake	א ַ ט א ַ ק ע
atmosfer	א ַ ט מ א ָ ס פ ֿ ע ר
atmosfere	א ַ ט מ א ָ ס פ ֿ ע ר ע
ateizm	א ַ ט ע י ִ ז ם
ateist	א ַ ט ע י ִ ס ט
otem	א ָ ט ע ם
akhtsikstl	א ַ כ צ י ק ס ט ל
akhtsntl	א ַ כ צ נ ט ל
algebre	א ַ ל ג ע ב ר ע
aluminyum	א ַ ל ו מ י נ י ו ם
altdaytsh	א ַ ל ט ד ײ ַ ט ש
althoykhdaytsh	א ַ ל ט ה ו י כ ד ײ ַ ט ש
alter	א ַ ל ט ע ר
aligator	א ַ ל י ג א ַ ט א ָ ר
oliv	א ָ ל י װ
almer	א ַ ל מ ע ר
alee	א ַ ל ע ע
alergye	א ַ ל ע ר ג י ע
alef	א ַ ל ף
alef-beys	א ַ ל ף ־ ב י ת
alfabet	א ַ ל פ ֿ א ַ ב ע ט
alkohol	א ַ ל ק א ָ ה א ָ ל
alkoholizm	א ַ ל ק א ָ ה א ָ ל י ז ם
am	א ַ ם
ambasade	א ַ מ ב א ַ ס א ַ ד ע
amoyre	א ַ מ ו ר א
omlet	א ָ מ ל ע ט
amnezye	א ַ מ נ ע ז י ע
amerikaner	א ַ מ ע ר י ק א ַ נ ע ר
ananas	א ַ נ א ַ נ א ַ ס
anarkhizm	א ַ נ א ַ ר כ י ז ם
anarkhist	א ַ נ א ַ ר כ י ס ט
ondenk	א ָ נ ד ע נ ק
onheyb	א ָ נ ה ײ ב
onheyber	א ָ נ ה ײ ב ע ר
antologye	א ַ נ ט א ָ ל א ָ ג י ע
ontologye	א ָ נ ט א ָ ל א ָ ג י ע
onton	א ָ נ ט א ָ ן
onteyl	א ָ נ ט ײ ל
onteylung	א ָ נ ט ײ ל ו נ ג
antisemitizm	א ַ נ ט י ס ע מ י ט י ז ם
antipatye	א ַ נ ט י פ ּ א ַ ט י ע
antik	א ַ נ ט י ק
antloyfer	א ַ נ ט ל ו י פ ֿ ע ר
anekdot	א ַ נ ע ק ד א ָ ט
onfanger	א ָ נ פ ֿ א ַ נ ג ע ר
ontsinder	א ָ נ צ י נ ד ע ר
ankete	א ַ נ ק ע ט ע
astronmye	א ַ ס ט ר א ָ נ מ י ע
osyen	א ָ ס י ע ן
aspirin	א ַ ס פ ּ י ר י ן
aerodrom	א ַ ע ר א ָ ד ר א ָ ם
aeroport	א ַ ע ר א ָ פ ּ א ָ ר ט
aeroplan	א ַ ע ר א ָ פ ּ ל א ַ ן
aforizm	א ַ פ ֿ א ָ ר י ז ם
apatye	א ַ פ ּ א ַ ט י ע
apteyker	א ַ פ ּ ט ײ ק ע ר
apikoyres	א ַ פ ּ י ק ו ר ס
opl	א ָ פ ּ ל
apetit	א ַ פ ּ ע ט י ט
operatsye	א ָ פ ּ ע ר א ַ צ י ע
opere	א ָ פ ּ ע ר ע
operete	א ָ פ ּ ע ר ע ט ע
april	א ַ פ ּ ר י ל
aprikos	א ַ פ ּ ר י ק א ָ ס
afikoymen	א ַ פ ֿ י ק ו מ ן
afrikaner	א ַ פ ֿ ר י ק א ַ נ ע ר
afrikanerin	א ַ פ ֿ ר י ק א ַ נ ע ר י ן
akordeon	א ַ ק א ָ ר ד ע א ָ ן
oktober	א ָ ק ט א ָ ב ע ר
oktopus	א ָ ק ט א ָ פ ּ ו ס
aktyor	א ַ ק ט י א ָ ר
aktivitet	א ַ ק ט י װ י ט ע ט
aktivist	א ַ ק ט י װ י ס ט
aktrise	א ַ ק ט ר י ס ע
aks	א ַ ק ס
oks	א ָ ק ס
aksl	א ַ ק ס ל
okean	א ָ ק ע א ַ ן
aker	א ַ ק ע ר
aker-ayzn	א ַ ק ע ר ־ א ײ ַ ז ן
arabish	א ַ ר א ַ ב י ש
araber	א ַ ר א ַ ב ע ר
aromat	א ַ ר א ָ מ א ַ ט
aramish	א ַ ר א ַ מ י ש
arbet	א ַ ר ב ע ט
arbes	א ַ ר ב ע ס
arbeszup	א ַ ר ב ע ס ז ו פ ּ
argon	א ַ ר ג א ָ ן
organ	א ָ ר ג א ַ ן
organizatsye	א ָ ר ג א ַ נ י ז א ַ צ י ע
aroysreyd	א ַ ר ו י ס ר ײ ד
aroysred	א ַ ר ו י ס ר ע ד
arumnem	א ַ ר ו מ נ ע ם
orn-koydesh	א ָ ר ו ן ־ ק ו ד ש
ortografye	א ָ ר ט א ָ ג ר א ַ פ ֿ י ע
ortodoks	א ָ ר ט א ָ ד א ָ ק ס
ortodoksiye	א ָ ר ט א ָ ד א ָ ק ס י ע
artist	א ַ ר ט י ס ט
artistke	א ַ ר ט י ס ט ק ע
artikl	א ַ ר ט י ק ל
artishok	א ַ ר ט י ש א ָ ק
arterye	א ַ ר ט ע ר י ע
original	א ָ ר י ג י נ א ַ ל
arkhitekt	א ַ ר כ י ט ע ק ט
arkhitektor	א ַ ר כ י ט ע ק ט א ָ ר
armey	א ַ ר מ ײ
armenish	א ַ ר מ ע נ י ש
armener	א ַ ר מ ע נ ע ר
orevnik	א ָ ר ע װ נ י ק
orem	א ָ ר ע ם
oreman	א ָ ר ע מ א ַ ן
araynfir	א ַ ר ײ ַ נ פ ֿ י ר
ash	א ַ ש
ashkenazi	א ַ ש כ ּ נ ז י
eyver	א ב ֿ ר
adoyn	א ד ו ן
adoyni	א ד ו נ י
ugerke	א ו ג ע ר ק ע
oyg	א ו י ג
oyganes	א ו י ג א ַ נ ע ס
oygust	א ו י ג ו ס ט
oygn-ledl	א ו י ג ן ־ ל ע ד ל
oygn-lepl	א ו י ג ן ־ ל ע פ ּ ל
oygnblik	א ו י ג נ ב ל י ק
oygnledl	א ו י ג נ ל ע ד ל
oygnlepl	א ו י ג נ ל ע פ ּ ל
oygenes	א ו י ג ע נ ע ס
oygepl	א ו י ג ע פ ּ ל
oyvn	א ו י װ ן
oyto	א ו י ט א ָ
oytomobil	א ו י ט א ָ מ א ָ ב י ל
oytomat	א ו י ט א ָ מ א ַ ט
oytor	א ו י ט א ָ ר
oysherer	א ו י ס ה ע ר ע ר
oyster	א ו י ס ט ע ר
oysleyg	א ו י ס ל ײ ג
oysshtelung	א ו י ס ש ט ע ל ו נ ג
oyer	א ו י ע ר
oyfruf	א ו י פ ֿ ר ו ף
oyfshtand	א ו י פ ֿ ש ט א ַ נ ד
umbreng-lager	א ו מ ב ר ע נ ג ־ ל א ַ ג ע ר
ungarish	א ו נ ג א ַ ר י ש
unger	א ו נ ג ע ר
ungerish	א ו נ ג ע ר י ש
untergang	א ו נ ט ע ר ג א ַ נ ג
untergrunt	א ו נ ט ע ר ג ר ו נ ט
untermentsh	א ו נ ט ע ר מ ע נ ט ש
unternemung	א ו נ ט ע ר נ ע מ ו נ ג
untersheyd	א ו נ ט ע ר ש ײ ד
univers	א ו נ י װ ע ר ס
universitet	א ו נ י װ ע ר ס י ט ע ט
oyfn	א ו פ ֿ ן
oytser	א ו צ ר
ukrainish	א ו ק ר א ַ י ִ נ י ש
ukrainer	א ו ק ר א ַ י ִ נ ע ר
ur-eynikl	א ו ר ־ א ײ נ י ק ל
uranus	א ו ר א ַ נ ו ס
ureltern	א ו ר ע ל ט ע ר ן
os	א ו ת
akhren	א ח ר ו ן
akhrayes	א ח ר י ו ת
iberzetsung	א י ב ע ר ז ע צ ו נ ג
iberzetser	א י ב ע ר ז ע צ ע ר
idyot	א י ד י א ָ ט
idilye	א י ד י ל י ע
ideologye	א י ד ע א ָ ל א ָ ג י ע
identitet	א י ד ע נ ט י ט ע ט
italyener	א י ט א ַ ל י ע נ ע ר
eyzl	א ײ ז ל
eykhnboym	א ײ כ נ ב ו י ם
eyl	א ײ ל
eynikl	א ײ נ י ק ל
eyntsol	א ײ נ צ א ָ ל
eyele	א ײ ע ל ע
eyropeish	א ײ ר א ָ פ ּ ע י ִ ש
eyropeer	א ײ ר א ָ פ ּ ע ע ר
eyropeerin	א ײ ר א ָ פ ּ ע ע ר י ן
imam	א י מ א ַ ם
imigrant	א י מ י ג ר א ַ נ ט
imigratsye	א י מ י ג ר א ַ צ י ע
ingber	א י נ ג ב ע ר
ind	א י נ ד
indzl	א י נ ד ז ל
indik	א י נ ד י ק
inzl	א י נ ז ל
inzhenir	א י נ ז ש ע נ י ר
inzhenirye	א י נ ז ש ע נ י ר י ע
inzhener	א י נ ז ש ע נ ע ר
inzhenerye	א י נ ז ש ע נ ע ר י ע
internet	א י נ ט ע ר נ ע ט
internets	א י נ ט ע ר נ ע ץ
initsyativ	א י נ י צ י א ַ ט י װ
institut	א י נ ס ט י ט ו ט
infinitiv	א י נ פ ֿ י נ י ט י װ
inflatsye	א י נ פ ֿ ל א ַ צ י ע
isomer	א י ס א ָ מ ע ר
iser	א י ס ו ר
islam	א י ס ל א ַ ם
islamist	א י ס ל א ַ מ י ס ט
islamer	א י ס ל א ַ מ ע ר
irlender	א י ר ל ע נ ד ע ר
ire	א י ר ע
elel	א ל ו ל
alef	א ל ף
alef-beys	א ל ף ־ ב י ת
emune	א מ ו נ ה
amoyre	א מ ו ר א
emes	א מ ת
afikoymen	א פ י ק ו מ ן
orn	א ר ו ן
esreg	א ת ר ו ג
ayzn	א ײ ַ ז ן
ayznban	א ײ ַ ז נ ב א ַ ן
ayzkrem	א ײ ַ ז ק ר ע ם
ayroplan	א ײ ַ ר א ָ פ ּ ל א ַ ן
bob	ב א ָ ב
bobe-zeyde	ב א ָ ב ע ־ ז ײ ד ע
bobe-mayse	ב א ָ ב ע ־ מ ע ש ׂ ה
bagazh	ב א ַ ג א ַ ז ש
baginen	ב א ַ ג י נ ע ן
bagegenish	ב א ַ ג ע ג ע נ י ש
bager	ב א ַ ג ע ר
bagrif	ב א ַ ג ר י ף
bagreber	ב א ַ ג ר ע ב ע ר
bod	ב א ָ ד
badarf	ב א ַ ד א ַ ר ף
bodhitl	ב א ָ ד ה י ט ל
badekns	ב א ַ ד ע ק נ ס
bodkostum	ב א ָ ד ק א ָ ס ט ו ם
badaytung	ב א ַ ד ײ ַ ט ו נ ג
bahn	ב א ַ ה ן
bavegung	ב א ַ װ ע ג ו נ ג
bazalt	ב א ַ ז א ַ ל ט
bazukh	ב א ַ ז ו ך
bazukher	ב א ַ ז ו כ ע ר
batyushke	ב א ַ ט י ו ש ק ע
boyarin	ב א ָ י א ַ ר י ן
balagan	ב א ַ ל א ַ ג א ַ ן
balad	ב א ַ ל א ַ ד
balade	ב א ַ ל א ַ ד ע
balalayke	ב א ַ ל א ַ ל ײ ַ ק ע
balebos	ב א ַ ל ע ב א ָ ס
baleboste	ב א ַ ל ע ב א ָ ס ט ע
balem	ב א ַ ל ע ם
ban	ב א ַ ן
banane	ב א ַ נ א ַ נ ע
bandzho	ב א ַ נ ד ז ש א ָ
bank	ב א ַ נ ק
bank-kvetsher	ב א ַ נ ק ־ ק װ ע ט ש ע ר
bankbetl	ב א ַ נ ק ב ע ט ל
bankir	ב א ַ נ ק י ר
bankirer	ב א ַ נ ק י ר ע ר
bas	ב א ַ ס
bosnyak	ב א ָ ס נ י א ַ ק
basketbol	ב א ַ ס ק ע ט ב א ָ ל
baptist	ב א ַ פ ּ ט י ס ט
bafelkerung	ב א ַ פ ֿ ע ל ק ע ר ו נ ג
bafrayung	ב א ַ פ ֿ ר ײ ַ ו נ ג
bak	ב א ַ ק
bakteryologye	ב א ַ ק ט ע ר י א ָ ל א ָ ג י ע
bar	ב א ַ ר
barg	ב א ַ ר ג
bord	ב א ָ ר ד
border	ב א ָ ר ד ע ר
borsht	ב א ָ ר ש ט
bashafung	ב א ַ ש א ַ פ ֿ ו נ ג
bashafer	ב א ַ ש א ַ פ ֿ ע ר
bashlus	ב א ַ ש ל ו ס
bashefer	ב א ַ ש ע פ ֿ ע ר
bovli	ב ב ֿ ל י
badkhn	ב ד ח ן
behole	ב ה ל ה
bube	ב ו ב ע
bubele	ב ו ב ע ל ע
budhizm	ב ו ד ה י ז ם
budhist	ב ו ד ה י ס ט
buhay	ב ו ה ײ
buhay	ב ו ה ײ ַ
boygn	ב ו י ג ן
boygn-shiser	ב ו י ג ן ־ ש י ס ע ר
boytshikl	ב ו י ט ש י ק ל
boykh	ב ו י ך
boykhreder	ב ו י כ ר ע ד ע ר
boym	ב ו י ם
bukh	ב ו ך
bulbe	ב ו ל ב ע
bulgarish	ב ו ל ג א ַ ר י ש
buksir	ב ו ק ס י ר
bur	ב ו ר
boyre	ב ו ר א
burg	ב ו ר ג
burik	ב ו ר י ק
bokher	ב ח ו ר
byografye	ב י א ָ ג ר א ַ פ ֿ י ע
byokhemye	ב י א ָ כ ע מ י ע
bialy	ב י א ַ ל י
biblyoteker	ב י ב ל י א ָ ט ע ק ע ר
biblyotekerin	ב י ב ל י א ָ ט ע ק ע ר א י ן
biber	ב י ב ע ר
byuro	ב י ו ר א ָ
byurokratye	ב י ו ר א ָ ק ר א ַ ט י ע
bit	ב י ט
beygung	ב ײ ג ו נ ג
beygele	ב ײ ג ע ל ע
beyml	ב ײ מ ל
beyn	ב ײ ן
bikhl	ב י כ ל
bild	ב י ל ד
bildung	ב י ל ד ו נ ג
bilet	ב י ל ע ט
bime	ב י מ ה
bimkem	ב י מ ק ו ם
bin	ב י ן
bintl	ב י נ ט ל
bis	ב י ס
bisl	ב י ס ל
biskup	ב י ס ק ו פ ּ
bitsiklet	ב י צ י ק ל ע ט
bik	ב י ק
biks	ב י ק ס
bir	ב י ר
birger	ב י ר ג ע ר
bishof	ב י ש א ָ ף
beys	ב י ת
beyz-din	ב י ת ־ ד י ן
beys-oylem	ב י ת ־ ע ו ל ם
bkhor	ב כ ו ר
bkhoyre	ב כ ו ר ה
bloz	ב ל א ָ ז
blat	ב ל א ַ ט
blote	ב ל א ָ ט ע
blok	ב ל א ָ ק
blut	ב ל ו ט
blutbod	ב ל ו ט ב א ָ ד
blum	ב ל ו ם
blumentop	ב ל ו מ ע נ ט א ָ פ ּ
blintse	ב ל י נ צ ע
blitsbriv	ב ל י צ ב ר י װ
blitsbrivl	ב ל י צ ב ר י װ ל
blitspost	ב ל י צ פ ּ א ָ ס ט
blitsshlesl	ב ל י צ ש ל ע ס ל
bletl	ב ל ע ט ל
blekh	ב ל ע ך
blekher	ב ל ע כ ע ר
blay	ב ל ײ ַ
blayer	ב ל ײ ַ ע ר
bimkem	ב מ ק ו ם
ben-meylekh	ב ן ־ מ ל ך
bonem	ב נ י ם
bezem	ב ע ז ע ם
bet	ב ע ט
balebos	ב ע ל ־ ה ב י ת
balmelokhe	ב ע ל ־ מ ל א ָ כ ה
bentshlikht	ב ע נ ט ש ל י כ ט
benkl	ב ע נ ק ל
besaraber	ב ע ס א ַ ר א ַ ב ע ר
bekishe	ב ע ק י ש ע
beker	ב ע ק ע ר
bekeray	ב ע ק ע ר ײ ַ
bekeshe	ב ע ק ע ש ע
ber	ב ע ר
berilyum	ב ע ר י ל י ו ם
bereze	ב ע ר ע ז ע
bakoshe	ב ק ש ה
broder	ב ר א ָ ד ע ר
brokh	ב ר א ָ ך
brondz	ב ר א ָ נ ד ז
bronfn	ב ר א ָ נ פ ֿ ן
bronkser	ב ר א ָ נ ק ס ע ר
breyshes	ב ר א ש י ת
bruder	ב ר ו ד ע ר
brudershaft	ב ר ו ד ע ר ש א ַ פ ֿ ט
broyz	ב ר ו י ז
broyt	ב ר ו י ט
broytnitse	ב ר ו י ט נ י צ ע
brunet	ב ר ו נ ע ט
brunem	ב ר ו נ ע ם
brustkastn	ב ר ו ס ט ק א ַ ס ט ן
brukliner	ב ר ו ק ל י נ ע ר
brie	ב ר י א ה
bridershaft	ב ר י ד ע ר ש א ַ פ ֿ ט
briv	ב ר י װ
breyt	ב ר ײ ט
breytl	ב ר ײ ט ל
briln	ב ר י ל ן
brik	ב ר י ק
brikl	ב ר י ק ל
breyre	ב ר י ר ה
bris	ב ר י ת
brokhe	ב ר כ ה
breg	ב ר ע ג
bret	ב ר ע ט
bretl	ב ר ע ט ל
brem	ב ר ע ם
bray	ב ר ײ ַ
besomem	ב ש ׂ מ י ם
bas-malke	ב ת ־ מ ל כ ּ ה
bas-kol	ב ת ־ ק ו ל
bsule	ב ת ו ל ה
baytsh	ב ײ ַ ט ש
baykhl	ב ײ ַ כ ל
bayshpil	ב ײ ַ ש פ ּ י ל
gob	ג א ָ ב
got	ג א ָ ט
gotenyu	ג א ָ ט ע נ י ו
galon	ג א ַ ל א ָ ן
gold	ג א ָ ל ד
goldshmid	ג א ָ ל ד ש מ י ד
galitsyaner	ג א ַ ל י צ י א ַ נ ע ר
galitsyanerin	ג א ַ ל י צ י א ַ נ ע ר י ן
golmeser	ג א ָ ל מ ע ס ע ר
golmeserl	ג א ָ ל מ ע ס ע ר ל
golkrem	ג א ָ ל ק ר ע ם
gombe	ג א ָ מ ב ע
gomlke	ג א ָ מ ל ק ע
gondole	ג א ָ נ ד א ָ ל ע
gandz	ג א ַ נ ד ז
ganz	ג א ַ נ ז
gas	ג א ַ ס
gast	ג א ַ ס ט
gopl	ג א ָ פ ּ ל
gopl-lefl	ג א ָ פ ּ ל ־ ל ע פ ֿ ל
garb	ג א ַ ר ב
garber	ג א ַ ר ב ע ר
garberay	ג א ַ ר ב ע ר ײ ַ
gortn	ג א ָ ר ט ן
gortshitse	ג א ָ ר ט ש י צ ע
gorile	ג א ָ ר י ל ע
goen	ג א ו ן
gvure	ג ב ֿ ו ר ה
gabe	ג ב א י
gabete	ג ב א י ט ע
gvald	ג װ א ַ ל ד
gvalt	ג װ א ַ ל ט
gutmakhung	ג ו ט מ א ַ כ ו נ ג
goy	ג ו י
gulag	ג ו ל א ַ ג
goylem	ג ו ל ם
gume	ג ו מ ע
guf	ג ו ף
goyrl	ג ו ר ל
gazlen	ג ז ל ן
gzhive	ג ז ש י װ ע
gzhivke	ג ז ש י װ ק ע
giber	ג י ב ו ר
gitar	ג י ט א ַ ר
gikh	ג י ך
giml	ג י מ ל
gimnazye	ג י מ נ א ַ ז י ע
gift	ג י פ ֿ ט
globalizatsye	ג ל א ָ ב א ַ ל י ז א ַ צ י ע
gloz	ג ל א ָ ז
glok	ג ל א ָ ק
glust	ג ל ו ס ט
goles	ג ל ו ת
galekh	ג ל ח
glitsh	ג ל י ט ש
glik	ג ל י ק
gan	ג ן
ganef	ג נ ב
ganef	ג נ ב ֿ
gnéyve	ג נ ב ֿ ה
geografye	ג ע א ָ ג ר א ַ פ ֿ י ע
geologye	ג ע א ָ ל א ָ ג י ע
geboyrntog	ג ע ב ו י ר נ ט א ָ ג
geburtstog	ג ע ב ו ר ט ס ט א ָ ג
geburtstog	ג ע ב ו ר צ ט א ָ ג
gegnt	ג ע ג נ ט
gedank	ג ע ד א ַ נ ק
gedikht	ג ע ד י כ ט
gederem	ג ע ד ע ר ע ם
gevald	ג ע װ א ַ ל ד
gevalt	ג ע װ א ַ ל ט
geveyn	ג ע װ ײ ן
gevikht	ג ע װ י כ ט
gevins	ג ע װ י נ ס
gever	ג ע װ ע ר
gezang	ג ע ז א ַ נ ג
gezunt	ג ע ז ו נ ט
gezikht	ג ע ז י כ ט
gezel	ג ע ז ע ל
gezelshaft	ג ע ז ע ל ש א ַ פ ֿ ט
gezets	ג ע ז ע ץ
geto	ג ע ט א ָ
getrank	ג ע ט ר א ַ נ ק
gelt	ג ע ל ט
gendzl	ג ע נ ד ז ל
genetik	ג ע נ ע ט י ק
gesl	ג ע ס ל
geske	ג ע ס ק ע
gesrokhe	ג ע ס ר ח ה
gefil	ג ע פ ֿ י ל
geroysh	ג ע ר ו י ש
gertl	ג ע ר ט ל
gertner	ג ע ר ט נ ע ר
gertneray	ג ע ר ט נ ע ר ײ ַ
germanets	ג ע ר מ א ַ נ ע ץ
geshank	ג ע ש א ַ נ ק
geshvir	ג ע ש װ י ר
geshtalt	ג ע ש ט א ַ ל ט
geshikhte	ג ע ש י כ ט ע
geshmak	ג ע ש מ א ַ ק
gesheft	ג ע ש ע פ ֿ ט
gesheftsman	ג ע ש ע פ ֿ ט ס מ א ַ ן
geshprekh	ג ע ש פ ּ ר ע ך
grobayzn	ג ר א ָ ב א ײ ַ ז ן
grager	ג ר א ַ ג ע ר
groz	ג ר א ָ ז
gram	ג ר א ַ ם
gramatik	ג ר א ַ מ א ַ ט י ק
granit	ג ר א ַ נ י ט
graf	ג ר א ַ ף
grafit	ג ר א ַ פ ֿ י ט
grafin	ג ר א ַ פ ֿ י ן
grafinye	ג ר א ַ פ ֿ י נ י ע
groshn	ג ר א ָ ש ן
gruzinish	ג ר ו ז י נ י ש
gruziner	ג ר ו ז י נ ע ר
groyl	ג ר ו י ל
grupe	ג ר ו פ ּ ע
gribenes	ג ר י ב ע נ ע ס
grive	ג ר י װ ע
grivke	ג ר י װ ק ע
greypfrut	ג ר ײ פ ּ פ ֿ ר ו ט
greypfrukht	ג ר ײ פ ּ פ ֿ ר ו כ ט
grikh	ג ר י ך
grikhish	ג ר י כ י ש
grinder	ג ר י נ ד ע ר
grins	ג ר י נ ס
grif	ג ר י ף
greber	ג ר ע ב ע ר
gret	ג ר ע ט
grenets	ג ר ע נ ע ץ
grayz	ג ר ײ ַ ז
gashmies	ג ש מ י ו ת
gayst	ג ײ ַ ס ט
date	ד א ַ ט ע
datshe	ד א ַ ט ש ע
dolar	ד א ָ ל א ַ ר
damke	ד א ַ מ ק ע
donershtik	ד א ָ נ ע ר ש ט י ק
dokument	ד א ָ ק ו מ ע נ ט
doktor	ד א ָ ק ט א ָ ר
dokter	ד א ָ ק ט ע ר
dokteray	ד א ָ ק ט ע ר ײ ַ
daks	ד א ַ ק ס
dorfman	ד א ָ ר פ ֿ מ א ַ ן
dorsht	ד א ָ ר ש ט
dvoryanin	ד װ א ָ ר י א ַ נ י ן
dvorets	ד װ א ָ ר ע ץ
dor	ד ו ר
dzhaz	ד ז ש א ַ ז
dzhukhe	ד ז ש ו כ ע
dzhiraf	ד ז ש י ר א ַ ף
dzshez	ד ז ש ע ז
dzheyl	ד ז ש ע י ל
dyademe	ד י א ַ ד ע מ ע
dyalekt	ד י א ַ ל ע ק ט
dyalektologye	ד י א ַ ל ע ק ט א ָ ל א ָ ג י ע
dibek	ד י ב ו ק
dividend	ד י װ י ד ע נ ד
dayen	ד ײ ן
dikh	ד י ך
dikhtung	ד י כ ט ו נ ג
dikhter	ד י כ ט ע ר
dil	ד י ל
diment	ד י מ ע נ ט
din	ד י ן
dinozaver	ד י נ א ָ ז א ַ װ ע ר
dinastye	ד י נ א ַ ס ט י ע
dinye	ד י נ י ע
dinst	ד י נ ס ט
dinstog	ד י נ ס ט א ָ ג
dinstik	ד י נ ס ט י ק
dinstmoyd	ד י נ ס ט מ ו י ד
dinke	ד י נ ק ע
diskotek	ד י ס ק א ָ ט ע ק
diskriminatsye	ד י ס ק ר י מ י נ א ַ צ י ע
dikdek	ד י ק ד ו ק
dire	ד י ר ה
dire-gelt	ד י ר ה ־ ג ע ל ט
dlonye	ד ל א ָ נ י ע
delfin	ד ע ל פ ֿ י ן
demografye	ד ע מ א ָ ג ר א ַ פ ֿ י ע
demokratsye	ד ע מ א ָ ק ר א ַ צ י ע
denish	ד ע נ י ש
denker	ד ע נ ק ע ר
detsember	ד ע צ ע מ ב ע ר
deklaratsye	ד ע ק ל א ַ ר א ַ צ י ע
deklinatsye	ד ע ק ל י נ א ַ צ י ע
dernerung	ד ע ר נ ע ר ו נ ג
derfl	ד ע ר פ ֿ ל
derklerung	ד ע ר ק ל ע ר ו נ ג
dramaturg	ד ר א ַ מ א ַ ט ו ר ג
dramaturgye	ד ר א ַ מ א ַ ט ו ר ג י ע
drame	ד ר א ַ מ ע
drakon	ד ר א ַ ק א ָ ן
dorem	ד ר ו ם
dritl	ד ר י ט ל
dreydl	ד ר ײ ד ל
drek	ד ר ע ק
draysikstl	ד ר ײ ַ ס י ק ס ט ל
drayek	ד ר ײ ַ ע ק
draytsntl	ד ר ײ ַ צ נ ט ל
daytsh	ד ײ ַ ט ש
daytshmerizm	ד ײ ַ ט ש מ ע ר י ז ם
habikht	ה א ַ ב י כ ט
hober	ה א ָ ב ע ר
hoberin	ה א ָ ב ע ר י ן
hotel	ה א ָ ט ע ל
haldz	ה א ַ ל ד ז
holeptses	ה א ָ ל ע פ ּ צ ע ס
holts	ה א ָ ל ץ
homoseksualutet	ה א ָ מ א ָ ס ע ק ס ו א ַ ל ו ט ע ט
hamer	ה א ַ מ ע ר
hon	ה א ָ ן
handler	ה א ַ נ ד ל ע ר
hant	ה א ַ נ ט
hantbukh	ה א ַ נ ט ב ו ך
hantflakh	ה א ַ נ ט פ ֿ ל א ַ ך
honik	ה א ָ נ י ק
honikteygl	ה א ָ נ י ק ט ײ ג ל
hofenung	ה א ָ פ ֿ ע נ ו נ ג
hak	ה א ַ ק
hakfleysh	ה א ַ ק פ ֿ ל ײ ש
harbst	ה א ַ ר ב ס ט
horn	ה א ָ ר ן
hore	ה א ָ ר ע
harf	ה א ַ ר ף
harfe	ה א ַ ר פ ֿ ע
harts	ה א ַ ר ץ
havdole	ה ב ֿ ד ל ה
hut	ה ו ט
hoyz	ה ו י ז
hoyzn	ה ו י ז ן
hoyt	ה ו י ט
hoykhdaytsh	ה ו י כ ד ײ ַ ט ש
hoyptl	ה ו י פ ּ ט ל
hoyptshtot	ה ו י פ ּ ט ש ט א ָ ט
huligan	ה ו ל י ג א ַ ן
humor	ה ו מ א ָ ר
humorist	ה ו מ א ָ ר י ס ט
hun	ה ו ן
hunger	ה ו נ ג ע ר
hundertstl	ה ו נ ד ע ר ט ס ט ל
hunt	ה ו נ ט
hore	ה ו ר ה
hidrogen	ה י ד ר א ָ ג ע ן
hitl	ה י ט ל
hey	ה ײ
heybmuter	ה ײ ב מ ו ט ע ר
heyvn	ה ײ װ ן
heykh	ה ײ ך
heyl	ה ײ ל
heym	ה ײ ם
heymarbet	ה ײ מ א ַ ר ב ע ט
heymland	ה ײ מ ל א ַ נ ד
heymshtot	ה ײ מ ש ט א ָ ט
heyrat	ה ײ ר א ַ ט
heysherik	ה ײ ש ע ר י ק
hilf	ה י ל ף
hilfsverb	ה י ל פ ֿ ס װ ע ר ב
himl	ה י מ ל
himne	ה י מ נ ע
hindl	ה י נ ד ל
hintele	ה י נ ט ע ל ע
hintergesl	ה י נ ט ע ר ג ע ס ל
historye	ה י ס ט א ָ ר י ע
hirzh	ה י ר ז ש
hirsh	ה י ר ש
homen-tash	ה מ ן ־ ט א ַ ש
homen-tashn	ה מ ן ־ ט א ַ ש ן
hebreish	ה ע ב ר ע י ש
hebreish	ה ע ב ר ע י ִ ש
held	ה ע ל ד
heldzl	ה ע ל ד ז ל
heldin	ה ע ל ד י ן
helyum	ה ע ל י ו ם
helfand	ה ע ל פ ֿ א ַ נ ד
helfant	ה ע ל פ ֿ א ַ נ ט
helfer	ה ע ל פ ֿ ע ר
helferin	ה ע ל פ ֿ ע ר י ן
hemd	ה ע מ ד
hendler	ה ע נ ד ל ע ר
hentshke	ה ע נ ט ש ק ע
heft	ה ע פ ֿ ט
hekl	ה ע ק ל
her	ה ע ר
herb	ה ע ר ב
hering	ה ע ר י נ ג
hertsog	ה ע ר צ א ָ ג
haftore	ה פ ֿ ט ר ה
hakdome	ה ק ד מ ה
horov	ה ר ב ֿ
harige	ה ר י ג ה
haskole	ה ש ׂ כ ּ ל ה
hayzl	ה ײ ַ ז ל
hayzke	ה ײ ַ ז ק ע
hayfish	ה ײ ַ פ ֿ י ש
h'	ה ׳
vog	װ א ָ ג
vagon	װ א ַ ג א ָ ן
vagine	װ א ַ ג י נ ע
voyevode	װ א ָ י ע װ א ָ ד ע
vakh	װ א ַ ך
vokh	װ א ָ ך
vakhnakht	װ א ַ כ נ א ַ כ ט
vokhnblat	װ א ָ כ נ ב ל א ַ ט
vald	װ א ַ ל ד
volf	װ א ָ ל ף
valfish	װ א ַ ל פ ֿ י ש
volkn	װ א ָ ל ק ן
vant	װ א ַ נ ט
vane	װ א ַ נ ע
vants	װ א ַ נ ץ
vontses	װ א ָ נ צ ע ס
vaser	װ א ַ ס ע ר
vasershtof	װ א ַ ס ע ר ש ט א ָ ף
vofn	װ א ָ פ ֿ ן
vakatsye	װ א ַ ק א ַ צ י ע
vakuum	װ א ַ ק ו ּ ו ם
vor	װ א ָ ר
vort	װ א ָ ר ט
varnishkes	װ א ַ ר נ י ש ק ע ס
vorem	װ א ָ ר ע ם
varemkeyt	װ א ַ ר ע מ ק ײ ט
vortsl	װ א ָ ר צ ל
varshever	װ א ַ ר ש ע װ ע ר
vashtsimer	װ א ַ ש צ י מ ע ר
vaser	װ א ס ע ר
vund	װ ו ּ נ ד
vunder	װ ו ּ נ ד ע ר
vursht	װ ו ּ ר ש ט
vurshtl	װ ו ּ ר ש ט ל
voynung	װ ו י נ ו נ ג
vyole	װ י א ָ ל ע
vyosle	װ י א ָ ס ל ע
vyorst	װ י א ָ ר ס ט
vig	װ י ג
viglid	װ י ג ל י ד
vigele	װ י ג ע ל ע
vider	װ י ד ע ר
vidergutmakhung	װ י ד ע ר ג ו ט מ א ַ כ ו נ ג
vize	װ י ז ע
vey	װ ײ
veydl	װ ײ ד ל
veyts	װ ײ ץ
vindl	װ י נ ד ל
vint	װ י נ ט
vinter	װ י נ ט ע ר
vintertsayt	װ י נ ט ע ר צ ײ ַ ט
visn	װ י ס ן
visnshaft	װ י ס נ ש א ַ פ ֿ ט
visnshaftler	װ י ס נ ש א ַ פ ֿ ט ל ע ר
viskey	װ י ס ק ײ
vie	װ י ִ ע
vits	װ י ץ
virus	װ י ר ו ס
veb	װ ע ב
vebzaytl	װ ע ב ז ײ ַ ט ל
veg	װ ע ג
vegetaryer	װ ע ג ע ט א ַ ר י ע ר
vet-zayt	װ ע ט ־ ז ײ ַ ט
veter	װ ע ט ע ר
velosiped	װ ע ל א ָ ס י פ ּ ע ד
velt	װ ע ל ט
velt-milkhome	װ ע ל ט ־ מ ל ח מ ה
veltsvertl	װ ע ל ט ס װ ע ר ט ל
veltsmentsh	װ ע ל ט ס מ ע נ ט ש
venger	װ ע נ ג ע ר
venus	װ ע נ ו ס
vesle	װ ע ס ל ע
verb	װ ע ר ב
verbe	װ ע ר ב ע
vert	װ ע ר ט
vertl	װ ע ר ט ל
verterklas	װ ע ר ט ע ר ק ל א ַ ס
verst	װ ע ר ס ט
verk	װ ע ר ק
vesher	װ ע ש ע ר
vesherin	װ ע ש ע ר י ן
vayb	װ ײ ַ ב
vaybl	װ ײ ַ ב ל
vaybershul	װ ײ ַ ב ע ר ש ו ל
vayn	װ ײ ַ ן
vayngortn	װ ײ ַ נ ג א ָ ר ט ן
vayntroyb	װ ײ ַ נ ט ר ו י ב
vaynshlboym	װ ײ ַ נ ש ל ב ו י ם
vaysfish	װ ײ ַ ס פ ֿ י ש
zoologye	ז א ָ א ָ ל א ָ ג י ע
zoger	ז א ָ ג ע ר
zogerin	ז א ָ ג ע ר י ן
zogerke	ז א ָ ג ע ר ק ע
zaverukhe	ז א ַ װ ע ר ו כ ע
zotl	ז א ָ ט ל
zotler	ז א ָ ט ל ע ר
zakh	ז א ַ ך
zalb	ז א ַ ל ב
zalts	ז א ַ ל ץ
zamd	ז א ַ מ ד
zamlbukh	ז א ַ מ ל ב ו ך
zamler	ז א ַ מ ל ע ר
zang	ז א ַ נ ג
zaft	ז א ַ פ ֿ ט
zak	ז א ַ ק
zok	ז א ָ ק
zorg	ז א ָ ר ג
zoygling	ז ו י ג ל י נ ג
zoyger	ז ו י ג ע ר
zoymen	ז ו י מ ע ן
zoyerkroyt	ז ו י ע ר ק ר ו י ט
zulu	ז ו ל ו
zumer	ז ו מ ע ר
zumer-feygele	ז ו מ ע ר ־ פ ֿ ײ ג ע ל ע
zumertsayt	ז ו מ ע ר צ ײ ַ ט
zun	ז ו ן
zun-untergang	ז ו ן ־ א ו נ ט ע ר ג א ַ נ ג
zunoyfgang	ז ו נ א ו י פ ֿ ג א ַ נ ג
zoyne	ז ו נ ה
zuntik	ז ו נ ט י ק
zunfargang	ז ו נ פ ֿ א ַ ר ג א ַ נ ג
zup	ז ו פ ּ
zukini	ז ו ק י נ י
zibetsikstl	ז י ב ע צ י ק ס ט ל
zibetsntl	ז י ב ע צ נ ט ל
ziveg	ז י װ ג
zeyger	ז ײ ג ע ר
zeygerl	ז ײ ג ע ר ל
zeyde	ז ײ ד ע
zeyde-bobe	ז ײ ד ע ־ ב א ָ ב ע
zeyf	ז ײ ף
zilber	ז י ל ב ע ר
zin	ז י ן
zinger	ז י נ ג ע ר
zingerin	ז י נ ג ע ר י ן
zind	ז י נ ד
ziskayt	ז י ס ק ײ ַ ט
zitstrin	ז י צ ט ר י ן
zitsfleysh	ז י צ פ ֿ ל ײ ש
zikorn	ז י ק א ָ ר ן
zikorn	ז כ ּ ר ו ן
zlote	ז ל א ָ ט ע
zemer	ז מ ר
zemerl	ז מ ר ל
zebre	ז ע ב ר ע
zeg	ז ע ג
zekhtsikstl	ז ע כ צ י ק ס ט ל
zekhtsntl	ז ע כ צ נ ט ל
zelner	ז ע ל נ ע ר
zeltservaser	ז ע ל צ ע ר װ א ַ ס ע ר
zeneft	ז ע נ ע פ ֿ ט
zekl	ז ע ק ל
zekstl	ז ע ק ס ט ל
zokn	ז ק ן
zhabe	ז ש א ַ ב ע
zhuk	ז ש ו ק
zhurnal	ז ש ו ר נ א ַ ל
zhurnalizm	ז ש ו ר נ א ַ ל י ז ם
zhurnalist	ז ש ו ר נ א ַ ל י ס ט
zhurnalistik	ז ש ו ר נ א ַ ל י ס ט י ק
zhid	ז ש י ד
zhmenye	ז ש מ ע נ י ע
zayd	ז ײ ַ ד
khaver	ח ב ֿ ר
khevre	ח ב ֿ ר ה
khevreman	ח ב ֿ ר ה ־ מ א ַ ן
khevreshaft	ח ב ֿ ר ה ש א ַ פ ֿ ט
khavershaft	ח ב ֿ ר ש א ַ פ ֿ ט
kheyder	ח ד ר
khoydesh	ח ו ד ש
kholemoyed	ח ו ל   ה מ ו ע ד
khumesh	ח ו מ ש
khupe	ח ו פ ּ ה
khutspe	ח ו צ פ ּ ה
khurbn	ח ו ר ב ן
khoyshekh	ח ו ש ך
khazer	ח ז י ר
khazeray	ח ז י ר ײ ַ
khazal	ח ז ״ ל
khidesh	ח י ד ו ש
khaye	ח י ה
khayem	ח ײ ם
khilek	ח י ל ו ק
khinekh	ח י נ ו ך
khirek	ח י ר י ק
khokhem	ח כ ם
khokhme	ח כ מ ה
khale	ח ל ה
kholem	ח ל ו ם
khomets	ח מ ץ
kheyn	ח ן
khaneke	ח נ ו כ ּ ה
khsides	ח ס י ד ו ת
khosidl	ח ס י ד ל
khisorn	ח ס ר ו ן
kheyfets	ח פ ֿ ץ
kharoyses	ח ר ו ס ת
kheshbn	ח ש ב ו ן
kheshvn	ח ש װ ן
kheyshek	ח ש ק
khasene	ח ת ו נ ה
khsime	ח ת י מ ה
khosn	ח ת ן
khosn-kale	ח ת ן ־ כ ּ ל ה
tabak	ט א ַ ב א ַ ק
tog	ט א ָ ג
tog-bukh	ט א ָ ג ־ ב ו ך
togblat	ט א ָ ג ב ל א ַ ט
togtsaytung	ט א ָ ג צ ײ ַ ט ו נ ג
togshul	ט א ָ ג ש ו ל
tovl	ט א ָ װ ל
tatuirung	ט א ַ ט ו י ִ ר ו נ ג
tate	ט א ַ ט ע
tate-mame	ט א ַ ט ע ־ מ א ַ מ ע
tokhter	ט א ָ כ ט ע ר
tol	ט א ָ ל
tomat	ט א ָ מ א ַ ט
tante	ט א ַ נ ט ע
tants	ט א ַ נ ץ
top	ט א ָ פ ּ
topografye	ט א ָ פ ּ א ָ ג ר א ַ פ ֿ י ע
topyel	ט א ָ פ ּ י ע ל
tok	ט א ָ ק
taktik	ט א ַ ק ט י ק
taksi	ט א ַ ק ס י
tarakan	ט א ַ ר א ַ ק א ַ ן
tash	ט א ַ ש
tashngelt	ט א ַ ש נ ג ע ל ט
teyves	ט ב ֿ ת
toyb	ט ו י ב
toyznt	ט ו י ז נ ט
toyzntstl	ט ו י ז נ ט ס ט ל
toyt	ט ו י ט
tukh	ט ו ך
tuml	ט ו מ ל
tumler	ט ו מ ל ע ר
tunfish	ט ו נ פ ֿ י ש
turme	ט ו ר מ ע
turem	ט ו ר ע ם
tiger	ט י ג ע ר
tey	ט ײ
teyg	ט ײ ג
teygl	ט ײ ג ל
teytl	ט ײ ט ל
teytlboym	ט ײ ט ל ב ו י ם
teyl	ט ײ ל
tikhl	ט י כ ל
tint	ט י נ ט
tintfish	ט י נ ט פ ֿ י ש
tir	ט י ר
tish	ט י ש
tales	ט ל י ת
teater	ט ע א ַ ט ע ר
teologye	ט ע א ָ ל א ָ ג י ע
teorye	ט ע א ָ ר י ע
toes	ט ע ו ת
tekhnologye	ט ע כ נ א ָ ל א ָ ג י ע
tekhnik	ט ע כ נ י ק
tekhniker	ט ע כ נ י ק ע ר
telegrame	ט ע ל ע ג ר א ַ מ ע
telegraf	ט ע ל ע ג ר א ַ ף
televizor	ט ע ל ע װ י ז א ָ ר
televizye	ט ע ל ע װ י ז י ע
teleskop	ט ע ל ע ס ק א ָ פ ּ
telefon	ט ע ל ע פ ֿ א ָ ן
tam	ט ע ם
teme	ט ע מ ע
templ	ט ע מ פ ּ ל
tenenboym	ט ע נ ע נ ב ו י ם
tentsl	ט ע נ צ ל
tentser	ט ע נ צ ע ר
tekl	ט ע ק ל
teritorye	ט ע ר י ט א ָ ר י ע
termometer	ט ע ר מ א ָ מ ע ט ע ר
terk	ט ע ר ק
terkltoyb	ט ע ר ק ל ט ו י ב
trotuar	ט ר א ָ ט ו א ַ ר
trakht	ט ר א ַ כ ט
trop	ט ר א ָ פ ּ
trofey	ט ר א ָ פ ֿ ײ
trotskizm	ט ר א ָ צ ק י ז ם
trotskist	ט ר א ָ צ ק י ס ט
traktor	ט ר א ַ ק ט א ָ ר
troyb	ט ר ו י ב
troym	ט ר ו י ם
trumeyt	ט ר ו מ ײ ט
trumpeyt	ט ר ו מ פ ּ ײ ט
trupe	ט ר ו פ ּ ע
treyst	ט ר ײ ס ט
trilogye	ט ר י ל א ָ ג י ע
treger	ט ר ע ג ע ר
trer	ט ר ע ר
tshatshke	ט ש א ַ ט ש ק ע
tsholnt	ט ש א ָ ל נ ט
tshardash	ט ש א ַ ר ד א ַ ש
tshikaves	ט ש י ק א ַ װ ע ס
tshekh	ט ש ע ך
tshekhish	ט ש ע כ י ש
tshemodan	ט ש ע מ א ָ ד א ַ ן
tshekist	ט ש ע ק י ס ט
tshernobl	ט ש ע ר נ א ָ ב ל
tsherepakhe	ט ש ע ר ע פ ּ א ַ כ ע
tshaynik	ט ש ײ ַ נ י ק
tayvl	ט ײ ַ װ ל
tayvele	ט ײ ַ װ ע ל ע
taytsh	ט ײ ַ ט ש
taykh	ט ײ ַ ך
tayster	ט ײ ַ ס ט ע ר
yog	י א ָ ג
yogurt	י א ָ ג ו ר ט
yagede	י א ַ ג ע ד ע
yod	י א ָ ד
yor	י א ָ ה ר
yanuar	י א ַ נ ו א ַ ר
yonts	י א ָ נ ץ
yapanish	י א ַ פ ּ א ַ נ י ש
yapaner	י א ַ פ ּ א ַ נ ע ר
yor	י א ָ ר
yorhundert	י א ָ ר ה ו נ ד ע ר ט
yortog	י א ָ ר ט א ָ ג
yortoyznt	י א ָ ר ט ו י ז נ ט
yarid	י א ַ ר י ד
yarmulke	י א ַ ר מ ו ל ק ע
yarmlke	י א ַ ר מ ל ק ע
yortsendling	י א ָ ר צ ע נ ד ל י נ ג
yortsendlik	י א ָ ר צ ע נ ד ל י ק
yortsayt	י א ָ ר צ ײ ַ ט
yashtsherke	י א ַ ש ט ש ע ר ק ע
yor	י א ר
yedie	י ד י ע ה
yugnt	י ו ג נ ט
yoykh	י ו י ך
yuli	י ו ל י
yom	י ו ם
yontev	י ו ם ־ ט ו ב ֿ
yung	י ו נ ג
yunger-man	י ו נ ג ע ר ־ מ א ַ ן
yuni	י ו נ י
yunyon	י ו נ י א ָ ן
yupiter	י ו פ ּ י ט ע ר
yikhus	י ח ו ס
yid	ײ ִ ד
yidishizm	ײ ִ ד י ש י ז ם
yidishist	ײ ִ ד י ש י ס ט
yidishkeyt	ײ ִ ד י ש ק ײ ט
yidl	ײ ִ ד ל
yidene	ײ ִ ד ע נ ע
yingl	ײ ִ נ ג ל
yingele	ײ ִ נ ג ע ל ע
yires-hakoved	ײ ִ ר א ת ־ ה כ ּ ב ֿ ו ד
yam	י ם
yam-shtern	י ם ־ ש ט ע ר ן
yisurem	י ס ו ר י ם
yeger	י ע ג ע ר
yevsektsye	י ע װ ס ע ק צ י ע
yente	י ע נ ט ע
yeke	י ע ק ע
yeytser-hatoyv	י צ ר ־ ה ט ו ב ֿ
yeytser-hore	י צ ר ־ ה ר ע
yirgozn	י ר ג ז ו ן
yerushe	י ר ו ש ה
yarid	י ר י ד
yash	י ש
yeshive	י ש י ב ֿ ה
yeshive-bokher	י ש י ב ֿ ה ־ ב ח ו ר
yeshivish	י ש י ב ֿ י ש
yeshive	י ש י ב ה
yosem	י ת ו ם
yesoyme	י ת ו מ ה
khalve	כ א ַ ל װ ע
khosn	כ א ָ ס ן
khasene	כ א ַ ס ע נ ע
khaper	כ א ַ פ ּ ע ר
koved	כ ּ ב ֿ ו ד
kehune	כ ּ ה ו נ ה
khvat	כ װ א ַ ט
khvalye	כ װ א ַ ל י ע
koyekh	כ ּ ו ח
kezayes	כ ּ ז י ת
koyekh	כ ּ ח
kale	כ ּ ל ה
khuligan	כ ו ל י ג א ַ ן
kislev	כ ּ ס ל ו
kashres	כ ּ ש ר ו ת
ksube	כ ּ ת ו ב ה
keser	כ ּ ת ר
khinezer	כ י נ ע ז ע ר
khirurgye	כ י ר ו ר ג י ע
khmare	כ מ א ַ ר ע
khnyok	כ נ י א ָ ק
khelmer	כ ע ל מ ע ר
khelemer	כ ע ל ע מ ע ר
khemye	כ ע מ י ע
khemiker	כ ע מ י ק ע ר
khrabye	כ ר א ַ ב י ע
khrizanteme	כ ר י ז א ַ נ ט ע מ ע
khreyn	כ ר ײ ן
khremzl	כ ר ע מ ז ל
khrestomatye	כ ר ע ס ט א ָ מ א ַ ט י ע
kashres	כ ש ר ו ת
keser	כ ת ר
labn	ל א ַ ב ן
logaritm	ל א ָ ג א ַ ר י ט ם
logik	ל א ָ ג י ק
laureat	ל א ַ ו ר ע א ַ ט
latvish	ל א ַ ט װ י ש
latutnik	ל א ַ ט ו ט נ י ק
latke	ל א ַ ט ק ע
lam	ל א ַ ם
lomp	ל א ָ מ פ ּ
land	ל א ַ נ ד
landsman	ל א ַ נ ד ס מ א ַ ן
landkarte	ל א ַ נ ד ק א ַ ר ט ע
lapserdak	ל א ַ פ ּ ס ע ר ד א ַ ק
lopete	ל א ָ פ ּ ע ט ע
lafer	ל א ַ פ ֿ ע ר
laks	ל א ַ ק ס
loksh	ל א ָ ק ש
lord	ל א ָ ר ד
levyosn	ל װ י ת ן
luekh	ל ו ח
loyb	ל ו י ב
loyz	ל ו י ז
loyer	ל ו י ע ר
levyosn	ל ו י ת ן
lulev	ל ו ל ב ֿ
lung	ל ו נ ג
luft	ל ו פ ֿ ט
luftmentsh	ל ו פ ֿ ט מ ע נ ט ש
luftfeld	ל ו פ ֿ ט פ ֿ ע ל ד
lyalke	ל י א ַ ל ק ע
libe	ל י ב ע
libele	ל י ב ע ל ע
lign	ל י ג ן
ligner	ל י ג נ ע ר
lid	ל י ד
litviner	ל י ט װ י נ ע ר
litvish	ל י ט װ י ש
litium	ל י ט י ו ם
liter	ל י ט ע ר
literatur	ל י ט ע ר א ַ ט ו ר
leyb	ל ײ ב
leybikhe	ל ײ ב י כ ע
leyter	ל ײ ט ע ר
leym	ל ײ ם
leyenung	ל ײ ע נ ו נ ג
leyener	ל ײ ע נ ע ר
leyke	ל ײ ק ע
likht	ל י כ ט
likhtl	ל י כ ט ל
lilye	ל י ל י ע
limonad	ל י מ א ָ נ א ַ ד
limed	ל י מ ו ד
limene	ל י מ ע נ ע
lingvist	ל י נ ג װ י ס ט
lingvistik	ל י נ ג װ י ס ט י ק
lindz	ל י נ ד ז
linkist	ל י נ ק י ס ט
lip	ל י פ ּ
lik	ל י ק
lamdn	ל מ ד ן
lebl	ל ע ב ל
lebn	ל ע ב ן
leber	ל ע ב ע ר
legende	ל ע ג ע נ ד ע
leder	ל ע ד ע ר
lezer	ל ע ז ע ר
letish	ל ע ט י ש
leml	ל ע מ ל
lendn	ל ע נ ד ן
leninizm	ל ע נ י נ י ז ם
leninist	ל ע נ י נ י ס ט
lefl	ל ע פ ֿ ל
lek	ל ע ק
leker	ל ע ק ע ר
lektsye	ל ע ק צ י ע
lernung	ל ע ר נ ו נ ג
lernyingl	ל ע ר נ ײ ִ נ ג ל
lerer	ל ע ר ע ר
lererin	ל ע ר ע ר י ן
lets	ל ץ
leytsekhe	ל צ ע כ ע
loshn	ל ש ו ן
loshn-koydesh	ל ש ו ן ־ ק ו ד ש
laybl	ל ײ ַ ב ל
layvnt	ל ײ ַ װ נ ט
layt	ל ײ ַ ט
mobil-telefon	מ א ָ ב י ל ־ ט ע ל ע פ ֿ א ָ ן
mobilke	מ א ָ ב י ל ק ע
magnet	מ א ַ ג נ ע ט
madzhar	מ א ַ ד ז ש א ַ ר
mototsikl	מ א ָ ט א ָ צ י ק ל
matematik	מ א ַ ט ע מ א ַ ט י ק
materye	מ א ַ ט ע ר י ע
may	מ א ַ י
makh	מ א ַ ך
mokh	מ א ָ ך
makhorke	מ א ַ כ א ָ ר ק ע
makht	מ א ַ כ ט
makher	מ א ַ כ ע ר
mol	מ א ָ ל
molekul	מ א ָ ל ע ק ו ל
moler	מ א ָ ל ע ר
moleray	מ א ָ ל ע ר ײ ַ
malpe	מ א ַ ל פ ּ ע
moltsayt	מ א ָ ל צ ײ ַ ט
maminke	מ א ַ מ י נ ק ע
mame	מ א ַ מ ע
mame-loshn	מ א ַ מ ע ־ ל ש ו ן
moment	מ א ָ מ ע נ ט
mamenyu	מ א ַ מ ע נ י ו
mameshi	מ א ַ מ ע ש י
man	מ א ַ ן
monat	מ א ָ נ א ַ ט
monolog	מ א ָ נ א ָ ל א ָ ג
mango	מ א ַ נ ג א ָ
manganez	מ א ַ נ ג א ַ נ ע ז
mandoline	מ א ַ נ ד א ָ ל י נ ע
mandl	מ א ַ נ ד ל
mandlakh	מ א ַ נ ד ל א ַ ך
mandlboym	מ א ַ נ ד ל ב ו י ם
mandlbrot	מ א ַ נ ד ל ב ר א ָ ט
mandlekh	מ א ַ נ ד ל ע ך
montik	מ א ָ נ ט י ק
monizm	מ א ָ נ י ז ם
monist	מ א ָ נ י ס ט
mansbil	מ א ַ נ ס ב י ל
mansparshoyn	מ א ַ נ ס פ ּ א ַ ר ש ו י ן
mos	מ א ָ ס
masline	מ א ַ ס ל י נ ע
moskver	מ א ָ ס ק װ ע ר
moral	מ א ָ ר א ַ ל
marants	מ א ַ ר א ַ נ ץ
morgn	מ א ָ ר ג ן
morgnshtern	מ א ָ ר ג נ ש ט ע ר ן
morde	מ א ָ ר ד ע
markh	מ א ַ ר ך
marmalad	מ א ַ ר מ א ַ ל א ַ ד
marmolad	מ א ַ ר מ א ָ ל א ַ ד
marmor	מ א ַ ר מ א ָ ר
marmelad	מ א ַ ר מ ע ל א ַ ד
marmer	מ א ַ ר מ ע ר
marts	מ א ַ ר ץ
mark	מ א ַ ר ק
marksizm	מ א ַ ר ק ס י ז ם
marksist	מ א ַ ר ק ס י ס ט
marsh	מ א ַ ר ש
mashin	מ א ַ ש י ן
madim	מ א ד י ם
maykhl	מ א כ ל
maymer	מ א מ ר
meyvn	מ ב ֿ י ן
megile	מ ג י ל ה
midber	מ ד ב ר
medine	מ ד י נ ה
madreyge	מ ד ר ג ה
medrash	מ ד ר ש
moyel	מ ו ה ל
moves	מ װ ת
muzey	מ ו ז ײ
muzik	מ ו ז י ק
muziker	מ ו ז י ק ע ר
moyekh	מ ו ח
mut	מ ו ט
muter	מ ו ט ע ר
muter-tseykhn	מ ו ט ע ר ־ צ ײ כ ן
muter-shprakh	מ ו ט ע ר ־ ש פ ּ ר א ַ ך
mutertrakht	מ ו ט ע ר ט ר א ַ כ ט
muterland	מ ו ט ע ר ל א ַ נ ד
mutersheyd	מ ו ט ע ר ש ײ ד
mutershprakh	מ ו ט ע ר ש פ ּ ר א ַ ך
moyd	מ ו י ד
moyz	מ ו י ז
moyl	מ ו י ל
moykher-sforim	מ ו כ ר ־ ס פ ֿ ר י ם
musulman	מ ו ס ו ל מ א ַ ן
muskl	מ ו ס ק ל
moyre	מ ו ר א
murashke	מ ו ר א ַ ש ק ע
mezuze	מ ז ו ז ה
mekhaber	מ ח ב ר
mekhutn	מ ח ו ת ּ ן
mekhuteneste	מ ח ו ת ּ נ ת ט ע
mekháye	מ ח י ה
makhloykes	מ ח ל ו ק ת
matbeye	מ ט ב ע
myate	מ י א ַ ט ע
meyeskeyt	מ י א ו ס ק ײ ט
midkeyt	מ י ד ק ײ ט
mit	מ י ט
mite	מ י ט ה
mitvokh	מ י ט װ א ָ ך
mitl	מ י ט ל
mitlhoykhdaytsh	מ י ט ל ה ו י כ ד ײ ַ ט ש
mitlshul	מ י ט ל ש ו ל
mitn	מ י ט ן
meydl	מ ײ ד ל
meydele	מ ײ ד ע ל ע
meynung	מ ײ נ ו נ ג
milgroym	מ י ל ג ר ו י ם
milyon	מ י ל י א ָ ן
milyoner	מ י ל י א ָ נ ע ר
milyard	מ י ל י א ַ ר ד
miligram	מ י ל י ג ר א ַ ם
militant	מ י ל י ט א ַ נ ט
militer	מ י ל י ט ע ר
mililiter	מ י ל י ל י ט ע ר
milimeter	מ י ל י מ ע ט ע ר
milkh	מ י ל ך
milkhiger	מ י ל כ י ג ע ר
milkiker	מ י ל כ י ק ע ר
milekh	מ י ל ע ך
min	מ י ן
minaret	מ י נ א ַ ר ע ט
minut	מ י נ ו ט
minister	מ י נ י ס ט ע ר
minsker	מ י נ ס ק ע ר
mineral-vaser	מ י נ ע ר א ַ ל ־ װ א ַ ס ע ר
mints	מ י נ ץ
mistitsizm	מ י ס ט י צ י ז ם
myente	מ י ע נ ט ע
mikrofilm	מ י ק ר א ָ פ ֿ י ל ם
mirml	מ י ר מ ל
mishne	מ י ש נ ה
mishpokhe	מ י ש פ ּ א ָ כ ע
makabi	מ כ ּ ב י
makabeyer	מ כ ּ ב ײ ע ר
make	מ כ ּ ה
melokhe	מ ל א ָ כ ה
malekh	מ ל א ך
malakhamoves	מ ל א ך ־ ה מ ו ת
melukhe	מ ל ו כ ה
milkhome	מ ל ח מ ה
meylekh	מ ל ך
malke	מ ל כ ה
malke	מ ל כ ּ ה
melamed	מ ל מ ד
memshole	מ מ ש ל ה
man	מ ן
menagan	מ נ ג ן
mineg	מ נ ה ג
mnoyre	מ נ ו ר ה
minkhe	מ נ ח ה
minyen	מ נ י ן
mesoyre	מ ס ו ר ה
mesire	מ ס י ר ה
mesekhte	מ ס כ ת א
mesekhte	מ ס כ ת ּ א
misper	מ ס פ ּ ר
mebl	מ ע ב ל
medikament	מ ע ד י ק א ַ מ ע נ ט
metod	מ ע ט א ָ ד
metodist	מ ע ט א ָ ד י ס ט
metodik	מ ע ט א ָ ד י ק
metode	מ ע ט א ָ ד ע
metafizik	מ ע ט א ַ פ ֿ י ז י ק
meteorologye	מ ע ט ע א ָ ר א ָ ל א ָ ג י ע
metro	מ ע ט ר א ָ
metshet	מ ע ט ש ע ט
mekhaniker	מ ע כ א ַ נ י ק ע ר
mel	מ ע ל
melon	מ ע ל א ָ ן
mayle	מ ע ל ה
memuarn	מ ע מ ו א ַ ר ן
mentsh	מ ע נ ט ש
meser	מ ע ס ע ר
mekler	מ ע ק ל ע ר
mer	מ ע ר
mayrev	מ ע ר ב
merheyt	מ ע ר ה ײ ט
marokhe	מ ע ר כ ה
mertsol	מ ע ר צ א ָ ל
mesh	מ ע ש
mayse/manse	מ ע ש ׂ ה
mayse/manse	מ ע ש ה
mafter	מ פ ֿ ט י ר
matse	מ צ ה
metsiye	מ צ י א ה
mikve	מ ק װ ה
mokem	מ ק ו ם
maskil	מ ש ׂ כ ּ י ל
meshugener	מ ש ו ג ע נ ע ר
meshugas	מ ש ו ג ע ת
moshiyekh	מ ש י ח
moshl	מ ש ל
mishpokhe	מ ש פ ּ ח ה
mishpokhe	מ ש פ ח ה
mashke	מ ש ק ה
meys	מ ת
matone	מ ת ּ נ ה
misnaged	מ ת נ ג ד
may	מ ײ ַ
mayl	מ ײ ַ ל
mayster	מ ײ ַ ס ט ע ר
nogl	נ א ָ ג ל
nodl	נ א ָ ד ל
november	נ א ָ װ ע מ ב ע ר
noz	נ א ָ ז
nozlokh	נ א ָ ז ל א ָ ך
natur	נ א ַ ט ו ר
nakht	נ א ַ כ ט
nakhthemd	נ א ַ כ ט ה ע מ ד
nokhloyfer	נ א ָ כ ל ו י פ ֿ ע ר
nomen	נ א ָ מ ע ן
nos	נ א ָ ס
natsyonal-sotsyalizm	נ א ַ צ י א ָ נ א ַ ל ־ ס א ָ צ י א ַ ל י ז ם
natsyonalizm	נ א ַ צ י א ָ נ א ַ ל י ז ם
natsizm	נ א ַ צ י ז ם
nakn	נ א ַ ק ן
nar	נ א ַ ר
narishkeyt	נ א ַ ר י ש ק ײ ט
nevue	נ ב ֿ ו א ה
novi	נ ב ֿ י א
neviye	נ ב ֿ י א ה
ngid	נ ג י ד
noyef	נ ו א ף
nudnik	נ ו ד נ י ק
numer	נ ו מ ע ר
nus	נ ו ס
nusekh	נ ו ס ח
nukleon	נ ו ק ל ע א ָ ן
nakhes	נ ח ת
nief	נ י א ו ף
nign	נ י ג ו ן
nigndl	נ י ג ו נ ד ל
nihilist	נ י ה י ל י ס ט
nivo	נ י װ א ָ
nitl	נ י ט ל
nitlboym	נ י ט ל ב ו י ם
neytron	נ ײ ט ר א ָ ן
neymashin	נ ײ מ א ַ ש י ן
nis	נ י ס
nisl	נ י ס ל
nisn	נ י ס ן
niseray	נ י ס ע ר ײ ַ
niked	נ י ק ו ד
nikl	נ י ק ל
nir	נ י ר
nes	נ ס
neon	נ ע א ָ ן
nebish	נ ע ב י ש
neger	נ ע ג ע ר
nevrologye	נ ע װ ר א ָ ל א ָ ג י ע
nest	נ ע ס ט
neptun	נ ע פ ּ ט ו ן
neptunyum	נ ע פ ּ ט ו נ י ו ם
nepl	נ ע פ ּ ל
nets	נ ע ץ
nerv	נ ע ר װ
neshome	נ ש מ ה
nayntl	נ ײ ַ נ ט ל
nayntsikstl	נ ײ ַ נ צ י ק ס ט ל
nayntsntl	נ ײ ַ נ צ נ ט ל
nayes	נ ײ ַ ע ס
sod	ס א ָ ד
sode	ס א ָ ד ע
sovyet	ס א ָ װ י ע ט
sove	ס א ָ װ ע
sovetizatsye	ס א ָ װ ע ט י ז א ַ צ י ע
sazhlke	ס א ַ ז ש ל ק ע
saturn	ס א ַ ט ו ר ן
salat	ס א ַ ל א ַ ט
soldat	ס א ָ ל ד א ַ ט
samoderzhavye	ס א ַ מ א ָ ד ע ר ז ש א ַ װ י ע
sosne	ס א ָ ס נ ע
sofe	ס א ָ פ ֿ ע
sotsyolog	ס א ָ צ י א ָ ל א ָ ג
sotsyalizm	ס א ָ צ י א ַ ל י ז ם
sotsyalist	ס א ָ צ י א ַ ל י ס ט
saksofon	ס א ַ ק ס א ָ פ ֿ א ָ ן
soroke	ס א ָ ר א ָ ק ע
svive	ס ב ֿ י ב ֿ ה
seyder	ס ד ר
subotnik	ס ו ב א ָ ט נ י ק
substantiv	ס ו ב ס ט א ַ נ ט י װ
sod	ס ו ד
sultan	ס ו ל ט א ַ ן
sof	ס ו ף
soyfer	ס ו פ ֿ ר
skhoyre	ס ח ו ר ה
stav	ס ט א ַ װ
statistik	ס ט א ַ ט י ס ט י ק
stolyar	ס ט א ָ ל י א ַ ר
stalinizm	ס ט א ַ ל י נ י ז ם
stalinist	ס ט א ַ ל י נ י ס ט
stolyer	ס ט א ָ ל י ע ר
stashke	ס ט א ַ ש ק ע
stashkeshmir	ס ט א ַ ש ק ע ש מ י ר
studyo	ס ט ו ד י א ָ
studye	ס ט ו ד י ע
student	ס ט ו ד ע נ ט
studentin	ס ט ו ד ע נ ט י ן
studentke	ס ט ו ד ע נ ט ק ע
struzh	ס ט ר ו ז ש
strune	ס ט ר ו נ ע
sibirer	ס י ב י ר ע ר
sider	ס י ד ו ר
sivn	ס י װ ן
seym	ס ײ ם
seymist	ס ײ מ י ס ט
silitsyum	ס י ל י צ י ו ם
silikon	ס י ל י ק א ָ ן
simbol	ס י מ ב א ָ ל
simen	ס י מ ן
sinagoge	ס י נ א ַ ג א ָ ג ע
sirop	ס י ר א ָ פ ּ
sirup	ס י ר ו פ ּ
slovak	ס ל א ָ װ א ַ ק
slovakish	ס ל א ָ װ א ַ ק י ש
sam	ס ם
smoking	ס מ א ָ ק י נ ג
smaragd	ס מ א ַ ר א ַ ג ד
smikhe	ס מ י כ ה
smetene	ס מ ע ט ע נ ע
snop	ס נ א ָ פ ּ
sude	ס ע ו ד ה
selerye	ס ע ל ע ר י ע
senat	ס ע נ א ַ ט
senator	ס ע נ א ַ ט א ָ ר
senyor	ס ע נ י א ָ ר
senyora	ס ע נ י א ָ ר א ַ
september	ס ע פ ּ ט ע מ ב ע ר
sekunde	ס ע ק ו נ ד ע
sekte	ס ע ק ט ע
serbish	ס ע ר ב י ש
servetke	ס ע ר װ ע ט ק ע
serye	ס ע ר י ע
spodnitse	ס פ ּ א ָ ד נ י צ ע
spodek	ס פ ּ א ָ ד ע ק
sparzhe	ס פ ּ א ַ ר ז ש ע
spudnitse	ס פ ּ ו ד נ י צ ע
spiritualizm	ס פ ּ י ר י ט ו א ַ ל י ז ם
spektakl	ס פ ּ ע ק ט א ַ ק ל
seyfer	ס פ ֿ ר
skorpyon	ס ק א ָ ר פ ּ י א ָ ן
stire	ס ת ּ י ר ה
aveyre	ע ב ֿ ר ה
ivre	ע ב ֿ ר י
eyd	ע ד
eyfele	ע ו פ ֿ ע ל ע
etik	ע ט י ק
iber-yor	ע י ב ו ר ־ י א ָ ר
ile	ע י ל ו י
eyn-hore	ע י ן ־ ה ר ע
elterns	ע ל ט ע ר נ ס
elnboygn	ע ל נ ב ו י ג ן
elegye	ע ל ע ג י ע
elefant	ע ל ע פ ֿ א ַ נ ט
elektron	ע ל ע ק ט ר א ָ ן
elektrye	ע ל ע ק ט ר י ע
elektritsitet	ע ל ע ק ט ר י צ י ט ע ט
elektriker	ע ל ע ק ט ר י ק ע ר
elektre	ע ל ע ק ט ר ע
elf	ע ל ף
elftl	ע ל פ ֿ ט ל
amorets	ע ם   ה א ר ץ
amorets	ע ם ־ ה א ָ ר ץ
emigrant	ע מ י ג ר א ַ נ ט
emigratsye	ע מ י ג ר א ַ צ י ע
emer	ע מ ע ר
english	ע נ ג ל י ש
entfer	ע נ ט פ ֿ ע ר
inyen	ע נ י ן
enlekhkeyt	ע נ ל ע כ ק ײ ט
energye	ע נ ע ר ג י ע
entsiklopedye	ע נ צ י ק ל א ָ פ ּ ע ד י ע
enkavedenik	ע נ ק א ַ װ ע ד ע נ י ק
estraykher	ע ס ט ר ײ ַ כ ע ר
esey	ע ס ײ
esik	ע ס י ק
esn	ע ס ן
esperanto	ע ס פ ּ ע ר א ַ נ ט א ָ
epokhe	ע פ ּ א ָ כ ע
epidemye	ע פ ּ י ד ע מ י ע
epistemologye	ע פ ּ י ס ט ע מ א ָ ל א ָ ג י ע
epl	ע פ ּ ל
eplzaft	ע פ ּ ל ז א ַ פ ֿ ט
ekonomye	ע ק א ָ נ א ָ מ י ע
ekonomik	ע ק א ָ נ א ָ מ י ק
ekdish	ע ק ד י ש
ekvator	ע ק װ א ַ ט א ָ ר
ekzekutsye	ע ק ז ע ק ו צ י ע
eroport	ע ר א ָ פ ּ א ָ ר ט
eroplan	ע ר א ָ פ ּ ל א ַ ן
erd	ע ר ד
erdarbeter	ע ר ד א ַ ר ב ע ט ע ר
fabrik	פ ֿ א ַ ב ר י ק
fodem	פ ֿ א ָ ד ע ם
foter	פ ֿ א ָ ט ע ר
fokh	פ ֿ א ָ ך
folk	פ ֿ א ָ ל ק
folklor	פ ֿ א ָ ל ק ל א ָ ר
folksbiblyotek	פ ֿ א ָ ל ק ס ב י ב ל י א ָ ט ע ק
folksvertl	פ ֿ א ָ ל ק ס װ ע ר ט ל
folksrepublik	פ ֿ א ָ ל ק ס ר ע פ ּ ו ב ל י ק
folksshul	פ ֿ א ָ ל ק ס ש ו ל
familye	פ ֿ א ַ מ י ל י ע
fon	פ ֿ א ָ ן
fonologye	פ ֿ א ָ נ א ָ ל א ָ ג י ע
fonetik	פ ֿ א ָ נ ע ט י ק
fasolye	פ ֿ א ַ ס א ָ ל י ע
fosil	פ ֿ א ָ ס י ל
fosfat	פ ֿ א ָ ס פ ֿ א ַ ט
fakt	פ ֿ א ַ ק ט
faktor	פ ֿ א ַ ק ט א ָ ר
fareyn	פ ֿ א ַ ר א ײ ן
fareynikung	פ ֿ א ַ ר א ײ נ י ק ו נ ג
farb	פ ֿ א ַ ר ב
farband	פ ֿ א ַ ר ב א ַ נ ד
farbeserung	פ ֿ א ַ ר ב ע ס ע ר ו נ ג
farbrekh	פ ֿ א ַ ר ב ר ע ך
farbrekher	פ ֿ א ַ ר ב ר ע כ ע ר
farbrengen	פ ֿ א ַ ר ב ר ע נ ג ע ן
fargang	פ ֿ א ַ ר ג א ַ נ ג
fargangenheyt	פ ֿ א ַ ר ג א ַ נ ג ע נ ה ײ ט
fargevaldigung	פ ֿ א ַ ר ג ע װ א ַ ל ד י ג ו נ ג
fargenign	פ ֿ א ַ ר ג ע נ י ג ן
fartog	פ ֿ א ַ ר ט א ָ ג
farlag	פ ֿ א ַ ר ל א ַ ג
farmer	פ ֿ א ַ ר מ ע ר
forem	פ ֿ א ָ ר ע ם
farfl	פ ֿ א ַ ר פ ֿ ל
forts	פ ֿ א ָ ר ץ
farkoyf	פ ֿ א ַ ר ק ו י ף
farkoyfer	פ ֿ א ַ ר ק ו י פ ֿ ע ר
farkoyferin	פ ֿ א ַ ר ק ו י פ ֿ ע ר י ן
forshung	פ ֿ א ָ ר ש ו נ ג
forshtelung	פ ֿ א ָ ר ש ט ע ל ו נ ג
fashizm	פ ֿ א ַ ש י ז ם
fashist	פ ֿ א ַ ש י ס ט
pakhve	פ א כ װ ע
far	פ א ר
pogrom	פ ּ א ָ ג ר א ָ ם
podloge	פ ּ א ָ ד ל א ָ ג ע
podkove	פ ּ א ָ ד ק א ָ װ ע
podkeve	פ ּ א ָ ד ק ע װ ע
peye	פ ּ א ַ ה
povidle	פ ּ א ָ װ י ד ל ע
pave	פ ּ א ַ װ ע
potasyum	פ ּ א ָ ט א ַ ס י ו ם
patlezhan	פ ּ א ַ ט ל ע ז ש א ַ ן
potkeve	פ ּ א ָ ט ק ע װ ע
patryot	פ ּ א ַ ט ר י א ָ ט
patryotizm	פ ּ א ַ ט ר י א ָ ט י ז ם
poyezd	פ ּ א ָ י ע ז ד
pakhve	פ ּ א ַ כ װ ע
palats	פ ּ א ַ ל א ַ ץ
polyaner	פ ּ א ָ ל י א ַ נ ע ר
polyak	פ ּ א ָ ל י א ַ ק
politik	פ ּ א ָ ל י ט י ק
politsyant	פ ּ א ָ ל י צ י א ַ נ ט
politsey	פ ּ א ָ ל י צ ײ
politsist	פ ּ א ָ ל י צ י ס ט
politse	פ ּ א ָ ל י צ ע
paleontologye	פ ּ א ַ ל ע א ָ נ ט א ָ ל א ָ ג י ע
palestiner	פ ּ א ַ ל ע ס ט י נ ע ר
pomidor	פ ּ א ָ מ י ד א ָ ר
pomerants	פ ּ א ָ מ ע ר א ַ נ ץ
pompernikl	פ ּ א ָ מ פ ּ ע ר נ י ק ל
pan	פ ּ א ַ ן
pani	פ ּ א ַ נ י
post	פ ּ א ָ ס ט
pastushke	פ ּ א ַ ס ט ו ש ק ע
pastekh	פ ּ א ַ ס ט ע ך
pastrami	פ ּ א ַ ס ט ר א ַ מ י
pastrame	פ ּ א ַ ס ט ר א ַ מ ע
paskhe	פ ּ א ַ ס כ ע
paskudnyak	פ ּ א ַ ס ק ו ד נ י א ַ ק
poet	פ ּ א ָ ע ט
poem	פ ּ א ָ ע ם
poeme	פ ּ א ָ ע מ ע
popugay	פ ּ א ָ פ ּ ו ג ײ ַ
papiresl	פ ּ א ַ פ ּ י ר ע ס ל
paprike	פ ּ א ַ פ ּ ר י ק ע
pots	פ ּ א ָ ץ
pokn	פ ּ א ָ ק ן
por	פ ּ א ָ ר
paradoks	פ ּ א ַ ר א ַ ד א ָ ק ס
parodist	פ ּ א ַ ר א ָ ד י ס ט
parodye	פ ּ א ַ ר א ָ ד י ע
parapsikhologye	פ ּ א ַ ר א ַ פ ּ ס י כ א ָ ל א ָ ג י ע
partizan	פ ּ א ַ ר ט י ז א ַ ן
partey	פ ּ א ַ ר ט ײ
parizer	פ ּ א ַ ר י ז ע ר
porl	פ ּ א ָ ר ל
pornografye	פ ּ א ָ ר נ א ָ ג ר א ַ פ ֿ י ע
pare	פ ּ א ַ ר ע
park	פ ּ א ַ ר ק
parshoyn	פ ּ א ַ ר ש ו י ן
pasha	פ ּ א ַ ש א ַ
peye	פ ּ א ה
pud	פ ּ ו ד
puter	פ ּ ו ט ע ר
poylish	פ ּ ו י ל י ש
poyer	פ ּ ו י ע ר
poyps	פ ּ ו י פ ּ ס
poyk	פ ּ ו י ק
poykshtekl	פ ּ ו י ק ש ט ע ק ל
punkt	פ ּ ו נ ק ט
purimshpil	פ ּ ו ר י מ ש פ ּ י ל
pyavke	פ ּ י א ַ װ ק ע
pyate	פ ּ י א ַ ט ע
pyon	פ ּ י א ָ ן
pyane	פ ּ י א ַ נ ע
pizhames	פ ּ י ז ש א ַ מ ע ס
pilot	פ ּ י ל א ָ ט
foyst	פ ֿ ו י ס ט
pistoyl	פ ּ י ס ט ו י ל
pisk	פ ּ י ס ק
pyese	פ ּ י ע ס ע
pipernoter	פ ּ י פ ּ ע ר נ א ָ ט ע ר
pitse	פ ּ י צ ע
pikholts	פ ּ י ק ה א ָ ל ץ
peyresh	פ ּ י ר ו ש
pirushke	פ ּ י ר ו ש ק ע
platinum	פ ּ ל א ַ ט י נ ו ם
planete	פ ּ ל א ַ נ ע ט ע
plats	פ ּ ל א ַ ץ
pluto	פ ּ ל ו ט א ָ
plutonyum	פ ּ ל ו ט א ָ נ י ו ם
plyush	פ ּ ל י ו ש
polet	פ ּ ל י ט
pleyte	פ ּ ל י ט ה
pleytse	פ ּ ל ײ צ ע
plimenitse	פ ּ ל י מ ע נ י צ ע
plimenik	פ ּ ל י מ ע נ י ק
pletsl	פ ּ ל ע צ ל
plishti	פ ּ ל ש ת ּ י
fungus	פ ֿ ו נ ג ו ס
ponem	פ ּ נ י ם
funk	פ ֿ ו נ ק
fus	פ ֿ ו ס
fusbal	פ ֿ ו ס ב א ַ ל
fusbol	פ ֿ ו ס ב א ָ ל
fusbalist	פ ֿ ו ס ב א ַ ל י ס ט
fusbolist	פ ֿ ו ס ב א ָ ל י ס ט
posek	פ ּ ס ו ק
psikhoanaliz	פ ּ ס י כ א ָ א ַ נ א ַ ל י ז
psikhoanalitiker	פ ּ ס י כ א ָ א ַ נ א ַ ל י ט י ק ע ר
psikholog	פ ּ ס י כ א ָ ל א ָ ג
psikhologye	פ ּ ס י כ א ָ ל א ָ ג י ע
psikhe	פ ּ ס י כ ע
pedler	פ ּ ע ד ל ע ר
pelikan	פ ּ ע ל י ק א ַ ן
pelts	פ ּ ע ל ץ
pempik	פ ּ ע מ פ ּ י ק
penis	פ ּ ע נ י ס
peso	פ ּ ע ס א ָ
peklfleysh	פ ּ ע ק ל פ ֿ ל ײ ש
perl	פ ּ ע ר ל
fuftsikstl	פ ֿ ו פ ֿ צ י ק ס ט ל
fuftsntl	פ ֿ ו פ ֿ צ נ ט ל
problem	פ ּ ר א ָ ב ל ע ם
probe	פ ּ ר א ָ ב ע
proze	פ ּ ר א ָ ז ע
proton	פ ּ ר א ָ ט א ָ ן
prolog	פ ּ ר א ָ ל א ָ ג
pronom	פ ּ ר א ָ נ א ָ ם
profesor	פ ּ ר א ָ פ ֿ ע ס א ָ ר
profesye	פ ּ ר א ָ פ ֿ ע ס י ע
prof'	פ ּ ר א ָ פ ֿ ׳
protsent	פ ּ ר א ָ צ ע נ ט
praktik	פ ּ ר א ַ ק ט י ק
prusak	פ ּ ר ו ס א ַ ק
prat	פ ּ ר ט
priz	פ ּ ר י ז
priziv	פ ּ ר י ז י װ
prizivnik	פ ּ ר י ז י װ נ י ק
prints	פ ּ ר י נ ץ
printsip	פ ּ ר י נ צ י פ ּ
printsesin	פ ּ ר י נ צ ע ס י ן
pripetshik	פ ּ ר י פ ּ ע ט ש י ק
prezident	פ ּ ר ע ז י ד ע נ ט
premye	פ ּ ר ע מ י ע
premyer	פ ּ ר ע מ י ע ר
prepozitsye	פ ּ ר ע פ ּ א ָ ז י צ י ע
prayz	פ ּ ר ײ ַ ז
pasekh	פ ּ ת ח
payn	פ ּ ײ ַ ן
paykl	פ ּ ײ ַ ק ל
paykler	פ ּ ײ ַ ק ל ע ר
fi	פ ֿ י
fyalke	פ ֿ י א ַ ל ק ע
fidl	פ ֿ י ד ל
fidler	פ ֿ י ד ל ע ר
fizik	פ ֿ י ז י ק
fiziker	פ ֿ י ז י ק ע ר
feygele	פ ֿ ײ ג ע ל ע
fikh	פ ֿ י ך
filolog	פ ֿ י ל א ָ ל א ָ ג
filologye	פ ֿ י ל א ָ ל א ָ ג י ע
filosofye	פ ֿ י ל א ָ ס א ָ פ ֿ י ע
film	פ ֿ י ל ם
finants	פ ֿ י נ א ַ נ ץ
finger	פ י נ ג ע ר
finger	פ ֿ י נ ג ע ר
fingerl	פ ֿ י נ ג ע ר ל
finish	פ ֿ י נ י ש
finsternish	פ ֿ י נ ס ט ע ר נ י ש
finftl	פ ֿ י נ פ ֿ ט ל
fintsternish	פ ֿ י נ צ ט ע ר נ י ש
fisl	פ ֿ י ס ל
firzogerin	פ ֿ י ר ז א ָ ג ע ר י ן
firme	פ ֿ י ר מ ע
firer	פ ֿ י ר ע ר
fish	פ ֿ י ש
fisher	פ ֿ י ש ע ר
flaterl	פ ֿ ל א ַ ט ע ר ל
flam	פ ֿ ל א ַ ם
flash	פ ֿ ל א ַ ש
floy	פ ֿ ל ו י
floym	פ ֿ ל ו י ם
fli	פ ֿ ל י
flig	פ ֿ ל י ג
fligl	פ ֿ ל י ג ל
fleyt	פ ֿ ל ײ ט
fleytist	פ ֿ ל ײ ט י ס ט
flifeld	פ ֿ ל י פ ֿ ע ל ד
fledermoyz	פ ֿ ל ע ד ע ר מ ו י ז
februar	פ ֿ ע ב ר ו א ַ ר
feder	פ ֿ ע ד ע ר
federatsye	פ ֿ ע ד ע ר א ַ צ י ע
feld	פ ֿ ע ל ד
felyeton	פ ֿ ע ל י ע ט א ָ ן
felyetonist	פ ֿ ע ל י ע ט א ָ נ י ס ט
feminizm	פ ֿ ע מ י נ י ז ם
feminist	פ ֿ ע מ י נ י ס ט
fentster	פ ֿ ע נ צ ט ע ר
fefer	פ ֿ ע פ ֿ ע ר
fets	פ ֿ ע ץ
ferd	פ ֿ ע ר ד
ferdbin	פ ֿ ע ר ד ב י ן
ferdl	פ ֿ ע ר ד ל
fertl	פ ֿ ע ר ט ל
feris-rod	פ ֿ ע ר י ס ־ ר א ָ ד
fertsikstl	פ ֿ ע ר צ י ק ס ט ל
fertsntl	פ ֿ ע ר צ נ ט ל
fragment	פ ֿ ר א ַ ג מ ע נ ט
frage	פ ֿ ר א ַ ג ע
frantsoyz	פ ֿ ר א ַ נ צ ו י ז
frantseyzish	פ ֿ ר א ַ נ צ ײ ז י ש
froy	פ ֿ ר ו י
frukht	פ ֿ ר ו כ ט
frumak	פ ֿ ר ו מ א ַ ק
frumkayt	פ ֿ ר ו מ ק ײ ַ ט
freyd	פ ֿ ר ײ ד
friling	פ ֿ ר י ל י נ ג
frimorgn	פ ֿ ר י מ א ָ ר ג ן
frishtik	פ ֿ ר י ש ט י ק
fregtseykhn	פ ֿ ר ע ג צ ײ כ ן
freser	פ ֿ ר ע ס ע ר
fraytik	פ ֿ ר ײ ַ ט י ק
fraynd	פ ֿ ר ײ ַ נ ד
frayndin	פ ֿ ר ײ ַ נ ד י ן
frayndshaft	פ ֿ ר ײ ַ נ ד ש א ַ פ ֿ ט
fayl	פ ֿ ײ ַ ל
fayer	פ ֿ ײ ַ ע ר
fayerl	פ ֿ ײ ַ ע ר ל
fayertsung	פ ֿ ײ ַ ע ר צ ו נ ג
fayfl	פ ֿ ײ ַ פ ֿ ל
f'	פ ֿ ׳
tsol	צ א ָ ל
tsam	צ א ַ ם
tsondokter	צ א ָ נ ד א ָ ק ט ע ר
tsop	צ א ָ פ ּ
tsatske	צ א ַ צ ק ע
tsar	צ א ַ ר
tsarine	צ א ַ ר י נ ע
tsaritse	צ א ַ ר י צ ע
tsadek	צ ד י ק
tsedoke	צ ד ק ה
tsadeykes	צ ד ק ת
tsug	צ ו ג
tsuherer	צ ו ה ע ר ע ר
tsvantsikstl	צ װ א ַ נ צ י ק ס ט ל
tsvorekh	צ װ א ָ ר ע ך
tsvit	צ װ י ט
tsviling	צ װ י ל י נ ג
tsvinter	צ װ י נ ט ע ר
tsvelftl	צ װ ע ל פ ֿ ט ל
tsvayg	צ װ ײ ַ ג
tsvaygmeser	צ װ ײ ַ ג מ ע ס ע ר
tsung	צ ו נ ג
tsukunft	צ ו ק ו נ פ ֿ ט
tsuker	צ ו ק ע ר
tsure	צ ו ר ה
tsibele	צ י ב ע ל ע
tsig	צ י ג
tsigar	צ י ג א ַ ר
tsigl	צ י ג ל
tsigayner	צ י ג ײ ַ נ ע ר
tsienizm	צ י ו נ י ז ם
tsienist	צ י ו נ י ס ט
tsitron	צ י ט ר א ָ ן
tsitrin	צ י ט ר י ן
tseykhn	צ ײ כ ן
tseynshtekher	צ ײ נ ש ט ע כ ע ר
tsil	צ י ל
tsilinder	צ י ל י נ ד ע ר
tsimukim-vayn	צ י מ ו ק י ם ־ װ ײ ַ ן
tsimes	צ י מ ע ס
tsimer	צ י מ ע ר
tsimering	צ י מ ע ר י נ ג
tsink	צ י נ ק
tsienist	צ י ע נ י ס ט
tsienist	צ י ִ ע נ י ס ט
tsits	צ י ץ
tsitsis	צ י צ י ת
tsitsl	צ י צ ל
tsitse	צ י צ ע
tsir	צ י ר
tsnies	צ נ י ע ו ת
tsederboym	צ ע ד ע ר ב ו י ם
tsendlik	צ ע נ ד ל י ק
tsenzus	צ ע נ ז ו ס
tsentimeter	צ ע נ ט י מ ע ט ע ר
tsentl	צ ע נ ט ל
tsenerling	צ ע נ ע ר ל י נ ג
tsar	צ ע ר
tserkve	צ ע ר ק װ ע
tsofn	צ פ ֿ ו ן
tsore	צ ר ה
tsayt	צ ײ ַ ט
tsaytforme	צ ײ ַ ט פ ֿ א ָ ר מ ע
kobalt	ק א ָ ב א ַ ל ט
kagebeshnik	ק א ַ ג ע ב ע ש נ י ק
kovadle	ק א ָ װ א ַ ד ל ע
koval	ק א ָ װ א ַ ל
kavyar	ק א ַ װ י א ַ ר
kovel	ק א ָ װ ע ל
kavenik	ק א ַ װ ע נ י ק
kozak	ק א ָ ז א ַ ק
katalog	ק א ַ ט א ַ ל א ָ ג
kategorye	ק א ַ ט ע ג א ָ ר י ע
katsher	ק א ַ ט ש ע ר
katshke	ק א ַ ט ש ק ע
kokhbukh	ק א ָ כ ב ו ך
kalosh	ק א ַ ל א ָ ש
kolvirtnik	ק א ָ ל װ י ר ט נ י ק
kalyum	ק א ַ ל י ו ם
kalifyor	ק א ַ ל י פ ֿ י א ָ ר
kalike	ק א ַ ל י ק ע
kolir	ק א ָ ל י ר
kolkhoz	ק א ָ ל כ א ָ ז
kolkhoznik	ק א ָ ל כ א ָ ז נ י ק
kale	ק א ַ ל ע
koleg	ק א ָ ל ע ג
kolegin	ק א ָ ל ע ג י ן
kolege	ק א ָ ל ע ג ע
kalendar	ק א ַ ל ע נ ד א ַ ר
kaltsyum	ק א ַ ל צ י ו ם
kalkulus	ק א ַ ל ק ו ל ו ס
kalke	ק א ַ ל ק ע
kam	ק א ַ ם
kamod	ק א ַ מ א ָ ד
komod	ק א ָ מ א ָ ד
komode	ק א ָ מ א ָ ד ע
komar	ק א ָ מ א ַ ר
komunizm	ק א ָ מ ו נ י ז ם
komunist	ק א ָ מ ו נ י ס ט
komunistke	ק א ָ מ ו נ י ס ט ק ע
komitet	ק א ָ מ י ט ע ט
kamish	ק א ַ מ י ש
kome	ק א ָ מ ע
komedye	ק א ָ מ ע ד י ע
kamf	ק א ַ מ ף
kompozitor	ק א ָ מ פ ּ א ָ ז י ט א ָ ר
kampanye	ק א ַ מ פ ּ א ַ נ י ע
kompyuter	ק א ָ מ פ ּ י ו ט ע ר
kon	ק א ָ ן
kanarik	ק א ַ נ א ַ ר י ק
kongres	ק א ָ נ ג ר ע ס
konduktor	ק א ָ נ ד ו ק ט א ָ ר
kantonist	ק א ַ נ ט א ָ נ י ס ט
konte	ק א ָ נ ט ע
kontrolyor	ק א ָ נ ט ר א ָ ל י א ָ ר
kontrakt	ק א ָ נ ט ר א ַ ק ט
konyugatsye	ק א ָ נ י ו ג א ַ צ י ע
konyunktiv	ק א ָ נ י ו נ ק ט י װ
konstitutsye	ק א ָ נ ס ט י ט ו צ י ע
konferents	ק א ָ נ פ ֿ ע ר ע נ ץ
kontsert	ק א ָ נ צ ע ר ט
kastn	ק א ַ ס ט ן
kaste	ק א ַ ס ט ע
kosmos	ק א ָ ס מ א ָ ס
kapital	ק א ַ פ ּ י ט א ַ ל
kapitalizm	ק א ַ פ ּ י ט א ַ ל י ז ם
kapitalist	ק א ַ פ ּ י ט א ַ ל י ס ט
kapitl	ק א ַ פ ּ י ט ל
kapelyush	ק א ַ פ ּ ע ל י ו ש
kaper	ק א ַ פ ּ ע ר
kafe	ק א ַ פ ֿ ע
kats	ק א ַ ץ
kakao	ק א ַ ק א ַ א ָ
kaker	ק א ַ ק ע ר
kort	ק א ָ ר ט
karton	ק א ַ ר ט א ָ ן
kartofl	ק א ַ ר ט א ָ פ ֿ ל
kartoflye	ק א ַ ר ט א ָ פ ֿ ל י ע
karte	ק א ַ ר ט ע
karikatur	ק א ַ ר י ק א ַ ט ו ר
korn	ק א ָ ר ן
kornbroyt	ק א ָ ר נ ב ר ו י ט
kore	ק א ָ ר ע
karsh	ק א ַ ר ש
koshtshol	ק א ָ ש ט ש א ָ ל
koshik	ק א ָ ש י ק
koshmar	ק א ָ ש מ א ַ ר
kashe	ק א ַ ש ע
kop	ק א פ
kapele	ק א פ ע ל ע
keyver	ק ב ֿ ר
kvores	ק ב ֿ ר ו ת
kabole	ק ב ל ה
kodesh	ק ד ו ש
kdushe	ק ד ו ש ה
kadesh	ק ד י ש
kehile	ק ה י ל ה
ku	ק ו
kugl	ק ו ג ל
kvote	ק װ א ָ ט ע
kvater	ק װ א ַ ט ע ר
kvaterin	ק װ א ַ ט ע ר י ן
kval	ק װ א ַ ל
kvit	ק װ י ט
kvitung	ק װ י ט ו נ ג
kvitl	ק װ י ט ל
kveyt	ק װ ײ ט
kvetsher	ק װ ע ט ש ע ר
kutshme	ק ו ט ש מ ע
koyl	ק ו י ל
koylnshtof	ק ו י ל נ ש ט א ָ ף
koymen	ק ו י מ ע ן
koyfman	ק ו י פ ֿ מ א ַ ן
koysh	ק ו י ש
koyshbol	ק ו י ש ב א ָ ל
kukhn	ק ו כ ן
kol	ק ו ל
kultur	ק ו ל ט ו ר
kunst	ק ו נ ס ט
kunts	ק ו נ ץ
kuntsnmakher	ק ו נ צ נ מ א ַ כ ע ר
kust	ק ו ס ט
kusele	ק ו ס ע ל ע
kuper	ק ו פ ּ ע ר
kuropate	ק ו ר א ָ פ ּ א ַ ט ע
kurve	ק ו ר װ ע
kurs	ק ו ר ס
kush	ק ו ש
kibuts	ק י ב ו ץ
kidesh	ק י ד ו ש
kiyem	ק י ו ם
kitl	ק י ט ל
keyt	ק ײ ט
keyser	ק ײ ס ע ר
keyshl	ק ײ ש ל
kikh	ק י ך
kikhl	ק י כ ל
kilo	ק י ל א ָ
kilometer	ק י ל א ָ מ ע ט ע ר
kimpet	ק י מ פ ּ ע ט
kimpet-palate	ק י מ פ ּ ע ט ־ פ ּ א ַ ל א ַ ט ע
kimpetorin	ק י מ פ ּ ע ט א ָ ר י ן
kimpetkind	ק י מ פ ּ ע ט ק י נ ד
kino	ק י נ א ָ
kinbeyn	ק י נ ב ײ ן
kind	ק י נ ד
kindele	ק י נ ד ע ל ע
kinderbikhl	ק י נ ד ע ר ב י כ ל
kindergortn	ק י נ ד ע ר ג א ָ ר ט ן
kinig	ק י נ י ג
kinigin	ק י נ י ג י ן
kinigl	ק י נ י ג ל
kinstler	ק י נ ס ט ל ע ר
kinstlerin	ק י נ ס ט ל ע ר י ן
kirkh	ק י ר ך
kirkhe	ק י ר כ ע
kishinever	ק י ש י נ ע װ ע ר
kishn	ק י ש ן
kishke	ק י ש ק ע
kloglid	ק ל א ָ ג ל י ד
kloger	ק ל א ָ ג ע ר
klavyatur	ק ל א ַ װ י א ַ ט ו ר
klas	ק ל א ַ ס
klasnkamf	ק ל א ַ ס נ ק א ַ מ ף
klarnet	ק ל א ַ ר נ ע ט
klarnetist	ק ל א ַ ר נ ע ט י ס ט
klimat	ק ל י מ א ַ ט
klipe	ק ל י פ ּ ה
klole	ק ל ל ה
klezmer	ק ל ע ז מ ע ר
komets	ק מ ץ
komets-alef	ק מ ץ ־ א ַ ל ף
komets-alef	ק מ ץ ־ א ל ף
knobl	ק נ א ָ ב ל
kanoes	ק נ א ו ת
kni	ק נ י
kneydl	ק נ ײ ד ל
knish	ק נ י ש
knas	ק נ ס
knekht	ק נ ע כ ט
knekhl	ק נ ע כ ל
knayper	ק נ ײ ַ פ ּ ע ר
ksilofon	ק ס י ל א ָ פ ֿ א ָ ן
kez	ק ע ז
ketshop	ק ע ט ש א ָ פ ּ
kelner	ק ע ל נ ע ר
kelnerin	ק ע ל נ ע ר י ן
kelnershe	ק ע ל נ ע ר ש ע
kenig	ק ע נ י ג
kenigin	ק ע נ י ג י ן
kepl	ק ע פ ּ ל
ketsl	ק ע צ ל
ketsele	ק ע צ ע ל ע
kerndl	ק ע ר נ ד ל
kerper	ק ע ר פ ּ ע ר
keshene	ק ע ש ע נ ע
keshenever	ק ע ש ע נ ע װ ע ר
kro	ק ר א ָ
krab	ק ר א ַ ב
kravat	ק ר א ַ װ א ַ ט
krom	ק ר א ָ ם
kraft	ק ר א ַ פ ֿ ט
krokodil	ק ר א ָ ק א ָ ד י ל
korbn	ק ר ב ן
kroyn	ק ר ו י ן
kroynele	ק ר ו י נ ע ל ע
krupnik	ק ר ו פ ּ נ י ק
kritik	ק ר י ט י ק
kritiker	ק ר י ט י ק ע ר
krinitse	ק ר י נ י צ ע
krist	ק ר י ס ט
kripton	ק ר י פ ּ ט א ָ ן
krem	ק ר ע ם
kreml	ק ר ע מ ל
kremer	ק ר ע מ ע ר
krenitse	ק ר ע נ י צ ע
krepl	ק ר ע פ ּ ל
kreplfleysh	ק ר ע פ ּ ל פ ֿ ל ײ ש
krayd	ק ר ײ ַ ד
krayz	ק ר ײ ַ ז
kashe	ק ש י א
rob	ר א ָ ב
rog	ר א ָ ג
rogal	ר א ָ ג א ַ ל
rod	ר א ָ ד
radzha	ר א ַ ד ז ש א ַ
radyo	ר א ַ ד י א ָ
rover	ר א ָ װ ע ר
rozhinke	ר א ָ ז ש י נ ק ע
rat	ר א ַ ט
rot	ר א ָ ט
rothoyz	ר א ָ ט ה ו י ז
role	ר א ָ ל ע
rats	ר א ַ ץ
ratsyonalizm	ר א ַ צ י א ָ נ א ַ ל י ז ם
raket	ר א ַ ק ע ט
rosheshone	ר א ָ ש ־ ה ש נ ה
reb	ר ב
rov	ר ב ֿ
rebe	ר ב י
rebe-nisl	ר ב י ־ נ י ס ל
rabeynu	ר ב י נ ו
rebenyu	ר ב י נ י ו
rebetsin	ר ב י צ י ן
rabeynu	ר ב נ ו
rege	ר ג ע
rov	ר ו ב ֿ
rubl	ר ו ב ל
roygez	ר ו ג ז
ruder	ר ו ד ע ר
ruekh	ר ו ח
rukhnies	ר ו ח נ י ו ת
royz	ר ו י ז
roytarmeyer	ר ו י ט א ַ ר מ ײ ע ר
rumenish	ר ו מ ע נ י ש
rumener	ר ו מ ע נ ע ר
rus	ר ו ס
ruslender	ר ו ס ל ע נ ד ע ר
riter	ר י ט ע ר
rikhter	ר י כ ט ע ר
ring	ר י נ ג
rind	ר י נ ד
rindfleysh	ר י נ ד פ ֿ ל ײ ש
rip	ר י פ ּ
regirung	ר ע ג י ר ו נ ג
reglament	ר ע ג ל א ַ מ ע נ ט
regn	ר ע ג ן
regn-boygn	ר ע ג ן ־ ב ו י ג ן
regn-mantl	ר ע ג ן ־ מ א ַ נ ט ל
regnboygn	ר ע ג נ ב ו י ג ן
reder	ר ע ד ע ר
revolver	ר ע װ א ָ ל װ ע ר
revolutsye	ר ע װ א ָ ל ו צ י ע
retekh	ר ע ט ע ך
retekhl	ר ע ט ע כ ל
rekht	ר ע כ ט
religye	ר ע ל י ג י ע
rendl	ר ע נ ד ל
restoran	ר ע ס ט א ָ ר א ַ ן
reform	ר ע פ ֿ א ָ ר ם
republik	ר ע פ ּ ו ב ל י ק
repertuar	ר ע פ ּ ע ר ט ו א ַ ר
referendum	ר ע פ ֿ ע ר ע נ ד ו ם
rekl	ר ע ק ל
resht	ר ע ש ט
refue	ר פ ֿ ו א ה
roshe	ר ש ע
rayz	ר ײ ַ ז
raykhkayt	ר ײ ַ כ ק ײ ַ ט
reb	ר ׳
soyne	ש ׂ ו נ א
sotn	ש ׂ ט ן
sine	ש ׂ י נ א ה
skhires	ש ׂ כ י ר ו ת
seykhl	ש ׂ כ ל
skhar	ש ׂ כ ר
simkhe	ש ׂ מ ח ה
sine	ש ׂ נ א ה
sreyfe	ש ׂ ר פ ֿ ה
srore	ש ׂ ר ר ה
sho	ש א ָ
shod	ש א ָ ד
shotn	ש א ָ ט ן
shakhmat	ש א ַ כ מ א ַ ט
shokhfigur	ש א ָ כ פ ֿ י ג ו ר
shal	ש א ַ ל
shalikl	ש א ַ ל י ק ל
shande	ש א ַ נ ד ע
shos	ש א ָ ס
shakal	ש א ַ ק א ַ ל
sharbn	ש א ַ ר ב ן
shayle	ש א ל ה
sheyvet	ש ב ֿ ט
shabes	ש ב ת
sheyd	ש ד
shadkhn	ש ד כ ן
shvab	ש װ א ַ ב
shvoger	ש װ א ָ ג ע ר
shvom	ש װ א ָ ם
shvan	ש װ א ַ ן
shvants	ש װ א ַ נ ץ
shvartser	ש װ א ַ ר צ ע ר
shviger	ש װ י ג ע ר
shvits	ש װ י ץ
shvitsbod	ש װ י צ ב א ָ ד
shvebl	ש װ ע ב ל
shvegerin	ש װ ע ג ע ר י ן
shvedish	ש װ ע ד י ש
shveml	ש װ ע מ ל
shvester	ש װ ע ס ט ע ר
shvesterl	ש װ ע ס ט ע ר ל
shver	ש װ ע ר
shverd	ש װ ע ר ד
shoykhet	ש ו ח ט
shoyte	ש ו ט ה
shoys	ש ו י ס
shoyshpiler	ש ו י ש פ ּ י ל ע ר
shoyshpilerin	ש ו י ש פ ּ י ל ע ר י ן
shukh	ש ו ך
shul	ש ו ל
shuld	ש ו ל ד
shule	ש ו ל ע
shuster	ש ו ס ט ע ר
shup	ש ו פ ּ
shoyfet	ש ו פ ֿ ט
shoyfer	ש ו פ ר
shoyfer	ש ו פ ֿ ר
shoyresh	ש ו ר ש
shokher	ש ח ו ר
shakhres	ש ח ר י ת
shtohl	ש ט א ָ ה ל
shtat	ש ט א ַ ט
shtot	ש ט א ָ ט
shtotgortn	ש ט א ָ ט ג א ָ ר ט ן
shtol	ש ט א ָ ל
shtapl	ש ט א ַ פ ּ ל
shtafet	ש ט א ַ פ ֿ ע ט
shtokfish	ש ט א ָ ק פ ֿ י ש
shtub	ש ט ו ב
shtoyb	ש ט ו י ב
shtoybzoyger	ש ט ו י ב ז ו י ג ע ר
shtoybmashin	ש ט ו י ב מ א ַ ש י ן
shtul	ש ט ו ל
shtup	ש ט ו פ ּ
shturem	ש ט ו ר ע ם
shtus	ש ט ו ת
shtuseray	ש ט ו ת ע ר ײ ַ
shtibl	ש ט י ב ל
shtivl	ש ט י װ ל
shteyn	ש ט ײ ן
shtik	ש ט י ק
shtikl	ש ט י ק ל
shtikshtof	ש ט י ק ש ט א ָ ף
shtetl	ש ט ע ט ל
shtekhl-khazer	ש ט ע כ ל ־ ח ז י ר
shtekhler	ש ט ע כ ל ע ר
shtekher	ש ט ע כ ע ר
shtekl	ש ט ע ק ל
shtekn	ש ט ע ק ן
shtern	ש ט ע ר ן
shtrom	ש ט ר א ָ ם
shtrudl	ש ט ר ו ד ל
shtroy	ש ט ר ו י
shtrik	ש ט ר י ק
shtrayml	ש ט ר ײ ַ מ ל
shtshav	ש ט ש א ַ װ
sheygets	ש ײ ג ע ץ
sheygetsl	ש ײ ג ע צ ל
sheyd	ש ײ ד
sheytl	ש ײ ט ל
shayekh	ש ײ ך
sheynkeyt	ש ײ נ ק ײ ט
shild	ש י ל ד
shinke	ש י נ ק ע
shif	ש י ף
shifl	ש י פ ֿ ל
shifer	ש י פ ֿ ע ר
shiferin	ש י פ ֿ ע ר י ן
shikse	ש י ק ס ע
shir	ש י ר
shire	ש י ר ה
shirem	ש י ר ע ם
shishke	ש י ש ק ע
shkhine	ש כ י נ ה
shokhn	ש כ ן
shlakht	ש ל א ַ כ ט
shlakhtman	ש ל א ַ כ ט מ א ַ ן
shlakhtfeld	ש ל א ַ כ ט פ ֿ ע ל ד
shlang	ש ל א ַ נ ג
shlos	ש ל א ָ ס
shloser	ש ל א ָ ס ע ר
shlof	ש ל א ָ ף
shloflozikeyt	ש ל א ָ פ ֿ ל א ָ ז י ק ײ ט
shloftsimer	ש ל א ָ פ ֿ צ י מ ע ר
shlafkeyt	ש ל א ַ פ ֿ ק ײ ט
shlofrok	ש ל א ָ פ ֿ ר א ָ ק
sholem	ש ל ו ם
shaleshudes	ש ל ו ש ־ ס ע ו ד ו ת
shlyukhe	ש ל י ו כ ע
shliekh	ש ל י ח
shleykes	ש ל ײ ק ע ס
shlimazlnitse	ש ל י מ ז ל נ י צ ע
shlimil	ש ל י מ י ל
shlisl	ש ל י ס ל
shlesl	ש ל ע ס ל
shmate	ש מ א ַ ט ע
shmokh	ש מ א ָ ך
shmant	ש מ א ַ נ ט
shmok	ש מ א ָ ק
shmorak	ש מ א ָ ר א ַ ק
shmues	ש מ ו ע ס
shmuts	ש מ ו ץ
shmidhamer	ש מ י ד ה א ַ מ ע ר
shmeykhl	ש מ ײ כ ל
shmir	ש מ י ר
shmegege	ש מ ע ג ע ג ע
shmek	ש מ ע ק
shmekl	ש מ ע ק ל
shmerts	ש מ ע ר ץ
shames	ש מ ש
shames	ש מ ש ׂ
shameste	ש מ ש ׂ ט ע
shnaps	ש נ א ַ פ ּ ס
shnorer	ש נ א ָ ר ע ר
shnur	ש נ ו ר
shney	ש נ ײ
shneymentsh	ש נ ײ מ ע נ ט ש
shneyele	ש נ ײ ע ל ע
shnips	ש נ י פ ּ ס
shnirl	ש נ י ר ל
shnek	ש נ ע ק
shnayder	ש נ ײ ַ ד ע ר
shnayderuk	ש נ ײ ַ ד ע ר ו ק
shnayderyung	ש נ ײ ַ ד ע ר י ו נ ג
shnayderke	ש נ ײ ַ ד ע ר ק ע
shnayderay	ש נ ײ ַ ד ע ר ײ ַ
sho	ש ע ה
sheptsh	ש ע פ ּ ט ש
sheps	ש ע פ ּ ס
sher	ש ע ר
sherl	ש ע ר ל
sherer	ש ע ר ע ר
shererin	ש ע ר ע ר י ן
shpalt	ש פ ּ א ַ ל ט
shpanyer	ש פ ּ א ַ נ י ע ר
shpyon	ש פ ּ י א ָ ן
shpyonazh	ש פ ּ י א ָ נ א ַ ז ש
shpigl	ש פ ּ י ג ל
shpiz	ש פ ּ י ז
shpitol	ש פ ּ י ט א ָ ל
shpil	ש פ ּ י ל
shpiler	ש פ ּ י ל ע ר
shpilke	ש פ ּ י ל ק ע
shpin	ש פ ּ י ן
shpinat	ש פ ּ י נ א ַ ט
shpits	ש פ ּ י ץ
shpek	ש פ ּ ע ק
shperl	ש פ ּ ע ר ל
shprakh	ש פ ּ ר א ַ ך
shprakh-visnshaft	ש פ ּ ר א ַ ך ־ װ י ס נ ש א ַ פ ֿ ט
shprakhgefil	ש פ ּ ר א ַ כ ג ע פ ֿ י ל
shprikhvort	ש פ ּ ר י כ װ א ָ ר ט
shpringer	ש פ ּ ר י נ ג ע ר
shprits	ש פ ּ ר י ץ
shpayekhts	ש פ ּ ײ ַ ע כ ץ
shpayekhts	ש פ ײ ע כ ץ
shifles	ש פ ֿ ל ו ת
shekl	ש ק ל
shklaf	ש ק ל א ַ ף
shroyf	ש ר ו י ף
shrimp	ש ר י מ פ ּ
shrift	ש ר י פ ֿ ט
shrek	ש ר ע ק
shrayber	ש ר ײ ַ ב ע ר
shtadlen	ש ת ּ ד ל ן
shtadlones	ש ת ּ ד ל נ ו ת
shayn	ש ײ ַ ן
tayve	ת ּ א ו ה
teyve	ת ּ ב ֿ ה
tom	ת ּ ה ו ם
tokh	ת ּ ו ך
tkhine	ת ּ ח י נ ה
tifle	ת ּ י פ ֿ ל ה
tikn	ת ּ י ק ו ן
tishebov	ת ּ י ש ע ה ־ ב א ָ ב ֿ
takhshet	ת ּ כ ש י ט
tliye	ת ּ ל י ה
talyen	ת ּ ל י ו ן
talyen	ת ּ ל י ן
talmetoyre	ת ּ ל מ ו ד ־ ת ּ ו ר ה
talmed	ת ּ ל מ י ד
talmid-khokhem	ת ּ ל מ י ד ־ ח כ ם
talmido	ת ּ ל מ י ד ה
tam	ת ּ ם
tamez	ת ּ מ ו ז
temimes	ת ּ מ י מ ו ת
tane	ת ּ נ א
tnay	ת ּ נ א ַ י
tones	ת ּ ע נ י ת
tfile	ת ּ פ ֿ י ל ה
tfiln	ת ּ פ ֿ י ל י ן
tfise	ת ּ פ ֿ י ס ה
tkufe	ת ּ ק ו פ ֿ ה
takone	ת ּ ק נ ה
targem	ת ּ ר ג ו ם
tshuve	ת ּ ש ו ב ֿ ה
tashlekh	ת ּ ש ל י ך
tishebov	ת ּ ש ע ה ־ ב א ָ ב ֿ
tokhes	ת ח ת