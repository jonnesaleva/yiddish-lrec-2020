אָב	o b
אָבֿינו	o v i n u
אָבלאַסט	o b l a s t
אַגענט	a g e n t
אָגער	o g e r
אַדװאָקאַט	a d v o k a t
אַדװערב	a d v e r b
אַדיעקטיװ	a d y e k t i v
אַדל	a d l
אָדלער	o d l e r
אָדער	o d e r
אָדר	o d e r
אַדרעס	a d r e s
אַװטאָגראַף	a v t o g r a f
אַװטאָר	a v t o r
אָװנט	o v n t
אָװנטשטערן	o v n t s h t e r n
אַזאָט	a z o t
אַזבוקע	a z b u k e
אָזערע	o z e r e
אָזשענע	o z h e n e
אַחריות	a k h r a y e s
אַטאָם	a t o m
אַטאַק	a t a k
אַטאַקע	a t a k e
אַטמאָספֿער	a t m o s f e r
אַטמאָספֿערע	a t m o s f e r e
אַטעיִזם	a t e i z m
אַטעיִסט	a t e i s t
אָטעם	o t e m
אַכציקסטל	a k h t s i k s t l
אַכצנטל	a k h t s n t l
אַלגעברע	a l g e b r e
אַלומיניום	a l u m i n y u m
אַלטדײַטש	a l t d a y t s h
אַלטהויכדײַטש	a l t h o y k h d a y t s h
אַלטער	a l t e r
אַליגאַטאָר	a l i g a t o r
אָליװ	o l i v
אַלמער	a l m e r
אַלעע	a l e e
אַלערגיע	a l e r g y e
אַלף	a l e f
אַלף־בית	a l e f - b e y s
אַלפֿאַבעט	a l f a b e t
אַלקאָהאָל	a l k o h o l
אַלקאָהאָליזם	a l k o h o l i z m
אַם	a m
אַמבאַסאַדע	a m b a s a d e
אַמורא	a m o y r e
אָמלעט	o m l e t
אַמנעזיע	a m n e z y e
אַמעריקאַנער	a m e r i k a n e r
אַנאַנאַס	a n a n a s
אַנאַרכיזם	a n a r k h i z m
אַנאַרכיסט	a n a r k h i s t
אָנדענק	o n d e n k
אָנהײב	o n h e y b
אָנהײבער	o n h e y b e r
אַנטאָלאָגיע	a n t o l o g y e
אָנטאָלאָגיע	o n t o l o g y e
אָנטאָן	o n t o n
אָנטײל	o n t e y l
אָנטײלונג	o n t e y l u n g
אַנטיסעמיטיזם	a n t i s e m i t i z m
אַנטיפּאַטיע	a n t i p a t y e
אַנטיק	a n t i k
אַנטלויפֿער	a n t l o y f e r
אַנעקדאָט	a n e k d o t
אָנפֿאַנגער	o n f a n g e r
אָנצינדער	o n t s i n d e r
אַנקעטע	a n k e t e
אַסטראָנמיע	a s t r o n m y e
אָסיען	o s y e n
אַספּירין	a s p i r i n
אַעראָדראָם	a e r o d r o m
אַעראָפּאָרט	a e r o p o r t
אַעראָפּלאַן	a e r o p l a n
אַפֿאָריזם	a f o r i z m
אַפּאַטיע	a p a t y e
אַפּטײקער	a p t e y k e r
אַפּיקורס	a p i k o y r e s
אָפּל	o p l
אַפּעטיט	a p e t i t
אָפּעראַציע	o p e r a t s y e
אָפּערע	o p e r e
אָפּערעטע	o p e r e t e
אַפּריל	a p r i l
אַפּריקאָס	a p r i k o s
אַפֿיקומן	a f i k o y m e n
אַפֿריקאַנער	a f r i k a n e r
אַפֿריקאַנערין	a f r i k a n e r i n
אַקאָרדעאָן	a k o r d e o n
אָקטאָבער	o k t o b e r
אָקטאָפּוס	o k t o p u s
אַקטיאָר	a k t y o r
אַקטיװיטעט	a k t i v i t e t
אַקטיװיסט	a k t i v i s t
אַקטריסע	a k t r i s e
אַקס	a k s
אָקס	o k s
אַקסל	a k s l
אָקעאַן	o k e a n
אַקער	a k e r
אַקער־אײַזן	a k e r - a y z n
אַראַביש	a r a b i s h
אַראַבער	a r a b e r
אַראָמאַט	a r o m a t
אַראַמיש	a r a m i s h
אַרבעט	a r b e t
אַרבעס	a r b e s
אַרבעסזופּ	a r b e s z u p
אַרגאָן	a r g o n
אָרגאַן	o r g a n
אָרגאַניזאַציע	o r g a n i z a t s y e
אַרויסרײד	a r o y s r e y d
אַרויסרעד	a r o y s r e d
אַרומנעם	a r u m n e m
אָרון־קודש	o r n - k o y d e s h
אָרטאָגראַפֿיע	o r t o g r a f y e
אָרטאָדאָקס	o r t o d o k s
אָרטאָדאָקסיע	o r t o d o k s i y e
אַרטיסט	a r t i s t
אַרטיסטקע	a r t i s t k e
אַרטיקל	a r t i k l
אַרטישאָק	a r t i s h o k
אַרטעריע	a r t e r y e
אָריגינאַל	o r i g i n a l
אַרכיטעקט	a r k h i t e k t
אַרכיטעקטאָר	a r k h i t e k t o r
אַרמײ	a r m e y
אַרמעניש	a r m e n i s h
אַרמענער	a r m e n e r
אָרעװניק	o r e v n i k
אָרעם	o r e m
אָרעמאַן	o r e m a n
אַרײַנפֿיר	a r a y n f i r
אַש	a s h
אַשכּנזי	a s h k e n a z i
אבֿר	e y v e r
אדון	a d o y n
אדוני	a d o y n i
אוגערקע	u g e r k e
אויג	o y g
אויגאַנעס	o y g a n e s
אויגוסט	o y g u s t
אויגן־לעדל	o y g n - l e d l
אויגן־לעפּל	o y g n - l e p l
אויגנבליק	o y g n b l i k
אויגנלעדל	o y g n l e d l
אויגנלעפּל	o y g n l e p l
אויגענעס	o y g e n e s
אויגעפּל	o y g e p l
אויװן	o y v n
אויטאָ	o y t o
אויטאָמאָביל	o y t o m o b i l
אויטאָמאַט	o y t o m a t
אויטאָר	o y t o r
אויסהערער	o y s h e r e r
אויסטער	o y s t e r
אויסלײג	o y s l e y g
אויסשטעלונג	o y s s h t e l u n g
אויער	o y e r
אויפֿרוף	o y f r u f
אויפֿשטאַנד	o y f s h t a n d
אומברענג־לאַגער	u m b r e n g - l a g e r
אונגאַריש	u n g a r i s h
אונגער	u n g e r
אונגעריש	u n g e r i s h
אונטערגאַנג	u n t e r g a n g
אונטערגרונט	u n t e r g r u n t
אונטערמענטש	u n t e r m e n t s h
אונטערנעמונג	u n t e r n e m u n g
אונטערשײד	u n t e r s h e y d
אוניװערס	u n i v e r s
אוניװערסיטעט	u n i v e r s i t e t
אופֿן	o y f n
אוצר	o y t s e r
אוקראַיִניש	u k r a i n i s h
אוקראַיִנער	u k r a i n e r
אור־אײניקל	u r - e y n i k l
אוראַנוס	u r a n u s
אורעלטערן	u r e l t e r n
אות	o s
אחרון	a k h r e n
אחריות	a k h r a y e s
איבערזעצונג	i b e r z e t s u n g
איבערזעצער	i b e r z e t s e r
אידיאָט	i d y o t
אידיליע	i d i l y e
אידעאָלאָגיע	i d e o l o g y e
אידענטיטעט	i d e n t i t e t
איטאַליענער	i t a l y e n e r
אײזל	e y z l
אײכנבוים	e y k h n b o y m
אײל	e y l
אײניקל	e y n i k l
אײנצאָל	e y n t s o l
אײעלע	e y e l e
אײראָפּעיִש	e y r o p e i s h
אײראָפּעער	e y r o p e e r
אײראָפּעערין	e y r o p e e r i n
אימאַם	i m a m
אימיגראַנט	i m i g r a n t
אימיגראַציע	i m i g r a t s y e
אינגבער	i n g b e r
אינד	i n d
אינדזל	i n d z l
אינדיק	i n d i k
אינזל	i n z l
אינזשעניר	i n z h e n i r
אינזשעניריע	i n z h e n i r y e
אינזשענער	i n z h e n e r
אינזשענעריע	i n z h e n e r y e
אינטערנעט	i n t e r n e t
אינטערנעץ	i n t e r n e t s
איניציאַטיװ	i n i t s y a t i v
אינסטיטוט	i n s t i t u t
אינפֿיניטיװ	i n f i n i t i v
אינפֿלאַציע	i n f l a t s y e
איסאָמער	i s o m e r
איסור	i s e r
איסלאַם	i s l a m
איסלאַמיסט	i s l a m i s t
איסלאַמער	i s l a m e r
אירלענדער	i r l e n d e r
אירע	i r e
אלול	e l e l
אלף	a l e f
אלף־בית	a l e f - b e y s
אמונה	e m u n e
אמורא	a m o y r e
אמת	e m e s
אפיקומן	a f i k o y m e n
ארון	o r n
אתרוג	e s r e g
אײַזן	a y z n
אײַזנבאַן	a y z n b a n
אײַזקרעם	a y z k r e m
אײַראָפּלאַן	a y r o p l a n
באָב	b o b
באָבע־זײדע	b o b e - z e y d e
באָבע־מעשׂה	b o b e - m a y s e
באַגאַזש	b a g a z h
באַגינען	b a g i n e n
באַגעגעניש	b a g e g e n i s h
באַגער	b a g e r
באַגריף	b a g r i f
באַגרעבער	b a g r e b e r
באָד	b o d
באַדאַרף	b a d a r f
באָדהיטל	b o d h i t l
באַדעקנס	b a d e k n s
באָדקאָסטום	b o d k o s t u m
באַדײַטונג	b a d a y t u n g
באַהן	b a h n
באַװעגונג	b a v e g u n g
באַזאַלט	b a z a l t
באַזוך	b a z u k h
באַזוכער	b a z u k h e r
באַטיושקע	b a t y u s h k e
באָיאַרין	b o y a r i n
באַלאַגאַן	b a l a g a n
באַלאַד	b a l a d
באַלאַדע	b a l a d e
באַלאַלײַקע	b a l a l a y k e
באַלעבאָס	b a l e b o s
באַלעבאָסטע	b a l e b o s t e
באַלעם	b a l e m
באַן	b a n
באַנאַנע	b a n a n e
באַנדזשאָ	b a n d z h o
באַנק	b a n k
באַנק־קװעטשער	b a n k - k v e t s h e r
באַנקבעטל	b a n k b e t l
באַנקיר	b a n k i r
באַנקירער	b a n k i r e r
באַס	b a s
באָסניאַק	b o s n y a k
באַסקעטבאָל	b a s k e t b o l
באַפּטיסט	b a p t i s t
באַפֿעלקערונג	b a f e l k e r u n g
באַפֿרײַונג	b a f r a y u n g
באַק	b a k
באַקטעריאָלאָגיע	b a k t e r y o l o g y e
באַר	b a r
באַרג	b a r g
באָרד	b o r d
באָרדער	b o r d e r
באָרשט	b o r s h t
באַשאַפֿונג	b a s h a f u n g
באַשאַפֿער	b a s h a f e r
באַשלוס	b a s h l u s
באַשעפֿער	b a s h e f e r
בבֿלי	b o v l i
בדחן	b a d k h n
בהלה	b e h o l e
בובע	b u b e
בובעלע	b u b e l e
בודהיזם	b u d h i z m
בודהיסט	b u d h i s t
בוהײ	b u h a y
בוהײַ	b u h a y
בויגן	b o y g n
בויגן־שיסער	b o y g n - s h i s e r
בויטשיקל	b o y t s h i k l
בויך	b o y k h
בויכרעדער	b o y k h r e d e r
בוים	b o y m
בוך	b u k h
בולבע	b u l b e
בולגאַריש	b u l g a r i s h
בוקסיר	b u k s i r
בור	b u r
בורא	b o y r e
בורג	b u r g
בוריק	b u r i k
בחור	b o k h e r
ביאָגראַפֿיע	b y o g r a f y e
ביאָכעמיע	b y o k h e m y e
ביאַלי	b i a l y
ביבליאָטעקער	b i b l y o t e k e r
ביבליאָטעקעראין	b i b l y o t e k e r i n
ביבער	b i b e r
ביוראָ	b y u r o
ביוראָקראַטיע	b y u r o k r a t y e
ביט	b i t
בײגונג	b e y g u n g
בײגעלע	b e y g e l e
בײמל	b e y m l
בײן	b e y n
ביכל	b i k h l
בילד	b i l d
בילדונג	b i l d u n g
בילעט	b i l e t
בימה	b i m e
בימקום	b i m k e m
בין	b i n
בינטל	b i n t l
ביס	b i s
ביסל	b i s l
ביסקופּ	b i s k u p
ביציקלעט	b i t s i k l e t
ביק	b i k
ביקס	b i k s
ביר	b i r
בירגער	b i r g e r
בישאָף	b i s h o f
בית	b e y s
בית־דין	b e y z - d i n
בית־עולם	b e y s - o y l e m
בכור	b k h o r
בכורה	b k h o y r e
בלאָז	b l o z
בלאַט	b l a t
בלאָטע	b l o t e
בלאָק	b l o k
בלוט	b l u t
בלוטבאָד	b l u t b o d
בלום	b l u m
בלומענטאָפּ	b l u m e n t o p
בלינצע	b l i n t s e
בליצבריװ	b l i t s b r i v
בליצבריװל	b l i t s b r i v l
בליצפּאָסט	b l i t s p o s t
בליצשלעסל	b l i t s s h l e s l
בלעטל	b l e t l
בלעך	b l e k h
בלעכער	b l e k h e r
בלײַ	b l a y
בלײַער	b l a y e r
במקום	b i m k e m
בן־מלך	b e n - m e y l e k h
בנים	b o n e m
בעזעם	b e z e m
בעט	b e t
בעל־הבית	b a l e b o s
בעל־מלאָכה	b a l m e l o k h e
בענטשליכט	b e n t s h l i k h t
בענקל	b e n k l
בעסאַראַבער	b e s a r a b e r
בעקישע	b e k i s h e
בעקער	b e k e r
בעקערײַ	b e k e r a y
בעקעשע	b e k e s h e
בער	b e r
בעריליום	b e r i l y u m
בערעזע	b e r e z e
בקשה	b a k o s h e
בראָדער	b r o d e r
בראָך	b r o k h
בראָנדז	b r o n d z
בראָנפֿן	b r o n f n
בראָנקסער	b r o n k s e r
בראשית	b r e y s h e s
ברודער	b r u d e r
ברודערשאַפֿט	b r u d e r s h a f t
ברויז	b r o y z
ברויט	b r o y t
ברויטניצע	b r o y t n i t s e
ברונעט	b r u n e t
ברונעם	b r u n e m
ברוסטקאַסטן	b r u s t k a s t n
ברוקלינער	b r u k l i n e r
בריאה	b r i e
ברידערשאַפֿט	b r i d e r s h a f t
בריװ	b r i v
ברײט	b r e y t
ברײטל	b r e y t l
ברילן	b r i l n
בריק	b r i k
בריקל	b r i k l
ברירה	b r e y r e
ברית	b r i s
ברכה	b r o k h e
ברעג	b r e g
ברעט	b r e t
ברעטל	b r e t l
ברעם	b r e m
ברײַ	b r a y
בשׂמים	b e s o m e m
בת־מלכּה	b a s - m a l k e
בת־קול	b a s - k o l
בתולה	b s u l e
בײַטש	b a y t s h
בײַכל	b a y k h l
בײַשפּיל	b a y s h p i l
גאָב	g o b
גאָט	g o t
גאָטעניו	g o t e n y u
גאַלאָן	g a l o n
גאָלד	g o l d
גאָלדשמיד	g o l d s h m i d
גאַליציאַנער	g a l i t s y a n e r
גאַליציאַנערין	g a l i t s y a n e r i n
גאָלמעסער	g o l m e s e r
גאָלמעסערל	g o l m e s e r l
גאָלקרעם	g o l k r e m
גאָמבע	g o m b e
גאָמלקע	g o m l k e
גאָנדאָלע	g o n d o l e
גאַנדז	g a n d z
גאַנז	g a n z
גאַס	g a s
גאַסט	g a s t
גאָפּל	g o p l
גאָפּל־לעפֿל	g o p l - l e f l
גאַרב	g a r b
גאַרבער	g a r b e r
גאַרבערײַ	g a r b e r a y
גאָרטן	g o r t n
גאָרטשיצע	g o r t s h i t s e
גאָרילע	g o r i l e
גאון	g o e n
גבֿורה	g v u r e
גבאי	g a b e
גבאיטע	g a b e t e
גװאַלד	g v a l d
גװאַלט	g v a l t
גוטמאַכונג	g u t m a k h u n g
גוי	g o y
גולאַג	g u l a g
גולם	g o y l e m
גומע	g u m e
גוף	g u f
גורל	g o y r l
גזלן	g a z l e n
גזשיװע	g z h i v e
גזשיװקע	g z h i v k e
גיבור	g i b e r
גיטאַר	g i t a r
גיך	g i k h
גימל	g i m l
גימנאַזיע	g i m n a z y e
גיפֿט	g i f t
גלאָבאַליזאַציע	g l o b a l i z a t s y e
גלאָז	g l o z
גלאָק	g l o k
גלוסט	g l u s t
גלות	g o l e s
גלח	g a l e k h
גליטש	g l i t s h
גליק	g l i k
גן	g a n
גנב	g a n e f
גנבֿ	g a n e f
גנבֿה	g n é y v e
געאָגראַפֿיע	g e o g r a f y e
געאָלאָגיע	g e o l o g y e
געבוירנטאָג	g e b o y r n t o g
געבורטסטאָג	g e b u r t s t o g
געבורצטאָג	g e b u r t s t o g
געגנט	g e g n t
געדאַנק	g e d a n k
געדיכט	g e d i k h t
געדערעם	g e d e r e m
געװאַלד	g e v a l d
געװאַלט	g e v a l t
געװײן	g e v e y n
געװיכט	g e v i k h t
געװינס	g e v i n s
געװער	g e v e r
געזאַנג	g e z a n g
געזונט	g e z u n t
געזיכט	g e z i k h t
געזעל	g e z e l
געזעלשאַפֿט	g e z e l s h a f t
געזעץ	g e z e t s
געטאָ	g e t o
געטראַנק	g e t r a n k
געלט	g e l t
גענדזל	g e n d z l
גענעטיק	g e n e t i k
געסל	g e s l
געסקע	g e s k e
געסרחה	g e s r o k h e
געפֿיל	g e f i l
גערויש	g e r o y s h
גערטל	g e r t l
גערטנער	g e r t n e r
גערטנערײַ	g e r t n e r a y
גערמאַנעץ	g e r m a n e t s
געשאַנק	g e s h a n k
געשװיר	g e s h v i r
געשטאַלט	g e s h t a l t
געשיכטע	g e s h i k h t e
געשמאַק	g e s h m a k
געשעפֿט	g e s h e f t
געשעפֿטסמאַן	g e s h e f t s m a n
געשפּרעך	g e s h p r e k h
גראָבאײַזן	g r o b a y z n
גראַגער	g r a g e r
גראָז	g r o z
גראַם	g r a m
גראַמאַטיק	g r a m a t i k
גראַניט	g r a n i t
גראַף	g r a f
גראַפֿיט	g r a f i t
גראַפֿין	g r a f i n
גראַפֿיניע	g r a f i n y e
גראָשן	g r o s h n
גרוזיניש	g r u z i n i s h
גרוזינער	g r u z i n e r
גרויל	g r o y l
גרופּע	g r u p e
גריבענעס	g r i b e n e s
גריװע	g r i v e
גריװקע	g r i v k e
גרײפּפֿרוט	g r e y p f r u t
גרײפּפֿרוכט	g r e y p f r u k h t
גריך	g r i k h
גריכיש	g r i k h i s h
גרינדער	g r i n d e r
גרינס	g r i n s
גריף	g r i f
גרעבער	g r e b e r
גרעט	g r e t
גרענעץ	g r e n e t s
גרײַז	g r a y z
גשמיות	g a s h m i e s
גײַסט	g a y s t
דאַטע	d a t e
דאַטשע	d a t s h e
דאָלאַר	d o l a r
דאַמקע	d a m k e
דאָנערשטיק	d o n e r s h t i k
דאָקומענט	d o k u m e n t
דאָקטאָר	d o k t o r
דאָקטער	d o k t e r
דאָקטערײַ	d o k t e r a y
דאַקס	d a k s
דאָרפֿמאַן	d o r f m a n
דאָרשט	d o r s h t
דװאָריאַנין	d v o r y a n i n
דװאָרעץ	d v o r e t s
דור	d o r
דזשאַז	d z h a z
דזשוכע	d z h u k h e
דזשיראַף	d z h i r a f
דזשעז	d z s h e z
דזשעיל	d z h e y l
דיאַדעמע	d y a d e m e
דיאַלעקט	d y a l e k t
דיאַלעקטאָלאָגיע	d y a l e k t o l o g y e
דיבוק	d i b e k
דיװידענד	d i v i d e n d
דײן	d a y e n
דיך	d i k h
דיכטונג	d i k h t u n g
דיכטער	d i k h t e r
דיל	d i l
דימענט	d i m e n t
דין	d i n
דינאָזאַװער	d i n o z a v e r
דינאַסטיע	d i n a s t y e
דיניע	d i n y e
דינסט	d i n s t
דינסטאָג	d i n s t o g
דינסטיק	d i n s t i k
דינסטמויד	d i n s t m o y d
דינקע	d i n k e
דיסקאָטעק	d i s k o t e k
דיסקרימינאַציע	d i s k r i m i n a t s y e
דיקדוק	d i k d e k
דירה	d i r e
דירה־געלט	d i r e - g e l t
דלאָניע	d l o n y e
דעלפֿין	d e l f i n
דעמאָגראַפֿיע	d e m o g r a f y e
דעמאָקראַציע	d e m o k r a t s y e
דעניש	d e n i s h
דענקער	d e n k e r
דעצעמבער	d e t s e m b e r
דעקלאַראַציע	d e k l a r a t s y e
דעקלינאַציע	d e k l i n a t s y e
דערנערונג	d e r n e r u n g
דערפֿל	d e r f l
דערקלערונג	d e r k l e r u n g
דראַמאַטורג	d r a m a t u r g
דראַמאַטורגיע	d r a m a t u r g y e
דראַמע	d r a m e
דראַקאָן	d r a k o n
דרום	d o r e m
דריטל	d r i t l
דרײדל	d r e y d l
דרעק	d r e k
דרײַסיקסטל	d r a y s i k s t l
דרײַעק	d r a y e k
דרײַצנטל	d r a y t s n t l
דײַטש	d a y t s h
דײַטשמעריזם	d a y t s h m e r i z m
האַביכט	h a b i k h t
האָבער	h o b e r
האָבערין	h o b e r i n
האָטעל	h o t e l
האַלדז	h a l d z
האָלעפּצעס	h o l e p t s e s
האָלץ	h o l t s
האָמאָסעקסואַלוטעט	h o m o s e k s u a l u t e t
האַמער	h a m e r
האָן	h o n
האַנדלער	h a n d l e r
האַנט	h a n t
האַנטבוך	h a n t b u k h
האַנטפֿלאַך	h a n t f l a k h
האָניק	h o n i k
האָניקטײגל	h o n i k t e y g l
האָפֿענונג	h o f e n u n g
האַק	h a k
האַקפֿלײש	h a k f l e y s h
האַרבסט	h a r b s t
האָרן	h o r n
האָרע	h o r e
האַרף	h a r f
האַרפֿע	h a r f e
האַרץ	h a r t s
הבֿדלה	h a v d o l e
הוט	h u t
הויז	h o y z
הויזן	h o y z n
הויט	h o y t
הויכדײַטש	h o y k h d a y t s h
הויפּטל	h o y p t l
הויפּטשטאָט	h o y p t s h t o t
הוליגאַן	h u l i g a n
הומאָר	h u m o r
הומאָריסט	h u m o r i s t
הון	h u n
הונגער	h u n g e r
הונדערטסטל	h u n d e r t s t l
הונט	h u n t
הורה	h o r e
הידראָגען	h i d r o g e n
היטל	h i t l
הײ	h e y
הײבמוטער	h e y b m u t e r
הײװן	h e y v n
הײך	h e y k h
הײל	h e y l
הײם	h e y m
הײמאַרבעט	h e y m a r b e t
הײמלאַנד	h e y m l a n d
הײמשטאָט	h e y m s h t o t
הײראַט	h e y r a t
הײשעריק	h e y s h e r i k
הילף	h i l f
הילפֿסװערב	h i l f s v e r b
הימל	h i m l
הימנע	h i m n e
הינדל	h i n d l
הינטעלע	h i n t e l e
הינטערגעסל	h i n t e r g e s l
היסטאָריע	h i s t o r y e
הירזש	h i r z h
הירש	h i r s h
המן־טאַש	h o m e n - t a s h
המן־טאַשן	h o m e n - t a s h n
העברעיש	h e b r e i s h
העברעיִש	h e b r e i s h
העלד	h e l d
העלדזל	h e l d z l
העלדין	h e l d i n
העליום	h e l y u m
העלפֿאַנד	h e l f a n d
העלפֿאַנט	h e l f a n t
העלפֿער	h e l f e r
העלפֿערין	h e l f e r i n
העמד	h e m d
הענדלער	h e n d l e r
הענטשקע	h e n t s h k e
העפֿט	h e f t
העקל	h e k l
הער	h e r
הערב	h e r b
הערינג	h e r i n g
הערצאָג	h e r t s o g
הפֿטרה	h a f t o r e
הקדמה	h a k d o m e
הרבֿ	h o r o v
הריגה	h a r i g e
השׂכּלה	h a s k o l e
הײַזל	h a y z l
הײַזקע	h a y z k e
הײַפֿיש	h a y f i s h
ה׳	h '
װאָג	v o g
װאַגאָן	v a g o n
װאַגינע	v a g i n e
װאָיעװאָדע	v o y e v o d e
װאַך	v a k h
װאָך	v o k h
װאַכנאַכט	v a k h n a k h t
װאָכנבלאַט	v o k h n b l a t
װאַלד	v a l d
װאָלף	v o l f
װאַלפֿיש	v a l f i s h
װאָלקן	v o l k n
װאַנט	v a n t
װאַנע	v a n e
װאַנץ	v a n t s
װאָנצעס	v o n t s e s
װאַסער	v a s e r
װאַסערשטאָף	v a s e r s h t o f
װאָפֿן	v o f n
װאַקאַציע	v a k a t s y e
װאַקוּום	v a k u u m
װאָר	v o r
װאָרט	v o r t
װאַרנישקעס	v a r n i s h k e s
װאָרעם	v o r e m
װאַרעמקײט	v a r e m k e y t
װאָרצל	v o r t s l
װאַרשעװער	v a r s h e v e r
װאַשצימער	v a s h t s i m e r
װאסער	v a s e r
װוּנד	v u n d
װוּנדער	v u n d e r
װוּרשט	v u r s h t
װוּרשטל	v u r s h t l
װוינונג	v o y n u n g
װיאָלע	v y o l e
װיאָסלע	v y o s l e
װיאָרסט	v y o r s t
װיג	v i g
װיגליד	v i g l i d
װיגעלע	v i g e l e
װידער	v i d e r
װידערגוטמאַכונג	v i d e r g u t m a k h u n g
װיזע	v i z e
װײ	v e y
װײדל	v e y d l
װײץ	v e y t s
װינדל	v i n d l
װינט	v i n t
װינטער	v i n t e r
װינטערצײַט	v i n t e r t s a y t
װיסן	v i s n
װיסנשאַפֿט	v i s n s h a f t
װיסנשאַפֿטלער	v i s n s h a f t l e r
װיסקײ	v i s k e y
װיִע	v i e
װיץ	v i t s
װירוס	v i r u s
װעב	v e b
װעבזײַטל	v e b z a y t l
װעג	v e g
װעגעטאַריער	v e g e t a r y e r
װעט־זײַט	v e t - z a y t
װעטער	v e t e r
װעלאָסיפּעד	v e l o s i p e d
װעלט	v e l t
װעלט־מלחמה	v e l t - m i l k h o m e
װעלטסװערטל	v e l t s v e r t l
װעלטסמענטש	v e l t s m e n t s h
װענגער	v e n g e r
װענוס	v e n u s
װעסלע	v e s l e
װערב	v e r b
װערבע	v e r b e
װערט	v e r t
װערטל	v e r t l
װערטערקלאַס	v e r t e r k l a s
װערסט	v e r s t
װערק	v e r k
װעשער	v e s h e r
װעשערין	v e s h e r i n
װײַב	v a y b
װײַבל	v a y b l
װײַבערשול	v a y b e r s h u l
װײַן	v a y n
װײַנגאָרטן	v a y n g o r t n
װײַנטרויב	v a y n t r o y b
װײַנשלבוים	v a y n s h l b o y m
װײַספֿיש	v a y s f i s h
זאָאָלאָגיע	z o o l o g y e
זאָגער	z o g e r
זאָגערין	z o g e r i n
זאָגערקע	z o g e r k e
זאַװערוכע	z a v e r u k h e
זאָטל	z o t l
זאָטלער	z o t l e r
זאַך	z a k h
זאַלב	z a l b
זאַלץ	z a l t s
זאַמד	z a m d
זאַמלבוך	z a m l b u k h
זאַמלער	z a m l e r
זאַנג	z a n g
זאַפֿט	z a f t
זאַק	z a k
זאָק	z o k
זאָרג	z o r g
זויגלינג	z o y g l i n g
זויגער	z o y g e r
זוימען	z o y m e n
זויערקרויט	z o y e r k r o y t
זולו	z u l u
זומער	z u m e r
זומער־פֿײגעלע	z u m e r - f e y g e l e
זומערצײַט	z u m e r t s a y t
זון	z u n
זון־אונטערגאַנג	z u n - u n t e r g a n g
זונאויפֿגאַנג	z u n o y f g a n g
זונה	z o y n e
זונטיק	z u n t i k
זונפֿאַרגאַנג	z u n f a r g a n g
זופּ	z u p
זוקיני	z u k i n i
זיבעציקסטל	z i b e t s i k s t l
זיבעצנטל	z i b e t s n t l
זיװג	z i v e g
זײגער	z e y g e r
זײגערל	z e y g e r l
זײדע	z e y d e
זײדע־באָבע	z e y d e - b o b e
זײף	z e y f
זילבער	z i l b e r
זין	z i n
זינגער	z i n g e r
זינגערין	z i n g e r i n
זינד	z i n d
זיסקײַט	z i s k a y t
זיצטרין	z i t s t r i n
זיצפֿלײש	z i t s f l e y s h
זיקאָרן	z i k o r n
זכּרון	z i k o r n
זלאָטע	z l o t e
זמר	z e m e r
זמרל	z e m e r l
זעברע	z e b r e
זעג	z e g
זעכציקסטל	z e k h t s i k s t l
זעכצנטל	z e k h t s n t l
זעלנער	z e l n e r
זעלצערװאַסער	z e l t s e r v a s e r
זענעפֿט	z e n e f t
זעקל	z e k l
זעקסטל	z e k s t l
זקן	z o k n
זשאַבע	z h a b e
זשוק	z h u k
זשורנאַל	z h u r n a l
זשורנאַליזם	z h u r n a l i z m
זשורנאַליסט	z h u r n a l i s t
זשורנאַליסטיק	z h u r n a l i s t i k
זשיד	z h i d
זשמעניע	z h m e n y e
זײַד	z a y d
חבֿר	k h a v e r
חבֿרה	k h e v r e
חבֿרה־מאַן	k h e v r e m a n
חבֿרהשאַפֿט	k h e v r e s h a f t
חבֿרשאַפֿט	k h a v e r s h a f t
חדר	k h e y d e r
חודש	k h o y d e s h
חול המועד	k h o l e m o y e d
חומש	k h u m e s h
חופּה	k h u p e
חוצפּה	k h u t s p e
חורבן	k h u r b n
חושך	k h o y s h e k h
חזיר	k h a z e r
חזירײַ	k h a z e r a y
חז״ל	k h a z a l
חידוש	k h i d e s h
חיה	k h a y e
חײם	k h a y e m
חילוק	k h i l e k
חינוך	k h i n e k h
חיריק	k h i r e k
חכם	k h o k h e m
חכמה	k h o k h m e
חלה	k h a l e
חלום	k h o l e m
חמץ	k h o m e t s
חן	k h e y n
חנוכּה	k h a n e k e
חסידות	k h s i d e s
חסידל	k h o s i d l
חסרון	k h i s o r n
חפֿץ	k h e y f e t s
חרוסת	k h a r o y s e s
חשבון	k h e s h b n
חשװן	k h e s h v n
חשק	k h e y s h e k
חתונה	k h a s e n e
חתימה	k h s i m e
חתן	k h o s n
חתן־כּלה	k h o s n - k a l e
טאַבאַק	t a b a k
טאָג	t o g
טאָג־בוך	t o g - b u k h
טאָגבלאַט	t o g b l a t
טאָגצײַטונג	t o g t s a y t u n g
טאָגשול	t o g s h u l
טאָװל	t o v l
טאַטויִרונג	t a t u i r u n g
טאַטע	t a t e
טאַטע־מאַמע	t a t e - m a m e
טאָכטער	t o k h t e r
טאָל	t o l
טאָמאַט	t o m a t
טאַנטע	t a n t e
טאַנץ	t a n t s
טאָפּ	t o p
טאָפּאָגראַפֿיע	t o p o g r a f y e
טאָפּיעל	t o p y e l
טאָק	t o k
טאַקטיק	t a k t i k
טאַקסי	t a k s i
טאַראַקאַן	t a r a k a n
טאַש	t a s h
טאַשנגעלט	t a s h n g e l t
טבֿת	t e y v e s
טויב	t o y b
טויזנט	t o y z n t
טויזנטסטל	t o y z n t s t l
טויט	t o y t
טוך	t u k h
טומל	t u m l
טומלער	t u m l e r
טונפֿיש	t u n f i s h
טורמע	t u r m e
טורעם	t u r e m
טיגער	t i g e r
טײ	t e y
טײג	t e y g
טײגל	t e y g l
טײטל	t e y t l
טײטלבוים	t e y t l b o y m
טײל	t e y l
טיכל	t i k h l
טינט	t i n t
טינטפֿיש	t i n t f i s h
טיר	t i r
טיש	t i s h
טלית	t a l e s
טעאַטער	t e a t e r
טעאָלאָגיע	t e o l o g y e
טעאָריע	t e o r y e
טעות	t o e s
טעכנאָלאָגיע	t e k h n o l o g y e
טעכניק	t e k h n i k
טעכניקער	t e k h n i k e r
טעלעגראַמע	t e l e g r a m e
טעלעגראַף	t e l e g r a f
טעלעװיזאָר	t e l e v i z o r
טעלעװיזיע	t e l e v i z y e
טעלעסקאָפּ	t e l e s k o p
טעלעפֿאָן	t e l e f o n
טעם	t a m
טעמע	t e m e
טעמפּל	t e m p l
טענענבוים	t e n e n b o y m
טענצל	t e n t s l
טענצער	t e n t s e r
טעקל	t e k l
טעריטאָריע	t e r i t o r y e
טערמאָמעטער	t e r m o m e t e r
טערק	t e r k
טערקלטויב	t e r k l t o y b
טראָטואַר	t r o t u a r
טראַכט	t r a k h t
טראָפּ	t r o p
טראָפֿײ	t r o f e y
טראָצקיזם	t r o t s k i z m
טראָצקיסט	t r o t s k i s t
טראַקטאָר	t r a k t o r
טרויב	t r o y b
טרוים	t r o y m
טרומײט	t r u m e y t
טרומפּײט	t r u m p e y t
טרופּע	t r u p e
טרײסט	t r e y s t
טרילאָגיע	t r i l o g y e
טרעגער	t r e g e r
טרער	t r e r
טשאַטשקע	t s h a t s h k e
טשאָלנט	t s h o l n t
טשאַרדאַש	t s h a r d a s h
טשיקאַװעס	t s h i k a v e s
טשעך	t s h e k h
טשעכיש	t s h e k h i s h
טשעמאָדאַן	t s h e m o d a n
טשעקיסט	t s h e k i s t
טשערנאָבל	t s h e r n o b l
טשערעפּאַכע	t s h e r e p a k h e
טשײַניק	t s h a y n i k
טײַװל	t a y v l
טײַװעלע	t a y v e l e
טײַטש	t a y t s h
טײַך	t a y k h
טײַסטער	t a y s t e r
יאָג	y o g
יאָגורט	y o g u r t
יאַגעדע	y a g e d e
יאָד	y o d
יאָהר	y o r
יאַנואַר	y a n u a r
יאָנץ	y o n t s
יאַפּאַניש	y a p a n i s h
יאַפּאַנער	y a p a n e r
יאָר	y o r
יאָרהונדערט	y o r h u n d e r t
יאָרטאָג	y o r t o g
יאָרטויזנט	y o r t o y z n t
יאַריד	y a r i d
יאַרמולקע	y a r m u l k e
יאַרמלקע	y a r m l k e
יאָרצענדלינג	y o r t s e n d l i n g
יאָרצענדליק	y o r t s e n d l i k
יאָרצײַט	y o r t s a y t
יאַשטשערקע	y a s h t s h e r k e
יאר	y o r
ידיעה	y e d i e
יוגנט	y u g n t
יויך	y o y k h
יולי	y u l i
יום	y o m
יום־טובֿ	y o n t e v
יונג	y u n g
יונגער־מאַן	y u n g e r - m a n
יוני	y u n i
יוניאָן	y u n y o n
יופּיטער	y u p i t e r
יחוס	y i k h u s
ײִד	y i d
ײִדישיזם	y i d i s h i z m
ײִדישיסט	y i d i s h i s t
ײִדישקײט	y i d i s h k e y t
ײִדל	y i d l
ײִדענע	y i d e n e
ײִנגל	y i n g l
ײִנגעלע	y i n g e l e
ײִראת־הכּבֿוד	y i r e s - h a k o v e d
ים	y a m
ים־שטערן	y a m - s h t e r n
יסורים	y i s u r e m
יעגער	y e g e r
יעװסעקציע	y e v s e k t s y e
יענטע	y e n t e
יעקע	y e k e
יצר־הטובֿ	y e y t s e r - h a t o y v
יצר־הרע	y e y t s e r - h o r e
ירגזון	y i r g o z n
ירושה	y e r u s h e
יריד	y a r i d
יש	y a s h
ישיבֿה	y e s h i v e
ישיבֿה־בחור	y e s h i v e - b o k h e r
ישיבֿיש	y e s h i v i s h
ישיבה	y e s h i v e
יתום	y o s e m
יתומה	y e s o y m e
כאַלװע	k h a l v e
כאָסן	k h o s n
כאַסענע	k h a s e n e
כאַפּער	k h a p e r
כּבֿוד	k o v e d
כּהונה	k e h u n e
כװאַט	k h v a t
כװאַליע	k h v a l y e
כּוח	k o y e k h
כּזית	k e z a y e s
כּח	k o y e k h
כּלה	k a l e
כוליגאַן	k h u l i g a n
כּסלו	k i s l e v
כּשרות	k a s h r e s
כּתובה	k s u b e
כּתר	k e s e r
כינעזער	k h i n e z e r
כירורגיע	k h i r u r g y e
כמאַרע	k h m a r e
כניאָק	k h n y o k
כעלמער	k h e l m e r
כעלעמער	k h e l e m e r
כעמיע	k h e m y e
כעמיקער	k h e m i k e r
כראַביע	k h r a b y e
כריזאַנטעמע	k h r i z a n t e m e
כרײן	k h r e y n
כרעמזל	k h r e m z l
כרעסטאָמאַטיע	k h r e s t o m a t y e
כשרות	k a s h r e s
כתר	k e s e r
לאַבן	l a b n
לאָגאַריטם	l o g a r i t m
לאָגיק	l o g i k
לאַורעאַט	l a u r e a t
לאַטװיש	l a t v i s h
לאַטוטניק	l a t u t n i k
לאַטקע	l a t k e
לאַם	l a m
לאָמפּ	l o m p
לאַנד	l a n d
לאַנדסמאַן	l a n d s m a n
לאַנדקאַרטע	l a n d k a r t e
לאַפּסערדאַק	l a p s e r d a k
לאָפּעטע	l o p e t e
לאַפֿער	l a f e r
לאַקס	l a k s
לאָקש	l o k s h
לאָרד	l o r d
לװיתן	l e v y o s n
לוח	l u e k h
לויב	l o y b
לויז	l o y z
לויער	l o y e r
לויתן	l e v y o s n
לולבֿ	l u l e v
לונג	l u n g
לופֿט	l u f t
לופֿטמענטש	l u f t m e n t s h
לופֿטפֿעלד	l u f t f e l d
ליאַלקע	l y a l k e
ליבע	l i b e
ליבעלע	l i b e l e
ליגן	l i g n
ליגנער	l i g n e r
ליד	l i d
ליטװינער	l i t v i n e r
ליטװיש	l i t v i s h
ליטיום	l i t i u m
ליטער	l i t e r
ליטעראַטור	l i t e r a t u r
לײב	l e y b
לײביכע	l e y b i k h e
לײטער	l e y t e r
לײם	l e y m
לײענונג	l e y e n u n g
לײענער	l e y e n e r
לײקע	l e y k e
ליכט	l i k h t
ליכטל	l i k h t l
ליליע	l i l y e
לימאָנאַד	l i m o n a d
לימוד	l i m e d
לימענע	l i m e n e
לינגװיסט	l i n g v i s t
לינגװיסטיק	l i n g v i s t i k
לינדז	l i n d z
לינקיסט	l i n k i s t
ליפּ	l i p
ליק	l i k
למדן	l a m d n
לעבל	l e b l
לעבן	l e b n
לעבער	l e b e r
לעגענדע	l e g e n d e
לעדער	l e d e r
לעזער	l e z e r
לעטיש	l e t i s h
לעמל	l e m l
לענדן	l e n d n
לעניניזם	l e n i n i z m
לעניניסט	l e n i n i s t
לעפֿל	l e f l
לעק	l e k
לעקער	l e k e r
לעקציע	l e k t s y e
לערנונג	l e r n u n g
לערנײִנגל	l e r n y i n g l
לערער	l e r e r
לערערין	l e r e r i n
לץ	l e t s
לצעכע	l e y t s e k h e
לשון	l o s h n
לשון־קודש	l o s h n - k o y d e s h
לײַבל	l a y b l
לײַװנט	l a y v n t
לײַט	l a y t
מאָביל־טעלעפֿאָן	m o b i l - t e l e f o n
מאָבילקע	m o b i l k e
מאַגנעט	m a g n e t
מאַדזשאַר	m a d z h a r
מאָטאָציקל	m o t o t s i k l
מאַטעמאַטיק	m a t e m a t i k
מאַטעריע	m a t e r y e
מאַי	m a y
מאַך	m a k h
מאָך	m o k h
מאַכאָרקע	m a k h o r k e
מאַכט	m a k h t
מאַכער	m a k h e r
מאָל	m o l
מאָלעקול	m o l e k u l
מאָלער	m o l e r
מאָלערײַ	m o l e r a y
מאַלפּע	m a l p e
מאָלצײַט	m o l t s a y t
מאַמינקע	m a m i n k e
מאַמע	m a m e
מאַמע־לשון	m a m e - l o s h n
מאָמענט	m o m e n t
מאַמעניו	m a m e n y u
מאַמעשי	m a m e s h i
מאַן	m a n
מאָנאַט	m o n a t
מאָנאָלאָג	m o n o l o g
מאַנגאָ	m a n g o
מאַנגאַנעז	m a n g a n e z
מאַנדאָלינע	m a n d o l i n e
מאַנדל	m a n d l
מאַנדלאַך	m a n d l a k h
מאַנדלבוים	m a n d l b o y m
מאַנדלבראָט	m a n d l b r o t
מאַנדלעך	m a n d l e k h
מאָנטיק	m o n t i k
מאָניזם	m o n i z m
מאָניסט	m o n i s t
מאַנסביל	m a n s b i l
מאַנספּאַרשוין	m a n s p a r s h o y n
מאָס	m o s
מאַסלינע	m a s l i n e
מאָסקװער	m o s k v e r
מאָראַל	m o r a l
מאַראַנץ	m a r a n t s
מאָרגן	m o r g n
מאָרגנשטערן	m o r g n s h t e r n
מאָרדע	m o r d e
מאַרך	m a r k h
מאַרמאַלאַד	m a r m a l a d
מאַרמאָלאַד	m a r m o l a d
מאַרמאָר	m a r m o r
מאַרמעלאַד	m a r m e l a d
מאַרמער	m a r m e r
מאַרץ	m a r t s
מאַרק	m a r k
מאַרקסיזם	m a r k s i z m
מאַרקסיסט	m a r k s i s t
מאַרש	m a r s h
מאַשין	m a s h i n
מאדים	m a d i m
מאכל	m a y k h l
מאמר	m a y m e r
מבֿין	m e y v n
מגילה	m e g i l e
מדבר	m i d b e r
מדינה	m e d i n e
מדרגה	m a d r e y g e
מדרש	m e d r a s h
מוהל	m o y e l
מװת	m o v e s
מוזײ	m u z e y
מוזיק	m u z i k
מוזיקער	m u z i k e r
מוח	m o y e k h
מוט	m u t
מוטער	m u t e r
מוטער־צײכן	m u t e r - t s e y k h n
מוטער־שפּראַך	m u t e r - s h p r a k h
מוטערטראַכט	m u t e r t r a k h t
מוטערלאַנד	m u t e r l a n d
מוטערשײד	m u t e r s h e y d
מוטערשפּראַך	m u t e r s h p r a k h
מויד	m o y d
מויז	m o y z
מויל	m o y l
מוכר־ספֿרים	m o y k h e r - s f o r i m
מוסולמאַן	m u s u l m a n
מוסקל	m u s k l
מורא	m o y r e
מוראַשקע	m u r a s h k e
מזוזה	m e z u z e
מחבר	m e k h a b e r
מחותּן	m e k h u t n
מחותּנתטע	m e k h u t e n e s t e
מחיה	m e k h á y e
מחלוקת	m a k h l o y k e s
מטבע	m a t b e y e
מיאַטע	m y a t e
מיאוסקײט	m e y e s k e y t
מידקײט	m i d k e y t
מיט	m i t
מיטה	m i t e
מיטװאָך	m i t v o k h
מיטל	m i t l
מיטלהויכדײַטש	m i t l h o y k h d a y t s h
מיטלשול	m i t l s h u l
מיטן	m i t n
מײדל	m e y d l
מײדעלע	m e y d e l e
מײנונג	m e y n u n g
מילגרוים	m i l g r o y m
מיליאָן	m i l y o n
מיליאָנער	m i l y o n e r
מיליאַרד	m i l y a r d
מיליגראַם	m i l i g r a m
מיליטאַנט	m i l i t a n t
מיליטער	m i l i t e r
מיליליטער	m i l i l i t e r
מילימעטער	m i l i m e t e r
מילך	m i l k h
מילכיגער	m i l k h i g e r
מילכיקער	m i l k i k e r
מילעך	m i l e k h
מין	m i n
מינאַרעט	m i n a r e t
מינוט	m i n u t
מיניסטער	m i n i s t e r
מינסקער	m i n s k e r
מינעראַל־װאַסער	m i n e r a l - v a s e r
מינץ	m i n t s
מיסטיציזם	m i s t i t s i z m
מיענטע	m y e n t e
מיקראָפֿילם	m i k r o f i l m
מירמל	m i r m l
מישנה	m i s h n e
מישפּאָכע	m i s h p o k h e
מכּבי	m a k a b i
מכּבײער	m a k a b e y e r
מכּה	m a k e
מלאָכה	m e l o k h e
מלאך	m a l e k h
מלאך־המות	m a l a k h a m o v e s
מלוכה	m e l u k h e
מלחמה	m i l k h o m e
מלך	m e y l e k h
מלכה	m a l k e
מלכּה	m a l k e
מלמד	m e l a m e d
ממשלה	m e m s h o l e
מן	m a n
מנגן	m e n a g a n
מנהג	m i n e g
מנורה	m n o y r e
מנחה	m i n k h e
מנין	m i n y e n
מסורה	m e s o y r e
מסירה	m e s i r e
מסכתא	m e s e k h t e
מסכתּא	m e s e k h t e
מספּר	m i s p e r
מעבל	m e b l
מעדיקאַמענט	m e d i k a m e n t
מעטאָד	m e t o d
מעטאָדיסט	m e t o d i s t
מעטאָדיק	m e t o d i k
מעטאָדע	m e t o d e
מעטאַפֿיזיק	m e t a f i z i k
מעטעאָראָלאָגיע	m e t e o r o l o g y e
מעטראָ	m e t r o
מעטשעט	m e t s h e t
מעכאַניקער	m e k h a n i k e r
מעל	m e l
מעלאָן	m e l o n
מעלה	m a y l e
מעמואַרן	m e m u a r n
מענטש	m e n t s h
מעסער	m e s e r
מעקלער	m e k l e r
מער	m e r
מערב	m a y r e v
מערהײט	m e r h e y t
מערכה	m a r o k h e
מערצאָל	m e r t s o l
מעש	m e s h
מעשׂה	m a y s e / m a n s e
מעשה	m a y s e / m a n s e
מפֿטיר	m a f t e r
מצה	m a t s e
מציאה	m e t s i y e
מקװה	m i k v e
מקום	m o k e m
משׂכּיל	m a s k i l
משוגענער	m e s h u g e n e r
משוגעת	m e s h u g a s
משיח	m o s h i y e k h
משל	m o s h l
משפּחה	m i s h p o k h e
משפחה	m i s h p o k h e
משקה	m a s h k e
מת	m e y s
מתּנה	m a t o n e
מתנגד	m i s n a g e d
מײַ	m a y
מײַל	m a y l
מײַסטער	m a y s t e r
נאָגל	n o g l
נאָדל	n o d l
נאָװעמבער	n o v e m b e r
נאָז	n o z
נאָזלאָך	n o z l o k h
נאַטור	n a t u r
נאַכט	n a k h t
נאַכטהעמד	n a k h t h e m d
נאָכלויפֿער	n o k h l o y f e r
נאָמען	n o m e n
נאָס	n o s
נאַציאָנאַל־סאָציאַליזם	n a t s y o n a l - s o t s y a l i z m
נאַציאָנאַליזם	n a t s y o n a l i z m
נאַציזם	n a t s i z m
נאַקן	n a k n
נאַר	n a r
נאַרישקײט	n a r i s h k e y t
נבֿואה	n e v u e
נבֿיא	n o v i
נבֿיאה	n e v i y e
נגיד	n g i d
נואף	n o y e f
נודניק	n u d n i k
נומער	n u m e r
נוס	n u s
נוסח	n u s e k h
נוקלעאָן	n u k l e o n
נחת	n a k h e s
ניאוף	n i e f
ניגון	n i g n
ניגונדל	n i g n d l
ניהיליסט	n i h i l i s t
ניװאָ	n i v o
ניטל	n i t l
ניטלבוים	n i t l b o y m
נײטראָן	n e y t r o n
נײמאַשין	n e y m a s h i n
ניס	n i s
ניסל	n i s l
ניסן	n i s n
ניסערײַ	n i s e r a y
ניקוד	n i k e d
ניקל	n i k l
ניר	n i r
נס	n e s
נעאָן	n e o n
נעביש	n e b i s h
נעגער	n e g e r
נעװראָלאָגיע	n e v r o l o g y e
נעסט	n e s t
נעפּטון	n e p t u n
נעפּטוניום	n e p t u n y u m
נעפּל	n e p l
נעץ	n e t s
נערװ	n e r v
נשמה	n e s h o m e
נײַנטל	n a y n t l
נײַנציקסטל	n a y n t s i k s t l
נײַנצנטל	n a y n t s n t l
נײַעס	n a y e s
סאָד	s o d
סאָדע	s o d e
סאָװיעט	s o v y e t
סאָװע	s o v e
סאָװעטיזאַציע	s o v e t i z a t s y e
סאַזשלקע	s a z h l k e
סאַטורן	s a t u r n
סאַלאַט	s a l a t
סאָלדאַט	s o l d a t
סאַמאָדערזשאַװיע	s a m o d e r z h a v y e
סאָסנע	s o s n e
סאָפֿע	s o f e
סאָציאָלאָג	s o t s y o l o g
סאָציאַליזם	s o t s y a l i z m
סאָציאַליסט	s o t s y a l i s t
סאַקסאָפֿאָן	s a k s o f o n
סאָראָקע	s o r o k e
סבֿיבֿה	s v i v e
סדר	s e y d e r
סובאָטניק	s u b o t n i k
סובסטאַנטיװ	s u b s t a n t i v
סוד	s o d
סולטאַן	s u l t a n
סוף	s o f
סופֿר	s o y f e r
סחורה	s k h o y r e
סטאַװ	s t a v
סטאַטיסטיק	s t a t i s t i k
סטאָליאַר	s t o l y a r
סטאַליניזם	s t a l i n i z m
סטאַליניסט	s t a l i n i s t
סטאָליער	s t o l y e r
סטאַשקע	s t a s h k e
סטאַשקעשמיר	s t a s h k e s h m i r
סטודיאָ	s t u d y o
סטודיע	s t u d y e
סטודענט	s t u d e n t
סטודענטין	s t u d e n t i n
סטודענטקע	s t u d e n t k e
סטרוזש	s t r u z h
סטרונע	s t r u n e
סיבירער	s i b i r e r
סידור	s i d e r
סיװן	s i v n
סײם	s e y m
סײמיסט	s e y m i s t
סיליציום	s i l i t s y u m
סיליקאָן	s i l i k o n
סימבאָל	s i m b o l
סימן	s i m e n
סינאַגאָגע	s i n a g o g e
סיראָפּ	s i r o p
סירופּ	s i r u p
סלאָװאַק	s l o v a k
סלאָװאַקיש	s l o v a k i s h
סם	s a m
סמאָקינג	s m o k i n g
סמאַראַגד	s m a r a g d
סמיכה	s m i k h e
סמעטענע	s m e t e n e
סנאָפּ	s n o p
סעודה	s u d e
סעלעריע	s e l e r y e
סענאַט	s e n a t
סענאַטאָר	s e n a t o r
סעניאָר	s e n y o r
סעניאָראַ	s e n y o r a
סעפּטעמבער	s e p t e m b e r
סעקונדע	s e k u n d e
סעקטע	s e k t e
סערביש	s e r b i s h
סערװעטקע	s e r v e t k e
סעריע	s e r y e
ספּאָדניצע	s p o d n i t s e
ספּאָדעק	s p o d e k
ספּאַרזשע	s p a r z h e
ספּודניצע	s p u d n i t s e
ספּיריטואַליזם	s p i r i t u a l i z m
ספּעקטאַקל	s p e k t a k l
ספֿר	s e y f e r
סקאָרפּיאָן	s k o r p y o n
סתּירה	s t i r e
עבֿרה	a v e y r e
עבֿרי	i v r e
עד	e y d
עופֿעלע	e y f e l e
עטיק	e t i k
עיבור־יאָר	i b e r - y o r
עילוי	i l e
עין־הרע	e y n - h o r e
עלטערנס	e l t e r n s
עלנבויגן	e l n b o y g n
עלעגיע	e l e g y e
עלעפֿאַנט	e l e f a n t
עלעקטראָן	e l e k t r o n
עלעקטריע	e l e k t r y e
עלעקטריציטעט	e l e k t r i t s i t e t
עלעקטריקער	e l e k t r i k e r
עלעקטרע	e l e k t r e
עלף	e l f
עלפֿטל	e l f t l
עם הארץ	a m o r e t s
עם־האָרץ	a m o r e t s
עמיגראַנט	e m i g r a n t
עמיגראַציע	e m i g r a t s y e
עמער	e m e r
ענגליש	e n g l i s h
ענטפֿער	e n t f e r
ענין	i n y e n
ענלעכקײט	e n l e k h k e y t
ענערגיע	e n e r g y e
ענציקלאָפּעדיע	e n t s i k l o p e d y e
ענקאַװעדעניק	e n k a v e d e n i k
עסטרײַכער	e s t r a y k h e r
עסײ	e s e y
עסיק	e s i k
עסן	e s n
עספּעראַנטאָ	e s p e r a n t o
עפּאָכע	e p o k h e
עפּידעמיע	e p i d e m y e
עפּיסטעמאָלאָגיע	e p i s t e m o l o g y e
עפּל	e p l
עפּלזאַפֿט	e p l z a f t
עקאָנאָמיע	e k o n o m y e
עקאָנאָמיק	e k o n o m i k
עקדיש	e k d i s h
עקװאַטאָר	e k v a t o r
עקזעקוציע	e k z e k u t s y e
עראָפּאָרט	e r o p o r t
עראָפּלאַן	e r o p l a n
ערד	e r d
ערדאַרבעטער	e r d a r b e t e r
פֿאַבריק	f a b r i k
פֿאָדעם	f o d e m
פֿאָטער	f o t e r
פֿאָך	f o k h
פֿאָלק	f o l k
פֿאָלקלאָר	f o l k l o r
פֿאָלקסביבליאָטעק	f o l k s b i b l y o t e k
פֿאָלקסװערטל	f o l k s v e r t l
פֿאָלקסרעפּובליק	f o l k s r e p u b l i k
פֿאָלקסשול	f o l k s s h u l
פֿאַמיליע	f a m i l y e
פֿאָן	f o n
פֿאָנאָלאָגיע	f o n o l o g y e
פֿאָנעטיק	f o n e t i k
פֿאַסאָליע	f a s o l y e
פֿאָסיל	f o s i l
פֿאָספֿאַט	f o s f a t
פֿאַקט	f a k t
פֿאַקטאָר	f a k t o r
פֿאַראײן	f a r e y n
פֿאַראײניקונג	f a r e y n i k u n g
פֿאַרב	f a r b
פֿאַרבאַנד	f a r b a n d
פֿאַרבעסערונג	f a r b e s e r u n g
פֿאַרברעך	f a r b r e k h
פֿאַרברעכער	f a r b r e k h e r
פֿאַרברענגען	f a r b r e n g e n
פֿאַרגאַנג	f a r g a n g
פֿאַרגאַנגענהײט	f a r g a n g e n h e y t
פֿאַרגעװאַלדיגונג	f a r g e v a l d i g u n g
פֿאַרגעניגן	f a r g e n i g n
פֿאַרטאָג	f a r t o g
פֿאַרלאַג	f a r l a g
פֿאַרמער	f a r m e r
פֿאָרעם	f o r e m
פֿאַרפֿל	f a r f l
פֿאָרץ	f o r t s
פֿאַרקויף	f a r k o y f
פֿאַרקויפֿער	f a r k o y f e r
פֿאַרקויפֿערין	f a r k o y f e r i n
פֿאָרשונג	f o r s h u n g
פֿאָרשטעלונג	f o r s h t e l u n g
פֿאַשיזם	f a s h i z m
פֿאַשיסט	f a s h i s t
פאכװע	p a k h v e
פאר	f a r
פּאָגראָם	p o g r o m
פּאָדלאָגע	p o d l o g e
פּאָדקאָװע	p o d k o v e
פּאָדקעװע	p o d k e v e
פּאַה	p e y e
פּאָװידלע	p o v i d l e
פּאַװע	p a v e
פּאָטאַסיום	p o t a s y u m
פּאַטלעזשאַן	p a t l e z h a n
פּאָטקעװע	p o t k e v e
פּאַטריאָט	p a t r y o t
פּאַטריאָטיזם	p a t r y o t i z m
פּאָיעזד	p o y e z d
פּאַכװע	p a k h v e
פּאַלאַץ	p a l a t s
פּאָליאַנער	p o l y a n e r
פּאָליאַק	p o l y a k
פּאָליטיק	p o l i t i k
פּאָליציאַנט	p o l i t s y a n t
פּאָליצײ	p o l i t s e y
פּאָליציסט	p o l i t s i s t
פּאָליצע	p o l i t s e
פּאַלעאָנטאָלאָגיע	p a l e o n t o l o g y e
פּאַלעסטינער	p a l e s t i n e r
פּאָמידאָר	p o m i d o r
פּאָמעראַנץ	p o m e r a n t s
פּאָמפּערניקל	p o m p e r n i k l
פּאַן	p a n
פּאַני	p a n i
פּאָסט	p o s t
פּאַסטושקע	p a s t u s h k e
פּאַסטעך	p a s t e k h
פּאַסטראַמי	p a s t r a m i
פּאַסטראַמע	p a s t r a m e
פּאַסכע	p a s k h e
פּאַסקודניאַק	p a s k u d n y a k
פּאָעט	p o e t
פּאָעם	p o e m
פּאָעמע	p o e m e
פּאָפּוגײַ	p o p u g a y
פּאַפּירעסל	p a p i r e s l
פּאַפּריקע	p a p r i k e
פּאָץ	p o t s
פּאָקן	p o k n
פּאָר	p o r
פּאַראַדאָקס	p a r a d o k s
פּאַראָדיסט	p a r o d i s t
פּאַראָדיע	p a r o d y e
פּאַראַפּסיכאָלאָגיע	p a r a p s i k h o l o g y e
פּאַרטיזאַן	p a r t i z a n
פּאַרטײ	p a r t e y
פּאַריזער	p a r i z e r
פּאָרל	p o r l
פּאָרנאָגראַפֿיע	p o r n o g r a f y e
פּאַרע	p a r e
פּאַרק	p a r k
פּאַרשוין	p a r s h o y n
פּאַשאַ	p a s h a
פּאה	p e y e
פּוד	p u d
פּוטער	p u t e r
פּויליש	p o y l i s h
פּויער	p o y e r
פּויפּס	p o y p s
פּויק	p o y k
פּויקשטעקל	p o y k s h t e k l
פּונקט	p u n k t
פּורימשפּיל	p u r i m s h p i l
פּיאַװקע	p y a v k e
פּיאַטע	p y a t e
פּיאָן	p y o n
פּיאַנע	p y a n e
פּיזשאַמעס	p i z h a m e s
פּילאָט	p i l o t
פֿויסט	f o y s t
פּיסטויל	p i s t o y l
פּיסק	p i s k
פּיעסע	p y e s e
פּיפּערנאָטער	p i p e r n o t e r
פּיצע	p i t s e
פּיקהאָלץ	p i k h o l t s
פּירוש	p e y r e s h
פּירושקע	p i r u s h k e
פּלאַטינום	p l a t i n u m
פּלאַנעטע	p l a n e t e
פּלאַץ	p l a t s
פּלוטאָ	p l u t o
פּלוטאָניום	p l u t o n y u m
פּליוש	p l y u s h
פּליט	p o l e t
פּליטה	p l e y t e
פּלײצע	p l e y t s e
פּלימעניצע	p l i m e n i t s e
פּלימעניק	p l i m e n i k
פּלעצל	p l e t s l
פּלשתּי	p l i s h t i
פֿונגוס	f u n g u s
פּנים	p o n e m
פֿונק	f u n k
פֿוס	f u s
פֿוסבאַל	f u s b a l
פֿוסבאָל	f u s b o l
פֿוסבאַליסט	f u s b a l i s t
פֿוסבאָליסט	f u s b o l i s t
פּסוק	p o s e k
פּסיכאָאַנאַליז	p s i k h o a n a l i z
פּסיכאָאַנאַליטיקער	p s i k h o a n a l i t i k e r
פּסיכאָלאָג	p s i k h o l o g
פּסיכאָלאָגיע	p s i k h o l o g y e
פּסיכע	p s i k h e
פּעדלער	p e d l e r
פּעליקאַן	p e l i k a n
פּעלץ	p e l t s
פּעמפּיק	p e m p i k
פּעניס	p e n i s
פּעסאָ	p e s o
פּעקלפֿלײש	p e k l f l e y s h
פּערל	p e r l
פֿופֿציקסטל	f u f t s i k s t l
פֿופֿצנטל	f u f t s n t l
פּראָבלעם	p r o b l e m
פּראָבע	p r o b e
פּראָזע	p r o z e
פּראָטאָן	p r o t o n
פּראָלאָג	p r o l o g
פּראָנאָם	p r o n o m
פּראָפֿעסאָר	p r o f e s o r
פּראָפֿעסיע	p r o f e s y e
פּראָפֿ׳	p r o f '
פּראָצענט	p r o t s e n t
פּראַקטיק	p r a k t i k
פּרוסאַק	p r u s a k
פּרט	p r a t
פּריז	p r i z
פּריזיװ	p r i z i v
פּריזיװניק	p r i z i v n i k
פּרינץ	p r i n t s
פּרינציפּ	p r i n t s i p
פּרינצעסין	p r i n t s e s i n
פּריפּעטשיק	p r i p e t s h i k
פּרעזידענט	p r e z i d e n t
פּרעמיע	p r e m y e
פּרעמיער	p r e m y e r
פּרעפּאָזיציע	p r e p o z i t s y e
פּרײַז	p r a y z
פּתח	p a s e k h
פּײַן	p a y n
פּײַקל	p a y k l
פּײַקלער	p a y k l e r
פֿי	f i
פֿיאַלקע	f y a l k e
פֿידל	f i d l
פֿידלער	f i d l e r
פֿיזיק	f i z i k
פֿיזיקער	f i z i k e r
פֿײגעלע	f e y g e l e
פֿיך	f i k h
פֿילאָלאָג	f i l o l o g
פֿילאָלאָגיע	f i l o l o g y e
פֿילאָסאָפֿיע	f i l o s o f y e
פֿילם	f i l m
פֿינאַנץ	f i n a n t s
פינגער	f i n g e r
פֿינגער	f i n g e r
פֿינגערל	f i n g e r l
פֿיניש	f i n i s h
פֿינסטערניש	f i n s t e r n i s h
פֿינפֿטל	f i n f t l
פֿינצטערניש	f i n t s t e r n i s h
פֿיסל	f i s l
פֿירזאָגערין	f i r z o g e r i n
פֿירמע	f i r m e
פֿירער	f i r e r
פֿיש	f i s h
פֿישער	f i s h e r
פֿלאַטערל	f l a t e r l
פֿלאַם	f l a m
פֿלאַש	f l a s h
פֿלוי	f l o y
פֿלוים	f l o y m
פֿלי	f l i
פֿליג	f l i g
פֿליגל	f l i g l
פֿלײט	f l e y t
פֿלײטיסט	f l e y t i s t
פֿליפֿעלד	f l i f e l d
פֿלעדערמויז	f l e d e r m o y z
פֿעברואַר	f e b r u a r
פֿעדער	f e d e r
פֿעדעראַציע	f e d e r a t s y e
פֿעלד	f e l d
פֿעליעטאָן	f e l y e t o n
פֿעליעטאָניסט	f e l y e t o n i s t
פֿעמיניזם	f e m i n i z m
פֿעמיניסט	f e m i n i s t
פֿענצטער	f e n t s t e r
פֿעפֿער	f e f e r
פֿעץ	f e t s
פֿערד	f e r d
פֿערדבין	f e r d b i n
פֿערדל	f e r d l
פֿערטל	f e r t l
פֿעריס־ראָד	f e r i s - r o d
פֿערציקסטל	f e r t s i k s t l
פֿערצנטל	f e r t s n t l
פֿראַגמענט	f r a g m e n t
פֿראַגע	f r a g e
פֿראַנצויז	f r a n t s o y z
פֿראַנצײזיש	f r a n t s e y z i s h
פֿרוי	f r o y
פֿרוכט	f r u k h t
פֿרומאַק	f r u m a k
פֿרומקײַט	f r u m k a y t
פֿרײד	f r e y d
פֿרילינג	f r i l i n g
פֿרימאָרגן	f r i m o r g n
פֿרישטיק	f r i s h t i k
פֿרעגצײכן	f r e g t s e y k h n
פֿרעסער	f r e s e r
פֿרײַטיק	f r a y t i k
פֿרײַנד	f r a y n d
פֿרײַנדין	f r a y n d i n
פֿרײַנדשאַפֿט	f r a y n d s h a f t
פֿײַל	f a y l
פֿײַער	f a y e r
פֿײַערל	f a y e r l
פֿײַערצונג	f a y e r t s u n g
פֿײַפֿל	f a y f l
פֿ׳	f '
צאָל	t s o l
צאַם	t s a m
צאָנדאָקטער	t s o n d o k t e r
צאָפּ	t s o p
צאַצקע	t s a t s k e
צאַר	t s a r
צאַרינע	t s a r i n e
צאַריצע	t s a r i t s e
צדיק	t s a d e k
צדקה	t s e d o k e
צדקת	t s a d e y k e s
צוג	t s u g
צוהערער	t s u h e r e r
צװאַנציקסטל	t s v a n t s i k s t l
צװאָרעך	t s v o r e k h
צװיט	t s v i t
צװילינג	t s v i l i n g
צװינטער	t s v i n t e r
צװעלפֿטל	t s v e l f t l
צװײַג	t s v a y g
צװײַגמעסער	t s v a y g m e s e r
צונג	t s u n g
צוקונפֿט	t s u k u n f t
צוקער	t s u k e r
צורה	t s u r e
ציבעלע	t s i b e l e
ציג	t s i g
ציגאַר	t s i g a r
ציגל	t s i g l
ציגײַנער	t s i g a y n e r
ציוניזם	t s i e n i z m
ציוניסט	t s i e n i s t
ציטראָן	t s i t r o n
ציטרין	t s i t r i n
צײכן	t s e y k h n
צײנשטעכער	t s e y n s h t e k h e r
ציל	t s i l
צילינדער	t s i l i n d e r
צימוקים־װײַן	t s i m u k i m - v a y n
צימעס	t s i m e s
צימער	t s i m e r
צימערינג	t s i m e r i n g
צינק	t s i n k
ציעניסט	t s i e n i s t
ציִעניסט	t s i e n i s t
ציץ	t s i t s
ציצית	t s i t s i s
ציצל	t s i t s l
ציצע	t s i t s e
ציר	t s i r
צניעות	t s n i e s
צעדערבוים	t s e d e r b o y m
צענדליק	t s e n d l i k
צענזוס	t s e n z u s
צענטימעטער	t s e n t i m e t e r
צענטל	t s e n t l
צענערלינג	t s e n e r l i n g
צער	t s a r
צערקװע	t s e r k v e
צפֿון	t s o f n
צרה	t s o r e
צײַט	t s a y t
צײַטפֿאָרמע	t s a y t f o r m e
קאָבאַלט	k o b a l t
קאַגעבעשניק	k a g e b e s h n i k
קאָװאַדלע	k o v a d l e
קאָװאַל	k o v a l
קאַװיאַר	k a v y a r
קאָװעל	k o v e l
קאַװעניק	k a v e n i k
קאָזאַק	k o z a k
קאַטאַלאָג	k a t a l o g
קאַטעגאָריע	k a t e g o r y e
קאַטשער	k a t s h e r
קאַטשקע	k a t s h k e
קאָכבוך	k o k h b u k h
קאַלאָש	k a l o s h
קאָלװירטניק	k o l v i r t n i k
קאַליום	k a l y u m
קאַליפֿיאָר	k a l i f y o r
קאַליקע	k a l i k e
קאָליר	k o l i r
קאָלכאָז	k o l k h o z
קאָלכאָזניק	k o l k h o z n i k
קאַלע	k a l e
קאָלעג	k o l e g
קאָלעגין	k o l e g i n
קאָלעגע	k o l e g e
קאַלענדאַר	k a l e n d a r
קאַלציום	k a l t s y u m
קאַלקולוס	k a l k u l u s
קאַלקע	k a l k e
קאַם	k a m
קאַמאָד	k a m o d
קאָמאָד	k o m o d
קאָמאָדע	k o m o d e
קאָמאַר	k o m a r
קאָמוניזם	k o m u n i z m
קאָמוניסט	k o m u n i s t
קאָמוניסטקע	k o m u n i s t k e
קאָמיטעט	k o m i t e t
קאַמיש	k a m i s h
קאָמע	k o m e
קאָמעדיע	k o m e d y e
קאַמף	k a m f
קאָמפּאָזיטאָר	k o m p o z i t o r
קאַמפּאַניע	k a m p a n y e
קאָמפּיוטער	k o m p y u t e r
קאָן	k o n
קאַנאַריק	k a n a r i k
קאָנגרעס	k o n g r e s
קאָנדוקטאָר	k o n d u k t o r
קאַנטאָניסט	k a n t o n i s t
קאָנטע	k o n t e
קאָנטראָליאָר	k o n t r o l y o r
קאָנטראַקט	k o n t r a k t
קאָניוגאַציע	k o n y u g a t s y e
קאָניונקטיװ	k o n y u n k t i v
קאָנסטיטוציע	k o n s t i t u t s y e
קאָנפֿערענץ	k o n f e r e n t s
קאָנצערט	k o n t s e r t
קאַסטן	k a s t n
קאַסטע	k a s t e
קאָסמאָס	k o s m o s
קאַפּיטאַל	k a p i t a l
קאַפּיטאַליזם	k a p i t a l i z m
קאַפּיטאַליסט	k a p i t a l i s t
קאַפּיטל	k a p i t l
קאַפּעליוש	k a p e l y u s h
קאַפּער	k a p e r
קאַפֿע	k a f e
קאַץ	k a t s
קאַקאַאָ	k a k a o
קאַקער	k a k e r
קאָרט	k o r t
קאַרטאָן	k a r t o n
קאַרטאָפֿל	k a r t o f l
קאַרטאָפֿליע	k a r t o f l y e
קאַרטע	k a r t e
קאַריקאַטור	k a r i k a t u r
קאָרן	k o r n
קאָרנברויט	k o r n b r o y t
קאָרע	k o r e
קאַרש	k a r s h
קאָשטשאָל	k o s h t s h o l
קאָשיק	k o s h i k
קאָשמאַר	k o s h m a r
קאַשע	k a s h e
קאפ	k o p
קאפעלע	k a p e l e
קבֿר	k e y v e r
קבֿרות	k v o r e s
קבלה	k a b o l e
קדוש	k o d e s h
קדושה	k d u s h e
קדיש	k a d e s h
קהילה	k e h i l e
קו	k u
קוגל	k u g l
קװאָטע	k v o t e
קװאַטער	k v a t e r
קװאַטערין	k v a t e r i n
קװאַל	k v a l
קװיט	k v i t
קװיטונג	k v i t u n g
קװיטל	k v i t l
קװײט	k v e y t
קװעטשער	k v e t s h e r
קוטשמע	k u t s h m e
קויל	k o y l
קוילנשטאָף	k o y l n s h t o f
קוימען	k o y m e n
קויפֿמאַן	k o y f m a n
קויש	k o y s h
קוישבאָל	k o y s h b o l
קוכן	k u k h n
קול	k o l
קולטור	k u l t u r
קונסט	k u n s t
קונץ	k u n t s
קונצנמאַכער	k u n t s n m a k h e r
קוסט	k u s t
קוסעלע	k u s e l e
קופּער	k u p e r
קוראָפּאַטע	k u r o p a t e
קורװע	k u r v e
קורס	k u r s
קוש	k u s h
קיבוץ	k i b u t s
קידוש	k i d e s h
קיום	k i y e m
קיטל	k i t l
קײט	k e y t
קײסער	k e y s e r
קײשל	k e y s h l
קיך	k i k h
קיכל	k i k h l
קילאָ	k i l o
קילאָמעטער	k i l o m e t e r
קימפּעט	k i m p e t
קימפּעט־פּאַלאַטע	k i m p e t - p a l a t e
קימפּעטאָרין	k i m p e t o r i n
קימפּעטקינד	k i m p e t k i n d
קינאָ	k i n o
קינבײן	k i n b e y n
קינד	k i n d
קינדעלע	k i n d e l e
קינדערביכל	k i n d e r b i k h l
קינדערגאָרטן	k i n d e r g o r t n
קיניג	k i n i g
קיניגין	k i n i g i n
קיניגל	k i n i g l
קינסטלער	k i n s t l e r
קינסטלערין	k i n s t l e r i n
קירך	k i r k h
קירכע	k i r k h e
קישינעװער	k i s h i n e v e r
קישן	k i s h n
קישקע	k i s h k e
קלאָגליד	k l o g l i d
קלאָגער	k l o g e r
קלאַװיאַטור	k l a v y a t u r
קלאַס	k l a s
קלאַסנקאַמף	k l a s n k a m f
קלאַרנעט	k l a r n e t
קלאַרנעטיסט	k l a r n e t i s t
קלימאַט	k l i m a t
קליפּה	k l i p e
קללה	k l o l e
קלעזמער	k l e z m e r
קמץ	k o m e t s
קמץ־אַלף	k o m e t s - a l e f
קמץ־אלף	k o m e t s - a l e f
קנאָבל	k n o b l
קנאות	k a n o e s
קני	k n i
קנײדל	k n e y d l
קניש	k n i s h
קנס	k n a s
קנעכט	k n e k h t
קנעכל	k n e k h l
קנײַפּער	k n a y p e r
קסילאָפֿאָן	k s i l o f o n
קעז	k e z
קעטשאָפּ	k e t s h o p
קעלנער	k e l n e r
קעלנערין	k e l n e r i n
קעלנערשע	k e l n e r s h e
קעניג	k e n i g
קעניגין	k e n i g i n
קעפּל	k e p l
קעצל	k e t s l
קעצעלע	k e t s e l e
קערנדל	k e r n d l
קערפּער	k e r p e r
קעשענע	k e s h e n e
קעשענעװער	k e s h e n e v e r
קראָ	k r o
קראַב	k r a b
קראַװאַט	k r a v a t
קראָם	k r o m
קראַפֿט	k r a f t
קראָקאָדיל	k r o k o d i l
קרבן	k o r b n
קרוין	k r o y n
קרוינעלע	k r o y n e l e
קרופּניק	k r u p n i k
קריטיק	k r i t i k
קריטיקער	k r i t i k e r
קריניצע	k r i n i t s e
קריסט	k r i s t
קריפּטאָן	k r i p t o n
קרעם	k r e m
קרעמל	k r e m l
קרעמער	k r e m e r
קרעניצע	k r e n i t s e
קרעפּל	k r e p l
קרעפּלפֿלײש	k r e p l f l e y s h
קרײַד	k r a y d
קרײַז	k r a y z
קשיא	k a s h e
ראָב	r o b
ראָג	r o g
ראָגאַל	r o g a l
ראָד	r o d
ראַדזשאַ	r a d z h a
ראַדיאָ	r a d y o
ראָװער	r o v e r
ראָזשינקע	r o z h i n k e
ראַט	r a t
ראָט	r o t
ראָטהויז	r o t h o y z
ראָלע	r o l e
ראַץ	r a t s
ראַציאָנאַליזם	r a t s y o n a l i z m
ראַקעט	r a k e t
ראָש־השנה	r o s h e s h o n e
רב	r e b
רבֿ	r o v
רבי	r e b e
רבי־ניסל	r e b e - n i s l
רבינו	r a b e y n u
רביניו	r e b e n y u
רביצין	r e b e t s i n
רבנו	r a b e y n u
רגע	r e g e
רובֿ	r o v
רובל	r u b l
רוגז	r o y g e z
רודער	r u d e r
רוח	r u e k h
רוחניות	r u k h n i e s
רויז	r o y z
רויטאַרמײער	r o y t a r m e y e r
רומעניש	r u m e n i s h
רומענער	r u m e n e r
רוס	r u s
רוסלענדער	r u s l e n d e r
ריטער	r i t e r
ריכטער	r i k h t e r
רינג	r i n g
רינד	r i n d
רינדפֿלײש	r i n d f l e y s h
ריפּ	r i p
רעגירונג	r e g i r u n g
רעגלאַמענט	r e g l a m e n t
רעגן	r e g n
רעגן־בויגן	r e g n - b o y g n
רעגן־מאַנטל	r e g n - m a n t l
רעגנבויגן	r e g n b o y g n
רעדער	r e d e r
רעװאָלװער	r e v o l v e r
רעװאָלוציע	r e v o l u t s y e
רעטעך	r e t e k h
רעטעכל	r e t e k h l
רעכט	r e k h t
רעליגיע	r e l i g y e
רענדל	r e n d l
רעסטאָראַן	r e s t o r a n
רעפֿאָרם	r e f o r m
רעפּובליק	r e p u b l i k
רעפּערטואַר	r e p e r t u a r
רעפֿערענדום	r e f e r e n d u m
רעקל	r e k l
רעשט	r e s h t
רפֿואה	r e f u e
רשע	r o s h e
רײַז	r a y z
רײַכקײַט	r a y k h k a y t
ר׳	r e b
שׂונא	s o y n e
שׂטן	s o t n
שׂינאה	s i n e
שׂכירות	s k h i r e s
שׂכל	s e y k h l
שׂכר	s k h a r
שׂמחה	s i m k h e
שׂנאה	s i n e
שׂרפֿה	s r e y f e
שׂררה	s r o r e
שאָ	s h o
שאָד	s h o d
שאָטן	s h o t n
שאַכמאַט	s h a k h m a t
שאָכפֿיגור	s h o k h f i g u r
שאַל	s h a l
שאַליקל	s h a l i k l
שאַנדע	s h a n d e
שאָס	s h o s
שאַקאַל	s h a k a l
שאַרבן	s h a r b n
שאלה	s h a y l e
שבֿט	s h e y v e t
שבת	s h a b e s
שד	s h e y d
שדכן	s h a d k h n
שװאַב	s h v a b
שװאָגער	s h v o g e r
שװאָם	s h v o m
שװאַן	s h v a n
שװאַנץ	s h v a n t s
שװאַרצער	s h v a r t s e r
שװיגער	s h v i g e r
שװיץ	s h v i t s
שװיצבאָד	s h v i t s b o d
שװעבל	s h v e b l
שװעגערין	s h v e g e r i n
שװעדיש	s h v e d i s h
שװעמל	s h v e m l
שװעסטער	s h v e s t e r
שװעסטערל	s h v e s t e r l
שװער	s h v e r
שװערד	s h v e r d
שוחט	s h o y k h e t
שוטה	s h o y t e
שויס	s h o y s
שוישפּילער	s h o y s h p i l e r
שוישפּילערין	s h o y s h p i l e r i n
שוך	s h u k h
שול	s h u l
שולד	s h u l d
שולע	s h u l e
שוסטער	s h u s t e r
שופּ	s h u p
שופֿט	s h o y f e t
שופר	s h o y f e r
שופֿר	s h o y f e r
שורש	s h o y r e s h
שחור	s h o k h e r
שחרית	s h a k h r e s
שטאָהל	s h t o h l
שטאַט	s h t a t
שטאָט	s h t o t
שטאָטגאָרטן	s h t o t g o r t n
שטאָל	s h t o l
שטאַפּל	s h t a p l
שטאַפֿעט	s h t a f e t
שטאָקפֿיש	s h t o k f i s h
שטוב	s h t u b
שטויב	s h t o y b
שטויבזויגער	s h t o y b z o y g e r
שטויבמאַשין	s h t o y b m a s h i n
שטול	s h t u l
שטופּ	s h t u p
שטורעם	s h t u r e m
שטות	s h t u s
שטותערײַ	s h t u s e r a y
שטיבל	s h t i b l
שטיװל	s h t i v l
שטײן	s h t e y n
שטיק	s h t i k
שטיקל	s h t i k l
שטיקשטאָף	s h t i k s h t o f
שטעטל	s h t e t l
שטעכל־חזיר	s h t e k h l - k h a z e r
שטעכלער	s h t e k h l e r
שטעכער	s h t e k h e r
שטעקל	s h t e k l
שטעקן	s h t e k n
שטערן	s h t e r n
שטראָם	s h t r o m
שטרודל	s h t r u d l
שטרוי	s h t r o y
שטריק	s h t r i k
שטרײַמל	s h t r a y m l
שטשאַװ	s h t s h a v
שײגעץ	s h e y g e t s
שײגעצל	s h e y g e t s l
שײד	s h e y d
שײטל	s h e y t l
שײך	s h a y e k h
שײנקײט	s h e y n k e y t
שילד	s h i l d
שינקע	s h i n k e
שיף	s h i f
שיפֿל	s h i f l
שיפֿער	s h i f e r
שיפֿערין	s h i f e r i n
שיקסע	s h i k s e
שיר	s h i r
שירה	s h i r e
שירעם	s h i r e m
שישקע	s h i s h k e
שכינה	s h k h i n e
שכן	s h o k h n
שלאַכט	s h l a k h t
שלאַכטמאַן	s h l a k h t m a n
שלאַכטפֿעלד	s h l a k h t f e l d
שלאַנג	s h l a n g
שלאָס	s h l o s
שלאָסער	s h l o s e r
שלאָף	s h l o f
שלאָפֿלאָזיקײט	s h l o f l o z i k e y t
שלאָפֿצימער	s h l o f t s i m e r
שלאַפֿקײט	s h l a f k e y t
שלאָפֿראָק	s h l o f r o k
שלום	s h o l e m
שלוש־סעודות	s h a l e s h u d e s
שליוכע	s h l y u k h e
שליח	s h l i e k h
שלײקעס	s h l e y k e s
שלימזלניצע	s h l i m a z l n i t s e
שלימיל	s h l i m i l
שליסל	s h l i s l
שלעסל	s h l e s l
שמאַטע	s h m a t e
שמאָך	s h m o k h
שמאַנט	s h m a n t
שמאָק	s h m o k
שמאָראַק	s h m o r a k
שמועס	s h m u e s
שמוץ	s h m u t s
שמידהאַמער	s h m i d h a m e r
שמײכל	s h m e y k h l
שמיר	s h m i r
שמעגעגע	s h m e g e g e
שמעק	s h m e k
שמעקל	s h m e k l
שמערץ	s h m e r t s
שמש	s h a m e s
שמשׂ	s h a m e s
שמשׂטע	s h a m e s t e
שנאַפּס	s h n a p s
שנאָרער	s h n o r e r
שנור	s h n u r
שנײ	s h n e y
שנײמענטש	s h n e y m e n t s h
שנײעלע	s h n e y e l e
שניפּס	s h n i p s
שנירל	s h n i r l
שנעק	s h n e k
שנײַדער	s h n a y d e r
שנײַדערוק	s h n a y d e r u k
שנײַדעריונג	s h n a y d e r y u n g
שנײַדערקע	s h n a y d e r k e
שנײַדערײַ	s h n a y d e r a y
שעה	s h o
שעפּטש	s h e p t s h
שעפּס	s h e p s
שער	s h e r
שערל	s h e r l
שערער	s h e r e r
שערערין	s h e r e r i n
שפּאַלט	s h p a l t
שפּאַניער	s h p a n y e r
שפּיאָן	s h p y o n
שפּיאָנאַזש	s h p y o n a z h
שפּיגל	s h p i g l
שפּיז	s h p i z
שפּיטאָל	s h p i t o l
שפּיל	s h p i l
שפּילער	s h p i l e r
שפּילקע	s h p i l k e
שפּין	s h p i n
שפּינאַט	s h p i n a t
שפּיץ	s h p i t s
שפּעק	s h p e k
שפּערל	s h p e r l
שפּראַך	s h p r a k h
שפּראַך־װיסנשאַפֿט	s h p r a k h - v i s n s h a f t
שפּראַכגעפֿיל	s h p r a k h g e f i l
שפּריכװאָרט	s h p r i k h v o r t
שפּרינגער	s h p r i n g e r
שפּריץ	s h p r i t s
שפּײַעכץ	s h p a y e k h t s
שפײעכץ	s h p a y e k h t s
שפֿלות	s h i f l e s
שקל	s h e k l
שקלאַף	s h k l a f
שרויף	s h r o y f
שרימפּ	s h r i m p
שריפֿט	s h r i f t
שרעק	s h r e k
שרײַבער	s h r a y b e r
שתּדלן	s h t a d l e n
שתּדלנות	s h t a d l o n e s
שײַן	s h a y n
תּאוה	t a y v e
תּבֿה	t e y v e
תּהום	t o m
תּוך	t o k h
תּחינה	t k h i n e
תּיפֿלה	t i f l e
תּיקון	t i k n
תּישעה־באָבֿ	t i s h e b o v
תּכשיט	t a k h s h e t
תּליה	t l i y e
תּליון	t a l y e n
תּלין	t a l y e n
תּלמוד־תּורה	t a l m e t o y r e
תּלמיד	t a l m e d
תּלמיד־חכם	t a l m i d - k h o k h e m
תּלמידה	t a l m i d o
תּם	t a m
תּמוז	t a m e z
תּמימות	t e m i m e s
תּנא	t a n e
תּנאַי	t n a y
תּענית	t o n e s
תּפֿילה	t f i l e
תּפֿילין	t f i l n
תּפֿיסה	t f i s e
תּקופֿה	t k u f e
תּקנה	t a k o n e
תּרגום	t a r g e m
תּשובֿה	t s h u v e
תּשליך	t a s h l e k h
תּשעה־באָבֿ	t i s h e b o v
תחת	t o k h e s