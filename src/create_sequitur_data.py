import pandas as pd
import sys

# constants
INPUT_PATH = '../data/wiktionary_standardized_nodiacritics.csv'
YI2ROM_TRAIN_OUTPUT_PATH = '../data/lex/wiktionary_yivo2rom_train.lex'
YI2ROM_TEST_OUTPUT_PATH = '../data/lex/wiktionary_yivo2rom_test.lex'
ROM2YI_TRAIN_OUTPUT_PATH = '../data/lex/wiktionary_rom2yivo_train.lex'
ROM2YI_TEST_OUTPUT_PATH = '../data/lex/wiktionary_rom2yivo_test.lex'
CH2YI_TRAIN_OUTPUT_PATH = '../data/lex/wiktionary_chasid2yivo_train.lex'
CH2YI_TEST_OUTPUT_PATH = '../data/lex/wiktionary_chasid2yivo_test.lex'
TEST_FRACTION = 0.1
SEED = 420

# load data
wiktionary = pd.read_csv(INPUT_PATH)
n = wiktionary.shape[0]
n_test = round(TEST_FRACTION*n)

# randomly sample test set
test = wiktionary.sample(n_test, random_state=SEED)
test_mask = wiktionary.isin(test).transliteration
train = wiktionary[~test_mask]

# process into rows
def create_yi2rom_row(row):
    yi_word = row.yiddish
    rom_chars = " ".join([c for c in row.transliteration])
    return f"{yi_word}\t{rom_chars}"

def create_rom2yi_row(row):
    rom_word = row.transliteration
    yi_chars = " ".join([c for c in row.yiddish])
    return f"{rom_word}\t{yi_chars}"

def create_chasid2yi_row(row):
    chasid = row.yiddish_without_diacritics
    yivo_chars = " ".join([c for c in row.yiddish])
    return f"{chasid}\t{yivo_chars}"

yi2rom_train_rows = "\n".join(train.apply(create_yi2rom_row, axis=1).tolist())
yi2rom_test_rows = "\n".join(test.apply(create_yi2rom_row, axis=1).tolist())

rom2yi_train_rows = "\n".join(train.apply(create_rom2yi_row, axis=1).tolist())
rom2yi_test_rows = "\n".join(test.apply(create_rom2yi_row, axis=1).tolist())

chasid2yi_train_rows = "\n".join(train.apply(create_chasid2yi_row, axis=1).tolist())
chasid2yi_test_rows = "\n".join(test.apply(create_chasid2yi_row, axis=1).tolist())

# output
def write_file(content, fpath):
    with open(fpath, 'w') as f:
        f.write(content)

output_data = [
    (yi2rom_train_rows, YI2ROM_TRAIN_OUTPUT_PATH),
    (yi2rom_test_rows, YI2ROM_TEST_OUTPUT_PATH ),
    (rom2yi_train_rows, ROM2YI_TRAIN_OUTPUT_PATH),
    (rom2yi_test_rows, ROM2YI_TEST_OUTPUT_PATH ),
    (chasid2yi_train_rows, CH2YI_TRAIN_OUTPUT_PATH),
    (chasid2yi_test_rows , CH2YI_TEST_OUTPUT_PATH)
]

for rows, out_file in output_data:
    write_file(rows, out_file)
