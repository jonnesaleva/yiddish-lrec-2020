import os
import re
import pandas as pd
import itertools as it
from collections import defaultdict

class TokenTransformer:

    def __init__(self):

        # LIGATURE CONSTANTS
        self.VOVVOV = 'וו'
        self.TSVEY_VOVN = 'װ'
        self.YUDYUD = 'יי'
        self.TSVEY_YUDN, self.PASEKH = 'ײַ'
        self.PASEKH_TSVEY_YUDN = self.TSVEY_YUDN + self.PASEKH
        self.VOVYUD_SEPARATE = 'וי'
        self.VOVYUD_JOINED = 'ױ'
        (self.ALEF, 
            self.KOMETS, 
            self.BEYS, 
            self.VEYS_FEY_DIACRITIC) = 'אָבֿ'
        self.PEY, self.PEY_DIACRITIC = 'פּ'
        self.SOF, self.TOF_DIACRITIC = 'תּ'
        self.VOV, self.VOV_U_DIACRITIC = 'וּ'
        self.DIACRITICS= set([self.PASEKH, self.KOMETS, 
                              self.VEYS_FEY_DIACRITIC, 
                              self.PEY_DIACRITIC, 
                              self.TOF_DIACRITIC,
                              self.VOV_U_DIACRITIC])

        self.CHAR_TO_DIACRITIC = {
            self.ALEF: [self.PASEKH, self.KOMETS],
            self.BEYS: [self.VEYS_FEY_DIACRITIC],
            self.PEY: [self.PEY_DIACRITIC, self.VEYS_FEY_DIACRITIC],
            self.VOV: [self.VOV_U_DIACRITIC],
            self.SOF: [self.TOF_DIACRITIC]
        }


    def __fix_ligature(self, ligature, desired, token):
        """
        Takes in `token` and substitutes instances
        of `ligature` with `desired`.
        """
        return re.sub(ligature, desired, token)


    def fix_yud_yud(self, token):
        return self.__fix_ligature(ligature=self.YUDYUD, 
                                   desired=self.TSVEY_YUDN, 
                                   token=token)

    def fix_vov_vov(self, token):
        return self.__fix_ligature(ligature=self.VOVVOV, 
                                   desired=self.TSVEY_VOVN, 
                                   token=token)

    def fix_vov_yud(self, token):
        return self.__fix_ligature(ligature=self.VOVYUD_SEPARATE, 
                                   desired=self.VOVYUD_JOINED, 
                                   token=token)

    def fix_ligatures(self, token, fix_vov_yud=False):
        """
        Fixes all ligatures in token
        """
        _f = [self.fix_yud_yud, self.fix_vov_vov]
        __f = [self.fix_vov_yud] if fix_vov_yud else []
        fixes = _f + __f
        for f in fixes:
            token = f(token)
        return token

    def remove_diacritics(self, token, sub_by='', join=True):
        chars = [c for c in token]
        sub = lambda c: c if c not in self.DIACRITICS else sub_by
        chars_without_diacritics = [sub(c) for c in chars]
        if join:
            return "".join(chars_without_diacritics)
        else:
            return chars_without_diacritics

    def possible_diacritics(self, char):
        if char not in self.CHAR_TO_DIACRITIC:
            raise ValueError(f'Diacritics do not exist for `{char}`')
        if char == self.PEY:
            return self.CHAR_TO_DIACRITIC[char]
        else:
            return [''] + self.CHAR_TO_DIACRITIC[char]

    def diacritize_exhaustively(self, token):
        if any([c in self.DIACRITICS for c in token]):
            raise ValueError(f'The token `{token}` already has diacritics!')

        # generate "lattice" of characters
        lattice = []
        for c in token:
            lattice.append([c])
            if c in self.CHAR_TO_DIACRITIC:
                lattice.append(self.possible_diacritics(c))
        print(lattice)

        # generate all possible strings from lattice
        candidates = ["".join(cand) for cand in it.product(*lattice)]
        return candidates

class CorpusTransformer:
    """
    Class for performing various operations on a corpus
    represented as a pandas DataFrame.
    """

    def __init__(self, token_transformer):
        self.token_transformer = token_transformer
    

    def group_transliterations(self, corpus):
        """
        Groups Yiddish words by their romanized transliterations
        so that we can pick 1 or more transliterations to be the
        gold standard.
        """
        output = defaultdict(list)
        for ix, row in corpus.iterrows():
            output[row.transliteration].append(row.yiddish)
        return output

    def contains_pey_but_no_diacritic(self, y):
        cond1 = self.token_transformer.PEY in y
        cond2 = self.token_transformer.VEYS_FEY_DIACRITIC in y or \
                self.token_transformer.PEY_DIACRITIC in y
        return cond1 and not cond2

    def ay_but_only_tsvey_yudn(self, t, y):
        cond1 = 'ay' in t
        cond2 = self.token_transformer.TSVEY_YUDN in y
        cond3 = self.token_transformer.PASEKH_TSVEY_YUDN not in y
        return cond1 and cond2 and cond3

    def potentially_nonstandard(self, t, y):
        return any([self.contains_pey_but_no_diacritic(y),
                    self.ay_but_only_tsvey_yudn(t,y)])

    def row_potentially_nonstandard(self, row):
        t, y = row.transliteration, row.yiddish
        return self.potentially_nonstandard(t,y)

    def __apply_function_to_col(self, f, corpus, col_name, in_place=False, return_single_col=False):
        """
        Abstract function to apply 1D function `f` to column `col_name`
        of `corpus`. Doesn't operate in place unless explicitly instructed
        to do so. Can also return only the single column.
        """
        column = corpus[col_name]
        column_fixed = column.apply(f)

        if return_single_col:
            return column_fixed
        elif in_place:
            corpus[col_name] = column_fixed
            return corpus
        else:
            output = corpus.copy()
            output[col_name] = column_fixed
            return output

    def fix_ligatures(self, corpus, yi_col='yiddish', in_place=False, return_single_col=False):
        """
        Fixes ligatures etc in column `yi_col` of `corpus`
        Returns a new corpus object unless explicitly told 
        to operate in-place
        """
        return self.__apply_function_to_col(f=self.token_transformer.fix_ligatures, 
                                           corpus=corpus, col_name=yi_col, in_place=in_place, 
                                           return_single_col=return_single_col)

    def remove_diacritics(self, corpus, yi_col='yiddish', in_place=False, return_single_col=False):
        """
        Removes diacritics in column `yi_col` of `corpus`
        Returns a new corpus object unless explicitly told 
        to operate in-place
        """
        return self.__apply_function_to_col(f=self.token_transformer.remove_diacritics, 
                                           corpus=corpus, col_name=yi_col, in_place=in_place,
                                           return_single_col=return_single_col)

