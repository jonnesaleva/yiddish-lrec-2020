"""
Script to clean Wiktionary corpus such that
    - All non-Hebraic words have proper YIVO-style orthography
    - All Hebrew words are spelled non-phonetically
    - Each transcription only appears once


# A WORD IS NONSTANDARD IF
# - it has pey but no diacritic (veyz/pey dot) AND it is not otherwise hebraic
# - alternatively: translit contains "ay" but yiddish only has TSVEY_YUDN
"""

import os
import re
import pandas as pd
import itertools as it
from collections import defaultdict

# OTHER CONSTANTS
INPUT_PATH = '../data/wiktionary_single_word_transliterations.csv'
OUTPUT_PATH = '../data/wiktionary_standardized.csv'

# LIGATURE CONSTANTS
VOVVOV = 'וו'
TSVEY_VOVN = 'װ'
YUDYUD = 'יי'
TSVEY_YUDN, PASEKH = 'ײַ'
VOVYUD_SEPARATE = 'וי'
VOVYUD_JOINED = 'ױ'
PASEKH_TSVEY_YUDN = TSVEY_YUDN + PASEKH
ALEF, KOMETS, BET, VEYS_FEY_DIACRITIC = 'אָבֿ'
PEY, PEY_DIACRITIC = 'פּ'
VOV, VOV_U_DIACRITIC = 'וּ'

# HELPER FUNCTIONS
def contains_pey_but_no_diacritic(y):
    cond1 = PEY in y
    cond2 = VEYS_FEY_DIACRITIC in y or PEY_DIACRITIC in y
    return cond1 and not cond2

def ay_but_only_tsvey_yudn(t, y):
    cond1 = 'ay' in t
    cond2 = TSVEY_YUDN in y
    cond3 = PASEKH_TSVEY_YUDN not in y
    return cond1 and cond2 and cond3

def row_potentially_nonstandard(row):
    t, y = row.transliteration, row.yiddish
    return potentially_nonstandard(t,y)

def potentially_nonstandard(t, y):
    return any([contains_pey_but_no_diacritic(y),
                ay_but_only_tsvey_yudn(t,y)])

def group_transliterations(df):
    output = defaultdict(list)
    for ix, row in df.iterrows():
        output[row.transliteration].append(row.yiddish)
    return output

def flatten(nested):
    """
    Flattens nested list.
    """
    return list(it.chain.from_iterable(nested))

if __name__ == '__main__':
    # load wiktionary
    wiktionary = pd.read_csv(INPUT_PATH)

    # fix yud yud => tsvey yudn and vov vov => tsvey vovn
    fixed_ligatures = wiktionary\
                        .yiddish\
                        .apply(lambda y: re.sub(YUDYUD, TSVEY_YUDN, y))\
                        .apply(lambda y: re.sub(VOVVOV, TSVEY_VOVN, y))
    wiktionary['original'] = wiktionary.yiddish.tolist()
    wiktionary['yiddish'] = fixed_ligatures

    # check rows that contain pey but no dot or fey
    non_standard_mask = wiktionary.apply(row_potentially_nonstandard, axis=1)
    potential_nonstandards = wiktionary[non_standard_mask]
    potential_nonstandard_words = set(potential_nonstandards.transliteration)

    # now find all instances of the potentially nonstandard transliterations
    # so that we can generate candidate transliterations for user to choose from
    f = lambda s: s in potential_nonstandard_words
    non_standard_mask_translit = wiktionary\
                                    .transliteration\
                                    .apply(f)

    candidate_transliterations = wiktionary[non_standard_mask_translit]
    candidate_dict = group_transliterations(candidate_transliterations)

    standardized = {}
    translits = group_transliterations(wiktionary)
    for rom, cand in translits.items():
        print(f'Word to choose spelling for: {rom}')
        if len(cand) == 1:
            print(f'There exists only one spelling. Picking {cand[0]}...')
            standardized[rom] = cand
        else:
            print('Candidate transliterations:')
            for s in [f"{ix}: {cand} ({list(cand)})" for ix, cand in enumerate(cand)]:
                print(s)
            user_selection = [
                int(x) for x in input('Please indicate the correct transliteration(s): ').split( )
            ]
            standardized[rom] = [cand[x] for x in user_selection]

    # construct new data frame
    new_yiddish = flatten([ys for t, y in standardized.items()])
    new_translits = flatten([[t for _ in ys] for t, y in standardized.items()])
    clean_wiktionary = pd.DataFrame({'transliteration': new_translits, 
                                     'yiddish': new_yiddish})

    # save cleaned
    clean_wiktionary.to_csv(OUTPUT_PATH, index=False)
