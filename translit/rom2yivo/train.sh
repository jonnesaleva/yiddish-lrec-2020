TRAIN_PATH="../../data/lex/wiktionary_rom2yivo_train.lex"

echo "Training model"
g2p.py -e UTF-8 --train $TRAIN_PATH --devel 5% --write-model model-1
g2p.py -e UTF-8 --model model-1 --ramp-up --train $TRAIN_PATH --devel 5% --write-model model-2
g2p.py -e UTF-8 --model model-2 --ramp-up --train $TRAIN_PATH --devel 5% --write-model model-3
g2p.py -e UTF-8 --model model-3 --ramp-up --train $TRAIN_PATH --devel 5% --write-model model-4
g2p.py -e UTF-8 --model model-4 --ramp-up --train $TRAIN_PATH --devel 5% --write-model model-5
g2p.py -e UTF-8 --model model-5 --ramp-up --train $TRAIN_PATH --devel 5% --write-model model-6

