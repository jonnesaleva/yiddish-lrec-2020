# Evaluates the model on the test set
TRAIN_PATH="../../data/lex/wiktionary_rom2yivo_train.lex"
TEST_PATH="../../data/lex/wiktionary_rom2yivo_test.lex"

echo "Train set metrics"
g2p.py -e UTF-8 --model model-6 --test $TRAIN_PATH

echo "Test set metrics"
g2p.py -e UTF-8 --model model-6 --test $TEST_PATH
