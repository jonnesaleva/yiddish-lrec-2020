# A Multi-Orthography Parallel Corpus of Yiddish Nouns

This repository hosts the code and LaTeX writeup for the paper _A Multi-Orthography Parallel Corpus of Yiddish Nouns_.

### Transliteration models & experiments

To run the Sequitur models on the train and test data and view the results, simply run `./experimental_results.sh`.

For verbose output, there is a `eval_verbose.sh` in each experiment folder under `./translit`.

To run model on unseen data, use `apply_to_unseen.sh` in each experiment folder. To read from stdin, simply pipe a word per line to `./apply_to_unseen.sh -`.

For instance:

```
# first this
cd translit/rom2yivo

# then this
./apply_to_unseen /path/to/my/romanized_wordlist

# or this
echo "geburtstog" | ./apply_to_unseen.sh -
```

### Contact

To get in touch with the author, visit my [website](http://www.jonnesaleva.com)
